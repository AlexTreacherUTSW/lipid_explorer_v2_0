# -*- coding: utf-8 -*-
"""
Created on Fri Dec 04 13:36:53 2015

@author: Mike
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 11:05:29 2015

@author: Alex
"""

import Tkinter as tk
import ttk
import tkMessageBox
import tkFileDialog
import os, sys
import pandas as pd
import numpy as np
import shelve
import subprocess


#subporcess.call
#python -i test.py
standardDir = os.path.join(os.getcwd(), 'LipPy_v2_0', 'Lookups','Standards_DB')
namesDir = os.path.join(os.getcwd(), 'LipPy_v2_0', 'Lookups','Molecule_Database')
g = shelve.open(standardDir)
standards = g
#g.close()

root_dir = os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0','LipPy_UI_Backend')
class LipPy_UI_backend:
    
    def __init__(self, master):
        self.master = master
        self.master.geometry('450x300+200+200')
        #self.master.iconbitmap(os.path.join(root_dir,'logo.ico'))
        self.frame = ttk.Frame(self.master)
        self.style = ttk.Style()
        self.style.theme_use('vista')
        self.title = master.title('LipPy_UI')
        self.titlecolor = '#%02x%02x%02x' % (145,166,213)
        
        #self.img = ImageTk.PhotoImage(Image.open("FrontPage.jpg"))
        #self.panel = tk.Label(self.master, image = self.img)
        #self.panel.pack(side = "bottom", fill = "both", expand = "yes")

        self.menubar = tk.Menu(self.master)
        self.fileMenu = tk.Menu(self.master, tearoff = False)
        self.stdMenu = tk.Menu(self.master, tearoff = False)
        self.nameFileMenu = tk.Menu(self.master, tearoff = False)
        
        self.menubar.add_cascade(label='File', menu=self.fileMenu)
        self.fileMenu.add_cascade(label='Standard', menu=self.stdMenu)
        self.stdMenu.add_command(label='Add Standard')
        self.stdMenu.add_command(label='View Standard')
        
        self.fileMenu.add_cascade(label='Naming Table', menu=self.nameFileMenu)
        #add commands for std menu
        self.nameFileMenu.add_command(label='View pos list')
        self.nameFileMenu.add_command(label='View neg list')
        self.nameFileMenu.add_command(label='Add to pos list')
        self.nameFileMenu.add_command(label='Add to neg list')
        self.fileMenu.add_command(label= 'Quit', command = self.close_window)
        self.master.config(menu=self.menubar)
        
        tk.Label(self.master, text = 'test message', font = 'Helvetica 10 bold', height =1).pack(pady = 5)
        
        self.button1 = ttk.Button(self.frame, text = 'Run LipPy', width = 25, command = self.run)
        self.button1.pack()
        self.frame.pack()
        
    def run(self):
        folder = tkFileDialog.askdirectory(parent=self.master,title='Choose a subtitle file')
        print folder
        lippyfolder = 'D:\Alex Treacher\Lipid_Explorer_v2_0'
        theproc = subprocess.Popen([sys.executable, folder+"inputs.py"],cwd = lippyfolder)
        theproc.communicate()
    
    def close_window(self):
        #QTOFinfo.close()
        self.master.destroy()
        
    def addStandard(self):
        n = shelve.open(namesDir)
        groupspos = list(set(n['+'].Group.tolist()))
        groupsneg = list(set(n['-'].Group.tolist()))
        n.close()
        #+ default and lipid types(pl,nl etc)
        #check groups are in names list        
        pass
#    def newWindow(self):
#        self.nW = tk.Toplevel(self.master)
#        self.nW.title('Window Name')
#        self.nW.geometry('300x300+100+50')
#        self.nW.iconbitmap(root_dir+'logo.ico')
#        self.nWFrame = ttk.Frame(self.mW)
#        
#        
#        self.nWFrame.pack
if True:
    root = tk.Tk()
    LipPy_UI_backend(root)
    root.mainloop()
