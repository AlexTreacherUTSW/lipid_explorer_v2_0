# -*- coding: utf-8 -*-
"""
Created on Fri Dec 04 13:36:53 2015

@author: Mike
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 11:05:29 2015

@author: Alex
"""

import Tkinter as tk
import ttk
import tkMessageBox
import tkFileDialog
from PIL import ImageTk, Image
from collections import OrderedDict
import os, sys
from shutil import copy
import pandas as pd
import numpy as np
import csv
from bisect import bisect_left
import shelve
import subprocess
from threading import Thread
import subprocess
from multiprocessing.pool import ThreadPool
#subporcess.call
#python -i test.py
import pdb


rootDir = os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0','LipPy_UI_Backend')
LipPyDir = os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0')
class LipPy_UI_backend:
    
    def __init__(self, master):
        self.master = master
        self.master.geometry('450x300+200+200')
        #self.master.iconbitmap(os.path.join(root_dir,'logo.ico'))
        self.frame = ttk.Frame(self.master)
        self.style = ttk.Style()
        self.style.theme_use('vista')
        self.title = master.title('LipPy_UI')
        self.titlecolor = '#%02x%02x%02x' % (145,166,213)
        
        #self.img = ImageTk.PhotoImage(Image.open("FrontPage.jpg"))
        #self.panel = tk.Label(self.master, image = self.img)
        #self.panel.pack(side = "bottom", fill = "both", expand = "yes")

        self.menubar = tk.Menu(self.master)
        self.fileMenu = tk.Menu(self.master, tearoff = False)
        self.stdMenu = tk.Menu(self.master, tearoff = False)
        self.nameFileMenu = tk.Menu(self.master, tearoff = False)
        
        self.menubar.add_cascade(label='File', menu=self.fileMenu)
        self.fileMenu.add_cascade(label='Standard', menu=self.stdMenu)
        self.stdMenu.add_command(label='Add Standard')
        self.stdMenu.add_command(label='View Standard')
        
        self.fileMenu.add_cascade(label='Naming Table', menu=self.nameFileMenu)
        #add commands for std menu
        self.nameFileMenu.add_command(label='View pos list')
        self.nameFileMenu.add_command(label='View neg list')
        self.nameFileMenu.add_command(label='Add to pos list')
        self.nameFileMenu.add_command(label='Add to neg list')
        self.fileMenu.add_command(label= 'Quit', command = self.close_window)
        self.master.config(menu=self.menubar)
        
        self.progressbar = ttk.Progressbar(orient=tk.HORIZONTAL, length=200, mode='indeterminate')
        self.progressbar.pack(side="bottom")
        
        tk.Label(self.master, text = 'test message', font = 'Helvetica 10 bold', height =1).pack(pady = 5)
        
        self.button1 = ttk.Button(self.frame, text = 'Run LipPy', width = 25, command = self.run4)
        self.button1.pack()
        
        self.que = []
        
        self.frame.pack()
        
    def run(self):
        folder = tkFileDialog.askdirectory(parent=self.master,title='Choose a subtitle file')
        print folder
        thread = Thread(target = runLipPy(folder))
        thread.start()
        self.progressbar.start(10)
        while thread.is_alive():
            pass
        self.progressbar.stop()
        
        '''
        lippyfolder = 'D:\Alex Treacher\Lipid_Explorer_v2_0'
        theproc = subprocess.Popen([sys.executable, folder+"inputs.py"],cwd = LipPyDir)
        theproc.communicate()
        '''
    
    def run2(self):
        folder = tkFileDialog.askdirectory(parent=self.master,title='Choose a subtitle file')
        theproc = subprocess.Popen([sys.executable, folder+os.sep+"test.py"],cwd = LipPyDir)
        
        theproc.communicate()
    
    def run3(self):
        folder = tkFileDialog.askdirectory(parent=self.master,title='Choose a subtitle file')
        self.proc=subprocess.Popen([sys.executable, folder+os.sep+"test.py"] ,shell=False)
        self.progressbar.start(10)
        while self.proc.poll() is None:
            pass
        self.progressbar.stop()

    def run4(self):
        folder = tkFileDialog.askdirectory(parent=self.master,title='Choose a subtitle file')
        sys.path.append(folder)
        from inputs import run
        self.progressbar.start(10)
        x= pool.apply_async(run)
        #x.wait()
        
        
    def close_window(self):
        #QTOFinfo.close()
        self.master.destroy()
        
#    def newWindow(self):
#        self.nW = tk.Toplevel(self.master)
#        self.nW.title('Window Name')
#        self.nW.geometry('300x300+100+50')
#        self.nW.iconbitmap(root_dir+'logo.ico')
#        self.nWFrame = ttk.Frame(self.mW)
#        
#        
#        self.nWFrame.pack
def runLipPy(folder):
        print 'got here'
        theproc = subprocess.Popen([sys.executable, folder+os.sep+"test.py"],cwd = LipPyDir)
        #theproc.communicate()
        
pool = ThreadPool(processes=1)

def run_file(file_name):
    subprocess.Popen([sys.executable, file_name], shell=True)

if True:
    root = tk.Tk()
    LipPy_UI_backend(root)
    root.mainloop()
