# -*- coding: utf-8 -*-
"""
Created on Mon Feb 09 16:51:47 2015

@author: Mike
"""

from scipy.stats import ttest_ind, ttest_rel, ranksums
import pandas as pd
import numpy as np
import math

SIGNIFICANCE = 0.05

def ttest(g1, g2, paired):
    if not paired:
        return ttest_ind(g1, g2, equal_var=False)[1]
    else:
        return ttest_rel(g1, g2)[1]
        
def benjamini_hochberg(array_like, FDR=.05):
    """
    Returns the p-value that would be required for a false discovery rate of
    FDR as well as the number of entries in array_like that are significant
    at this level.
    """
    k = 0
    m = len(array_like)
    for p in array_like:
        if p <= FDR*float(k+1)/float(m):
            k += 1
        else:
            sig = p
            break
    return sig, k
        
def fold_change(g1, g2):
    pass

def nldist(x,m,s):
    f = (1/math.sqrt(2*math.pi*(s**2)))*math.exp(-((x-s)**2/(2*(s**2))))
    return f
    
def Hypothesis_Tests_Sheet(dataframe, g1_cols, g2_cols, g1_name = 'Group1', g2_name = 'Group2', cols_to_keep = [], paired=False, append_group_names=False, sort=False):
    '''        
    STDEV = Sqrt(SUM(x-ave(x))/(n-1)) x is mean average and n is sample size

    '''         
    stats_frame = pd.DataFrame()
    g1, g2 = g1_cols,g2_cols
    gname1, gname2 = g1_name,g2_name
    app = append_group_names*('_'+gname2+'_/_'+gname1)
    df = dataframe[g1+g2]
    stats_frame = dataframe[cols_to_keep]
    stats_frame['Mean_'+gname1] = df[g1].mean(axis=1)
    stats_frame['Mean_'+gname2] = df[g2].mean(axis=1)
    stats_frame['Stdev_'+gname1] = df[g1].std(axis=1)
    stats_frame['Stdev_'+gname2] = df[g2].std(axis=1)
    stats_frame['Fold_Change'+app] = stats_frame['Mean_'+gname2].div(stats_frame['Mean_'+gname1])
    if not paired:        
        stats_frame['RS_p_value'+app] = df.apply(lambda x : ranksums(x[g1], x[g2])[1], axis=1)
        stats_frame['t_test_p_value'+app] = ttest_ind(df[g1], df[g2], axis=1)[1]
    if paired:
        stats_frame['t_test_p_value'+app] = ttest_rel(df[g1], df[g2], axis=1)[1]
    stats_frame['Median_'+gname1] = df[g1].median(axis=1)
    stats_frame['Median_'+gname2] = df[g2].median(axis=1)
    stats_frame['Range_'+gname1] = df[g1].max(axis=1) - df[g1].min(axis=1)
    stats_frame['Range_'+gname2] = df[g2].max(axis=1) - df[g2].min(axis=1)
    cols = ['Fold_Change'+app, 'RS_p_value'+app,
            't_test_p_value'+app,
            'Mean_'+gname1, 'Stdev_'+gname1,
            'Median_'+gname1, 'Range_'+gname1,
            'Mean_'+gname2, 'Stdev_'+gname2,
            'Median_'+gname2, 'Range_'+gname2]
    if paired:
        cols = cols.remove('RS_p_value'+app)
    if sort:
        stats_frame = stats_frame.sort('Fold_Change')
    #self.stats_cols = [c for c in cols if c not in self.name_cols+self.mass_cols]
    return stats_frame
    
def outlier_test(lst):
    """
    returns"
    [0] - True of False depending on if there are or are not outliers
    [1] - position of outlyers
    [2] - outlyers numbers
    """
    if type(lst) in [pd.DataFrame().__class__.__name__,pd.Series().__class__.__name__]:
        original = lst.copy()
    else:
        original = lst[:]
    lst.sort()
    length = len(lst)/2
    q1list = lst[0:length]
    q3list = lst[-length:]
    q1 = np.median(q1list)
    q3 = np.median(q3list)
    iqr = q3 - q1
    minimum = q1 - 1.5*iqr
    maximum = q3 + 1.5*iqr
    outliers = list(y for y in lst if y>maximum or y<minimum)
    return outliers
    
def is_outlier(num,lst):
    if type(lst) in [pd.DataFrame().__class__.__name__,pd.Series().__class__.__name__]:
        if num not in lst.values:
            return ValueError('%r must be in the list %r'%(num,lst))
    elif type(lst) == list().__class__.__name__:
        if num not in lst:
            return ValueError('%r must be in the list %r'%(num,lst))
    lst.sort()
    length = len(lst)/2
    q1list = lst[0:length]
    q3list = lst[-length:]
    q1 = np.median(q1list)
    q3 = np.median(q3list)
    iqr = q3 - q1
    minimum = q1 - 1.5*iqr
    maximum = q3 + 1.5*iqr
    outliers = list(y for y in lst if y>maximum or y<minimum)
    if num in outliers:
        return True
    else:
        return False
        
def is_outlier_df(df, cols, overlap = False, outlier_amount = .5):
    """
    returns a df with boonian values of wether or not the value is an outlier 
    in repect to its row
    takes into account the columns selected
    if overlap = Ture the function will return the complete original DataFrame
    if overlap = Flase the fiction will return as DataFrame with only the columns
        chosen
        default is False
    returns [0] the dataframe
    return [1] the columns where more than the outlier_amount are outliers
        eg if outlier_amount = .5 if there are 10 columns,
        if 5 or more are outliers then the column is listed
    """
    new_df = df[cols]
    if overlap == True:
        odf = df.copy()
    elif overlap == False:
        odf = pd.DataFrame(columns=new_df.columns,index=new_df.index)    
    for i in new_df.index:
        lst = new_df.loc[i]
        for i2 in lst.index:
            odf[i2][i] = is_outlier(new_df[i2][i],lst)
    outlier_col = []
    for c in odf.columns:
        lst =  list(odf[c])
        if sum(lst) >= len(lst)/2:
            outlier_col.append(c)
    return odf, outlier_col