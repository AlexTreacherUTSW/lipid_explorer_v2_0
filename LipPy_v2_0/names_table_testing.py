# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 07:53:57 2015

@author: Mike
"""

import os ,sys
import LipPy_v2_0.MS_classes as ms
import Usefull.xlsx_writer as wr 
import time
import LipPy_v2_0.clean_up as cu
import LipPy_v2_0.utilities as util
from collections import OrderedDict
import LipPy_v2_0.Stats.LipidFunctions as lf
import pandas as pd
import numpy as np

import inputs
root = os.getcwd()+os.sep
output_root = root+'Output'+os.sep
write_files = True

input_data = OrderedDict([('pos_ms' , inputs.pos_mode_data),
              ('neg_ms' , inputs.neg_mode_data),
              ('pos_tof', inputs.pos_mode_tof),
              ('neg_tof', inputs.neg_mode_tof),
              ('name_file', inputs.name_table_string),
              ('groups_to_test', inputs.groups_to_test),
              ('omit', inputs.to_omit),
              ('header', inputs.header),
              ('new_dir', inputs.new_directory+os.sep),
              ])
                            
raw_data = ['pos_ms','neg_ms','pos_tof','neg_tof']
name_data = ['name_file']
for i in input_data:
    if i in raw_data and input_data[i] != None:
        input_data[i] = root+'Raw_Data'+os.sep+input_data[i]
    elif i in name_data and input_data[i] != None:
        input_data[i] = root+'Names_Files'+os.sep+input_data[i]
    elif i == 'new_dir':
        input_data[i] = output_root+input_data[i]
    else:
        pass
if (input_data['name_file'] is None) and (input_data['groups_to_test'] is not None):
    raise IOError ('In order to do group testing a Names_File is needed, please add one and rerun')

#remove duplicates in groups_to_test
for l in range(len(input_data['groups_to_test'])):
    input_data['groups_to_test'][l] = tuple(sorted(input_data['groups_to_test'][l]))
input_data['groups_to_test'] = list(set(input_data['groups_to_test']))
#check to make sure all needed files and folders are ther
print 'Checking necessary system files'
cu.check(cu.dir_dict)
print 'System files located and correct'
#imports data into MS_classes
print 'Importing your data'
pos_data = ms.MS_Data.from_raw_data_files(input_data['pos_ms'], input_data['name_file'], mode='+')
neg_data = ms.MS_Data.from_raw_data_files(input_data['neg_ms'], input_data['name_file'], mode='-')
#MS_classes filteres data
print 'Applying initial filter'
pos_filtered = pos_data.all_filters(pos_data.sample_cols)
neg_filtered = neg_data.all_filters(neg_data.sample_cols)
#MS_classes named data
print 'Adding names to your data'
pos_named = pos_filtered.Name_Molecules()
neg_named = neg_filtered.Name_Molecules()