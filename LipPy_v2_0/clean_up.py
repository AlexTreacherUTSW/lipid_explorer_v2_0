# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 14:44:29 2015

@author: Alex
"""


'''
makes sure all files are one the protected_list, and deletes any files or 
folders not on the list
anything in a safe file will not be deleted.
'''

import os,sys
from utilities import copy_file,move_file
import datetime as dt
import shutil
from collections import OrderedDict


Back_Up = False
Clean = False
Check = True
if os.path.exists(os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0')):
    root = os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0')
else:
    root = os.path.join('C:',os.sep,'Users','JMLab1','Lipid_Explorer_v2_0')

'''
Back up will back the folders listed in backup_folders, but only if they are
in the root directory

Clean will make sure that the wanted files are in the location desired,
and delete any other files.

'''
'''
when being used for real, change copy_file to move_file!!!!!
this is so dont have to keep making files to be moved.

'''

def back_up(back_up_folders):
    root = os.getcwd()+os.sep
    if root != 'D:\\Alex Treacher\\play\\testing file checker\\':
        raise IOError('WARNING THIS IS NOT BEING RAN FROM PLAY!!!!!!!!!')
    
    date = dt.datetime.today().strftime("%m%d%Y")
    
    back_up_drive = 'J:'+os.sep +'Lipid_back_up'
    back_up_file = back_up_drive+os.sep+date+'_back_up'
    
    if not os.path.exists(back_up_drive):
        raise IOError('Back up drive not found, please connect and try again.')   
    if not os.path.exists(back_up_file):    
        os.makedirs(back_up_file)
    for f in backup_folders:
        folder = back_up_file + os.sep + date+'_'+f
        if not os.path.exists(folder):    
            os.makedirs(folder)
        cont = os.listdir(root+f)
        if cont: 
            for c in cont:
                copy_file(root+f+os.sep+c,folder)

def check_and_clean(dir_dict):
 
    for v in dir_dict:    
        path = dir_dict[v][0] + os.sep
        actual_contents = os.listdir(path)   
        wanted_contents = dir_dict[v][1]
        for missing in wanted_contents:
            if missing not in actual_contents:
                if '.' not in missing:
                    raise IOError ('The %s folder is missing, please place the missing folder and the needed contents in to %s.' %(missing, path) )
                else:
                    raise IOError ('The %s file is missing, please place the missing file into %s .' %(missing, path))
        
        items_for_removal = [x for x in actual_contents if x not in wanted_contents]
        for rem in items_for_removal:
            try:
                os.remove(path + rem)
            except WindowsError:
                shutil.rmtree(path + rem)
                print path+rem

def check(dir_dict):
        for v in dir_dict:    
            path = dir_dict[v][0] + os.sep
            actual_contents = os.listdir(path)   
            wanted_contents = dir_dict[v][1]
            for missing in wanted_contents:
                if missing not in actual_contents:
                    if '.' not in missing:
                        raise IOError ('The %s folder is missing, please place the missing folder and the needed contents in to %s.' %(missing, path) )
                    else:
                        raise IOError ('The %s file is missing, please place the missing file into %s .' %(missing, path))
                    
dir_dict = OrderedDict([
    ('root_dir' , [root,['.git','LipPy_UI_Backend','LipPy_v2_0','Names_Files','Output','Raw_Data','TOF_data','__init__.py', 'inputs.py']]),
    ('LipPy_UI_Backend',[root+os.sep+'LipPy_UI_Backend', ['cascade menu.py','hello.py','logo.ico','UI.py','UI2.py','UI_.py','UI_play.py']]),
    ('LipPy_v2_0' , [root + os.sep + 'LipPy_v2_0',['Lookups','Molecules','QualityControl','Stats','Templates','Tutorials','__init__.py','clean_up.py','MS_classes.py','Pipeline.py','settings.py','utilities.py','runmarkerview.py']]),
    ('Names_Files' , [root+os.sep + 'Names_Files',[]]),
    ('Output' , [root + os.sep + 'Output',[]]),
    ('Raw_Data' , [root + os.sep + 'Raw_Data',[]]),
    ('TOF_data' , [root + os.sep +'TOF_data',[]]),
    ('Tutorials' , [root + os.sep +'LipPy_v2_0'+ os.sep+'Tutorials',[]]),
    ('Molecules' , [root + os.sep + 'LipPy_v2_0'+os.sep+'Molecules',['__init__.py','Lipids.py','Compounds.py','mass_pairs.py','sim_isotopes.py']]),
    ('Lookups' , [root + os.sep + 'LipPy_v2_0'+os.sep+'Lookups',['.ipynb_checkpoints','add_to_Molecule_Database.py','add_to_standards.py','Cardiolipins.xlsx','Cardiolipin_addition.ipynb','check_Molecule_Database_for_dupelicates.py','Ceramides','Ceramide_Addition.ipynb','Isoprenoid List.xlsx','Isoprenoid_Addition.ipynb','Molecule_Database','names_checker.csv','Standards_DB','standard_mix_1.csv','__init__.py']]),
    ('QualityControl' , [root + os.sep + 'LipPy_v2_0'+os.sep+'QualityControl',['__init__.py','qc.py','Saturation.py']]),
    ('Stats' , [root + os.sep + 'LipPy_v2_0'+os.sep+'Stats',['InformationTheory.py','LipidFunctions.py','Rescaling.py','stats.py','__init__.py']]),
    ('Templates' , [root + os.sep + 'LipPy_v2_0'+os.sep+'Templates',['__init__.py','QC_sheet.html', 'QC_temp.html', 'readme_writer.py']]),
    ])

if __name__ == '__main__':
    if Check:
        check(dir_dict)       
    
    if Back_Up:
        backup_folders = ['Output','Raw_data','Names_files','TOF_data']
        back_up(backup_folders)
    
    if Clean:
        check_and_clean(dir_dict)
