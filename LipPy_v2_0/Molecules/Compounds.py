# -*- coding: utf-8 -*-
"""
Created on Mon Mar 02 14:34:53 2015

@author: Mike Trenfield
Contain building block for testing Lipid methods as well
as functions that take chemical formulae as inputs.
"""
from collections import OrderedDict
from re import search

ATOMS = ['C', 'H', 'D', 'O', 'N', 'P', 'S', 'F']
CHOLESTEROL = {'C': 27, 'H': 46, 'O': 1}
GLYCEROL = {'C' : 3, 'H' : 8, 'O' : 3}
HYDROXYL = {'O' : 1, 'H' : 1}
AMMONIA = {'N' : 1, 'H' : 3}
AMMONIUM = {'N' : 1, 'H' : 4}
TRIOLEIN_MS1 = {'C': 57, 'H': 108, 'N': 1, 'O': 6}
TRIOLEIN_MS2 = {'C': 39, 'H': 70, 'O': 4}
METHYLENE = {'C' : 1, 'H' : 2}


MASSES = OrderedDict([('C', 12.00),
                      ('H', 1.0079),
                      ('O', 15.999),
                      ('N', 14.007),
                      ('P', 30.97376200),
                      ('S', 32.06),
                      ('D', 2.014102),
                      ('F', 18.998403),
                      ])
                      
def mass(chem_formula, Kendrick=False):
    """
    Given a chemical formula, returns the mass of the molecule.
    Parameters
    ----------  
    chem_formula : dict or str
        A chemical formula.
    Kendrick: book, default False
        Whether to return the IUPAC mass or the Kendrick mass.
    
    Returns
    --------
    A float corresponding to the mass.
    
    Examples
    --------
    >>> mass('CH2')
    14.0158
    >>> mass({'C': 1, 'H': 2})
    14.0148
    >>> mass('CH2', Kendrick=True)
    14.0
    References
    --------
    http://en.wikipedia.org/wiki/Kendrick_mass
    """
    if isinstance(chem_formula, str):
        return mass(form_from_str(chem_formula), Kendrick=Kendrick)
    IUPAC = sum(map(lambda x: MASSES[x]*chem_formula[x], chem_formula.keys()))
    if Kendrick:
        return KendrickMass(IUPAC)
    else:
        return IUPAC
    
def KendrickMass(IUPAC_mass):
    """
    Given a mass in IUPAC units, returns the Kendrick mass.
    ----------  
    IUPAC_mass : numeric
        An IUPAC mass.
    
    Returns
    --------
    A float corresponding to the Kendrick mass.
    
    Examples
    --------
    >>> mass(14.0158)
    14.0
    References
    --------
    http://en.wikipedia.org/wiki/Kendrick_mass
    """
    return (14.0 / mass('CH2')) * IUPAC_mass
    
def KM_Defect(chem_formula_or_mass):
    """
    Given chemical formula, returns the Kendrick mass defect.
    ----------  
    chem_formula : dict or str
        A chemical formula.
    
    Returns
    --------
    The Kendrick mass defect.
    
    Examples
    --------
    >>> KM_Defect('CH2')
    0
    References
    --------
    http://en.wikipedia.org/wiki/Kendrick_mass
    """
    ###Yes, this could be done more cleanly with the modulus operator, but the code
    ###I wrote it this way because this is closest to the formula on wikipedia,
    ###which I assume is how chemists think about it.
    if isinstance(chem_formula_or_mass, (str, dict)):
        return mass(chem_formula_or_mass) - int(mass(chem_formula_or_mass, Kendrick=True))
    elif isinstance(chem_formula_or_mass, float):
        return chem_formula_or_mass - int(chem_formula_or_mass)
        
def form_from_str(ch_form_string):
    """
    Converts a chemical formula from string form to dictionary form.  Order is
    enforced by the ATOMS list defined in Compounds.py.
    """
    if isinstance(ch_form_string, dict):
        return ch_form_string
    formula = OrderedDict()
    for A in ATOMS:
        ###https://xkcd.com/208/
        num = search(A+'(\d+)', ch_form_string)
        if A in ch_form_string:
            if num is not None:
                formula[A] = int(num.group(1))
            else:
                formula[A] = 1
    return formula
    
def string_from_form(form):
    """
    Converts a chemical formula from dictionary form to string form.  Order is
    enforced by the ATOMS list defined in Compounds.py.
    """

    #MS1dict = form[0]
    #MS2dict = form[1]
    #MS1str = ''
    #MS2str = ''
    #for key in ATOMS:
    #    if key in MS1dict.keys():
    #        MS1str+= str(key)+str(MS1dict[key])
    #    if key in MS2dict.keys():
    #        MS2str+=  str(key)+str(MS2dict[key])
    #return MS1str,MS2str
        
    ###YOLO
    return ''.join([k+(form[k] != 1)*str(form[k])
                    for k in ATOMS if k in form.keys()])
                      

if __name__ == '__main__':    
    assert round(mass(TRIOLEIN_MS1)) == 903
    assert round(mass(TRIOLEIN_MS2)) == 603
    assert round(mass(AMMONIUM)) == 18