# -*- coding: utf-8 -*-
"""
Created on Thu Dec 04 13:41:21 2014

@author: Mike
"""
from re import search
import os, sys
from pandas import Series
from Compounds import *
from mass_pairs import *

ISO_THRESH = 200

class Lipid(object):   
    """
    Represents a lipid.  Has attributes representing features such as number of
    carbons, number of double bonds, taxonomy.  Lipid constructor is not called
    directly, but serves as a base class that creates a template for other lipids.
    The safest way to initialize a lipid is with the from_group static_method.
    
    Parameters
    ----------
    name : Name of the lipid.  Must be in a format in which features are possible
        to read off using string splitting or regular expressions.
    mode : string '+' or '-'
        The mode that the lipid has been read in.  This may affect feature reading.
    mass_pair : t (defined in mass_pairs.py) or None
        The mass pair associated with the lipid.  Used for generating isotopes.
        If None, isotope methods will throw errors.
    
    Examples
    --------
    >>> triolein = TAG('TAG(54:3) NL-18:1', '+', mass_pair=MassPair(902, 603))
    >>> triolein = Lipid.from_group('TAG', 'TAG(54:3) NL-18:1', '+', mass_pair=MassPair(902, 603))
    """

    
    def __init__(self, name, mode, mass_pair=None):
        
        self.name = name
        self.group = self.__class__.__name__
        self.mode = mode
        self.accurate_formulas = False
        if isinstance(mass_pair, (list, tuple)):
            self.mass_pair = MassPair.from_list(mass_pair)
        elif isinstance(mass_pair, MassPair):
            self.mass_pair = mass_pair
        elif mass_pair is None:
            self.mass_pair = mass_pair
        else:
            msg = 'Argument mass_pair must be of type list, tuple, NoneType, or LipPy.MassPair.  Passed argment is of type: '+str(type(mass_pair))
            raise TypeError(msg)
        try:
            self.get_features()
        except:
            if self.name != 'Unknown':
                print 'Could not get features for '+self.name
            
    @classmethod
    def __eq__(cls, other):
        return str(cls) == str(other)
            
    @classmethod
    def __float__(self):
        raise ValueError
        
    def __len__(self):
        return len(self.name)
        
    @staticmethod
    def from_name(lipid_name, mode=None, mass_pair=None):
        if isinstance(lipid_name, Lipid):
            print 'from Lipid'
            print lipid_name.name, lipid_name.mode
            return Lipid.from_name(str(lipid_name), lipid_name.mode)
        name = lipid_name
        if ' or '  in name or 'unknown' in name.lower():
            return Unknown(lipid_name, mode)
        if not any(char.isdigit() for char in lipid_name) and len(lipid_name) > 3:
            return FAHFA(lipid_name, mode)
        if 'lyso' in lipid_name:
            scls = dict(zip(Lyso.get_descendants(), 
                            Lyso.get_descendants(as_str=False)))
            name = lipid_name.replace('lyso', 'L')
        else:
            scls = dict(zip(Lipid.get_descendants(),
                            Lipid.get_descendants(as_str=False)))
        best_match = 0
        winner = None
        for key in scls.keys():
            assert key == scls[key].__name__
            if key.lower() in name.lower():
                if 'standard' not in name.lower():
                    ##Pick the longest class name that the string starts with
                    if name.lower().startswith(key.lower()):
                        score = key
                    else:
                        score = 0
                    if score > best_match:
                        best_match = score
                        winner = key
#                else:
#                    return Standard(name, mode, mass_pair=mass_pair)
        if winner is not None:
            return scls[winner](name, mode, mass_pair=mass_pair)
        else:
            print 'Lipid '+name+' could not be named.'
            return Unknown(lipid_name, mode, mass_pair=mass_pair)
        
    @staticmethod
    def from_group(group_name, name, mode, mass_pair=None):
        """
        Constructs a lipid of type group_name.  In general, this is the most secure
        and reliable way to initiate a lipid object of the correct type.
        
        Parameters
        ----------
       group_name : string
            The name of the lipid group you want to create and instance of.
        name : string
            Name of the lipid.  Must be in a format in which features are possible
            to read off using string splitting or regular expressions.
        mode : string '+' or '-'
            The mode that the lipid has been read in.  This may affect feature reading.
        mass_pair : MassPair (defined in mass_pairs.py) or None
            The mass pair associated with the lipid.  Used for generating isotopes.
            If None, isotope methods will throw errors.
        
        Examples
        --------
        >>> triolein = Lipid.from_group('TAG', 'TAG(54:3) NL-18:1', '+', mass_pair=MassPair(902, 603))
        
        
        Nomenclature
        for standards the name shoud be 'standardname'_Name eg SPLASH_PC(33:1){d7}
        
        """

        scls = dict(zip(map(lambda x : x.__name__, 
                            Lipid.get_descendants(as_str=False)),
                            Lipid.get_descendants(as_str=False)))
        for key in scls.keys():
            assert key == scls[key].__name__
            if key == group_name.strip(): ##strip whitespace just in case
#                if 'standard' not in group_name.lower():
                return scls[key](name, mode, mass_pair=mass_pair)
#                else:
#                    return Standard(name, mode, mass_pair=mass_pair)
                
        print group_name+" is not a recognized lipid group."
        return Unknown(name, mode, mass_pair=mass_pair)
        
    def __repr__(self):
        return self.name.__repr__()
      
    def __str__(self):
        return self.name.__str__()
        
    def __hash__(self):
        return hash(self.name)
    
    def get_standard(self, observation):
        """
        Gets the standard associated with the lipid in the observation.
        
        Parameters
        ----------
        observation : MS_data
            The MS_data object corresponding to the dataset in which you want 
            to find the standard to be used for the current lipid.
        
        Returns
        --------
        standard :
            the standard to be associated with the lipid in the current observation.
        """
        lineage = self.get_lineage()
        lineage.reverse()
        standard = observation.standards['Input'][self.mode].ix['default'].Name
        found = False
        i = 0
        while i < len(lineage) and not found:
            node = lineage[i]
            i += 1
            if node in observation.standards['Input'][self.mode].index and node != 'default':
                standard_name = observation.standards['Input'][self.mode].ix[node].Name
                if standard_name in observation.standards['Observed'][self.mode].index:
                    standard = standard_name
                    found = True
        if not standard in observation.standards['Observed'][self.mode].index:
            standard = observation.standards['Observed'][self.mode].mean(axis=1).idxmax()
        if 'Lipids' in str(standard.__class__):
            standard = standard.name                   
        return standard

    def get_features(self):
        reg_ex = {'Carbons' : '\((\d+):', 'Double_Bonds' : ':(\d+)[\)e]'}
        self.features = {}
        for key in reg_ex:
            try:
                self.features[key] = int(search(reg_ex[key], self.name).group(1))
            except:
                self.features[key] = 'Undefined'
            setattr(self, key.lower(), self.features[key])
        return self.features
        
    def probability_matrix(self, N=1, ratio=True, RANGE=ISO_RANGE):
        form1, form2 = self.formulas()
        if self.is_fragmented():
            dist = N*MSMS_isotope_probabilities(form1, form2, ratio=ratio)
        else:
            dist = N*single_molecule_isotope_probabilities(form1, as_ratio=ratio)[:RANGE]
        return dist
        
    def which_isotopomer(self, other):
        if isinstance(other, Lipid):
            return self.mass_pair.which_isotopomer(other.mass_pair)
        elif isinstance(other, (MassPair, str, list, tuple)):
            return self.mass_pair.which_isotopomer(other)
            
    def ms1_mass(self):
        return sum(mul_dict(self.formulas()[0],MASSES).values())
        
    def ms2_mass(self):
        return sum(mul_dict(self.formulas()[1],MASSES).values())    
    
    def masses(self):
        return self.ms1_mass(), self.ms2_mass()
        
    def ms1_formula(self):
        return self.formulas()[0]
    
    def ms2_formula(self):
        return self.formulas()[1]
        
    def check_masses(self):
        '''
        if (not self.mass_pair[0]-.3 < self.ms1_mass() < self.mass_pair[0]+.3) and \
            (not self.mass_pair[1]-.3 < self.ms2_mass() < self.mass_pair[1]+.3):
                return 'neither mass matches'
        elif not self.mass_pair[0]-.3 < self.ms1_mass() < self.mass_pair[0]+.3:
            return 'MS1 mass does not match'
        elif not self.mass_pair[1]-.3 < self.ms2_mass() < self.mass_pair[1]+.3:
            return 'MS2 mass does not match'
        else:
            return 'masses match within .3'
        '''
        if (not self.mass_pair[0]-.5 < self.ms1_mass() < self.mass_pair[0]+.5) and \
            (not self.mass_pair[1]-.5 < self.ms2_mass() < self.mass_pair[1]+.5):
                return 'neither mass matches'
        elif not self.mass_pair[0]-.5 < self.ms1_mass() < self.mass_pair[0]+.5:
            return 'MS1 mass does not match'
        elif not self.mass_pair[1]-.5 < self.ms2_mass() < self.mass_pair[1]+.5:
            return 'MS2 mass does not match'
        else:
            return 'masses match within .5'
        
    
    def is_fragmented(self):
        """
        Returns True if the ms1 fragments in the mass spectrometer.
        Returns False otherwise.  A mass pair is said to be fragmented
        if the ms2 is more than 14 mass units smaller than the ms1.
        """
        return self.mass_pair.is_fragmented()
            
            
    def mass_pair_dist(self, **kwargs):
        """
        Predicts the probability distribution of isotopes.
        
        Parameters
        ----------
        as_ratio: bool, default True
            Whether or not to return abundances as relative abundances to the 
            [M0, M0] pair or as absolute probabilities.
        N: int, default 1
            The factor by which to multiply the calculated abundances.  If as_ratio
            is set to True and N is the intensity of the [M0, M0] pair, this corresponds
            to calculating the expected counts for each isotope pair.
        THRESH: float, default 200
            Predictions less than thresh will not be returned.
        
        RANGE: float, default 5
            Number of incrementations in MS1 that will be predicted
        
        Returns
        --------
        predicted :
            A Pandas series object with MassPair objects forming the index that
            contains the isotope distribution for the lipid in question.
        """
        as_ratio = kwargs.get('as_ratio')
        if as_ratio is None:
            as_ratio = True
        N = kwargs.get('N')
        if N is None:
            N = 1
        if N > 1:
            THRESH = kwargs.get('THRESH')
            if THRESH is None:
                THRESH = ISO_THRESH
        else:
            THRESH = 0
        RANGE = kwargs.get('RANGE')
        if RANGE is None:
            RANGE = ISO_RANGE
        from_zero = bool(kwargs.get('from_zero'))
        if self.is_fragmented():
            mp = dict(marginal_mass_pairs(self.formulas(), self.gen_isotopes()))
            mp = Series(mp)
            if as_ratio:
                mp = mp / mp[mp.first_valid_index()]
            predicted = N*mp
            predicted = predicted[predicted > THRESH]
        else:
            form = self.formulas()[0]
            predicted = N*single_molecule_isotope_probabilities(form, as_ratio=as_ratio)[:RANGE]
            predicted = Series(predicted, index=self.gen_isotopes())
            predicted = predicted[predicted > THRESH]
        if from_zero:
            predicted.index = np.array(predicted.index) - predicted.index[0]
        return predicted
        
        
    def gen_isotopes(self, RANGE=ISO_RANGE):
        """
        Generates a list of the lipid's isotopes.
        
        Parameters
        ----------  
        RANGE: float, default 5
            Number of incrementations in MS1 that will be returned
        
        Returns
        --------
        A list of isotopes of the given lipid
        """
        return self.mass_pair.gen_isotopes(RANGE=RANGE)
        
    def check_overlap(self, other, RANGE=ISO_RANGE):
        """
        Checks if the lipid will have overlapping spectra (in the sense that 
        they share an isotope) with other..
        
        Parameters
        ----------  
        other: MassPair or Lipid
            The mass pair or lipid that will 
        RANGE: float, default 5
            Maximum separation between the ms1's before they will be said not to
            overlap.
        Returns
        --------
        True if self and other have overlapping spectra, False otherwise.
        """
        if hasattr(other, mass_pair):
            return self.mass_pair.check_overlap(other.mass_pair, RANGE=RANGE)
        elif isinstance(other, MassPair):
            return self.mass_pair.check_overlap(other, RANGE=RANGE)
        else:
            raise ValueError('Argument must be of instance MassPair or have attribute mass_pair.')
        
    def check(self):
        x = self.check_masses()
        if x != 'masses match within .5':
            raise ValueError('masses on %s, %s dont match'%(self.name, self.mode))
        self.mass_pair_dist()
        
    @classmethod
    def get_lineage(cls, as_str=True):
        """
        Returns the lineage/taxonomy of a given lipid.  This is a class method,
        so it can be called either from an instance or from the class itself.
        See the examples section for an example.
        
        Parameters
        ----------  
        as_str : bool, default True
            Whether to return a list of strings or a list of of class objects.
            A list of class objects will be returned if False.
        Returns
        --------
        A list corresponding to the path in the lipid tree that leads to the given
        lipid.
        Examples
        --------
        >>> triolein = Lipid.from_group('TAG', 'TAG(54:3) NL-18:1', '+', mass_pair=MassPair(902, 603))
        >>> triolein.get_lineage()
        ['Lipid', 'Neutral_Lipid', 'TAG']
        >>> TAG.get_lineage()
        ['Lipid', 'Neutral_Lipid', 'TAG']
        """
        if cls == 'Unknown':
            return 'Unknown'
        mro = cls.mro()
        mro.reverse()
        if as_str:
            return map(lambda x : x.__name__, mro[1:])
        else:
            return mro[1:]
        
    @classmethod
    def dir_string(cls):
        string = reduce(lambda x, y : str(x)+os.sep+str(y), cls.get_lineage())
        string += os.sep
        return string
        
    @classmethod
    def get_subclasses(cls, lipid_group=None, as_str=True):
        if lipid_group is not None:            
            lg = str(lipid_group)
            if not lg in Lipid.get_descendants():
                print("Could not find lipid "+lg+'.')
                return None
            else:
                return eval(lg).get_subclasses(as_str=as_str)
        scls = cls.__subclasses__()
        if as_str:
            scls = map(lambda x : x.__name__, scls)
        return scls
        
    @classmethod
    def has_subclasses(cls, lipid_group=None):
        if lipid_group is not None:            
            lg = str(lipid_group)
            if not lg in Lipid.get_descendants():
                print("Could not find lipid "+lg+'.')
                return None
            else:
                return eval(lg).has_subclasses()
        if len(cls.get_subclasses()) > 0:
            return True
        else:
            return False
        
    @classmethod    
    def get_descendants(cls, lipid_group=None, as_str=True):
        if lipid_group is not None:            
            lg = str(lipid_group)
            if not lg in Lipid.get_descendants():
                print("Could not find lipid "+lg+'.')
                return None
            else:
                return eval(lg).get_descendants(as_str=as_str)
        desc = cls.__subclasses__()+[g for s in cls.__subclasses__() 
                                     for g in s.get_descendants(as_str=False)]
        if as_str:
            names = map(lambda x : x.__name__, desc)
            return names
        else:
            return desc
        
    @classmethod
    def get_type(cls):
        try:
            lipid_type = cls.get_lineage()[-2]
        except IndexError:
            lipid_type = cls.get_lineage()[-2]
        return lipid_type
        
    def get_mass(self):
        try:        
            forms = self.formulas()
            return map(mass, forms)
        except:
            return [0, 0]            
            
    def formula_string(self):
        """
        Returns a list of chemical formulas written as strings.
        
        Parameters
        ----------  
        None
        
        Returns
        --------
        [ms1, ms2] :
            A list containing the string for the chemical formula for the ms1
            and the chemical formula for the ms2.  Some of these are exact and
            some will be approximations based of the divide-by-fourteen rule.
        Examples
        --------
        >>> triolein = Lipid.from_group('TAG', 'TAG(54:3) NL-18:1', '+', mass_pair=MassPair(902, 603))
        >>> triolein.formula_string()
        ['C57H108O6N', 'C39H70O4']
        """
        #try:
        form = self.formulas()
        ms1, ms2 = map(string_from_form, form)
        return [ms1, ms2]
        #except:
            #return 'NA'
            
    def formulas(self, masses=None):
        if masses is None:
            if hasattr(self, 'mass_pair'):
                return self.formulas(masses=self.mass_pair)
        else:
            ms1 = {}
            ms2 = {}
            ms1['C'] = int(int(masses[0]) / 14.0)
            ms1['H'] = 2*ms1['C'] + (int(masses[0]%14))
            ms2['C'] = int(int(masses[1]) / 14.0)
            ms2['H'] = 2*ms2['C'] + (int(masses[1])%14)
            return [ms1, ms2]
            
    def has_isotope(self, other):
        if isinstance(other, MassPair):
            return self.mass_pair.has_isotope(other)
        else:
            if hasattr(other, 'mass_pair'):
                return self.mass_pair.has_isotope(other.mass_pair)
            else:
                try:
                    return self.mass_pair.has_isotope(MassPair(other))
                except AttributeError:
                    err = 'Argument "other" must be of type MassPair or Lipid.'
                raise TypeError(err)
                
    def is_isotope(self, other):
        err = 'Argument other must have attribute mass pair or be valid argument for MassPair constructor.'
        if hasattr(other, 'mass_pair'):
            return other.mass_pair.has_isotope(self.mass_pair)
        elif isinstance(other, MassPair):
            return other.has_isotope(self.mass_pair)
        elif isinstance(other, (list, tuple)):
            return MassPair(other).has_isotope(self.mass_pair)
        elif isinstance(other, str):
            try:
                return MassPair(other).has_isotope(self.mass_pair)
            except ValueError:
                raise ValueError(err)
        else:
            raise ValueError(err)
        

class Neutral_Lipid(Lipid):
    
        
    def get_features(self):
        super(Neutral_Lipid, self).get_features()
        if 'MAG' in self.get_lineage():
           reg_ex = {'Carbons' : '\((\d+):', 'Double_Bonds' : ':(\d+)\)'}
           self.features = {key : int(search(reg_ex[key], self.name).group(1)) for key in reg_ex.keys()}
        else:
            NL = self.name.split('NL-')[1]
            self.features['Fatty_Acid'] = NL
            self.features['FA_Carbons'] = int(NL.split(':')[0])
            self.features['FA_Double_Bonds'] = int(NL.split(':')[1])
            
    def get_type(self):
        return 'Neutral_Lipid'
        
class Polar_Lipid(Lipid):

    def get_type(self):
        return 'Polar_Lipid'
        
class Endocyne(Lipid):
    pass

class Isotope(Lipid):
    pass
        
class Other(Lipid):
    pass
        

####NEUTRAL LIPIDS

class TAG(Neutral_Lipid):

    def __init__(self, name, mode, mass_pair):
        Neutral_Lipid.__init__(self, name, mode, mass_pair)
        self.backbone = {'C':3,'H':5}
        self.adduct = {'N':1,'H':4}
        self.accurate_formulas = True
    
    def R1_formula(self):
        FA = {'C':self.features['FA_Carbons']}
        FA['H'] = 2*self.features['FA_Carbons']-2*self.features['FA_Double_Bonds']-1
        FA['O'] = 2
        return FA
        
    def NL_formula(self):
        NL = add_dict(self.R1_formula(),self.adduct)
        return NL
    
    def other_chains_formula(self):
        carbons = self.features['Carbons']-self.features['FA_Carbons']
        db = self.features['Double_Bonds']-self.features['FA_Double_Bonds']
        form = {'C':carbons}
        form['H'] = 2*carbons - 2*db -2
        form['O'] = 4
        return form
        
    def formulas(self):
        MS1 = add_dict(self.backbone,self.R1_formula())
        MS1 = add_dict(MS1,self.other_chains_formula())
        MS1 = add_dict(MS1,self.adduct)
        MS2 = sub_dict(MS1, self.NL_formula())
        return MS1, MS2

    def MS2_features(self, as_str = False):
        """
        returns No C and DB in the MS2 as a dict or string 
        """
        dct = {}
        dct['Carbons'] = self.features['Carbons'] - self.features['FA_Carbons']
        dct['Double_Bonds'] = self.features['Double_Bonds'] - self.features['FA_Double_Bonds']
        if as_str == False:
            return dct
        else:
            return '('+str(dct['Carbons'])+':'+str(dct['Double_Bonds'])+')'
        
    def MS1(self):
        return self.formulas()[0]
        
    def MS2(self):
        return self.formulas()[1]

        
class DAG(Neutral_Lipid):

    def __init__(self, name, mode, mass_pair):
        Neutral_Lipid.__init__(self, name, mode, mass_pair)
        self.backbone = {'C':3,'H':6,'O':1}
        self.adduct = {'N':1,'H':4}
        self.accurate_formulas = True
    
    def R1_formula(self):
        '''
        R1 will be the FA lost in NL
        '''
        FA = {'C':self.features['FA_Carbons']}
        FA['H'] = 2*FA['C']-2*self.features['FA_Double_Bonds']-1
        FA['O'] = 2
        return FA
        
    def R2_formula(self):
        carbons = self.features['Carbons']-self.features['FA_Carbons']
        db = self.features['Double_Bonds']-self.features['FA_Double_Bonds']
        FA = {'C':carbons}
        FA['H'] = 2*FA['C']-2*db-1
        FA['O'] = 2
        return FA

    def MS2_features(self, as_str = False):
        """
        returns No C and DB in the MS2 as a dict or string 
        """
        dct = {}
        dct['Carbons'] = self.features['Carbons'] - self.features['FA_Carbons']
        dct['Double_Bonds'] = self.features['Double_Bonds'] - self.features['FA_Double_Bonds']
        if as_str == False:
            return dct
        else:
            return '('+str(dct['Carbons'])+':'+str(dct['Double_Bonds'])+')'
        
    def chain_formula(self):
        return self.R1_formula(),self.R2_formula()
    
    def chains_formula(self):
        return add_dict(self.R1_formula(),self.R2_formula())

    def NL_formula(self):
        form = add_dict(self.R1_formula(),self.adduct)
        return form

    def formulas(self):     
        MS1 = add_dict(self.backbone, self.chains_formula())
        MS1 = add_dict(MS1, self.adduct)
        MS2 = sub_dict(MS1, self.NL_formula())
        return MS1, MS2

    def MS1(self):
        return self.formulas()[0]
        
    def MS2(self):
        return self.formulas()[1]        

class MAG(Neutral_Lipid):
    
    def __init__(self, name, mode, mass_pair):
        Neutral_Lipid.__init__(self, name, mode, mass_pair)
        self.backbone = {'C':3,'H':7,'O':3}
        self.adduct = {'N':1,'H':4}
        self.accurate_formulas = True
    
    def chain_formula(self):
        form = {'C':self.carbons}
        form['H'] = 2*form['C']-2*self.double_bonds-1
        form['O'] = 1
        return form
    
    def formulas(self):
        MS1 = add_dict(self.backbone,self.chain_formula())
        MS1 = add_dict(MS1,self.adduct)
        nh3 = {'N':1,'H':3}
        water = {'O':1,'H':2}
        NL = add_dict(nh3,water)
        MS2 = sub_dict(MS1,NL)
        return MS1,MS2

    def MS1(self):
        return self.formulas()[0]
        
    def MS2(self):
        return self.formulas()[1]

###POLAR LIPIDS
class FFA(Polar_Lipid):
    
    def __init__(self, name, mode, mass_pair):
        Polar_Lipid.__init__(self, name, mode, mass_pair)
        self.adduct = {'H':-1}
        self.accurate_formulas = True
        if self.mode == '+':
            print ('WARNIGN!!!! Free fatty acids only aloud in neg mode!')

    def get_features(self):
        string_2_manip = self.name.split('a')[1]
        num_carbons = int(string_2_manip.split(':')[0])
        num_db = int(string_2_manip.split(':')[1])
        features = {}
        features['Carbons'] = num_carbons
        features['Double_Bonds'] = num_db
        self.features = features
        
    def chain_formula(self):
        form = {'C':self.features['Carbons']}
        form['H'] = 2*form['C']-2*self.features['Double_Bonds']
        form['O'] = 2
        return form
        
        
    def formulas(self):
        MS1 = add_dict(self.chain_formula(),self.adduct)
        MS2 = MS1
        return MS1,MS2

class CL(Polar_Lipid):
    pass

        
class Lyso(Polar_Lipid):
    
    def __init__(self, name, mode, mass_pair):
        Polar_Lipid.__init__(self, name, mode, mass_pair)
        self.link = {'C':3, 'H':6, 'O':1}
        
    def chain_formula(self):            
        '''
        return the fomula for the chain
        '''
        noC = self.features['Carbons']
        nodb = self.features['Double_Bonds']
        form = {'C' : noC, 'H' : 2*(noC-nodb)-1, 'O' : 2}
        if 'e' in self.name:
            form['O'] = form['O'] - 1
            form['H'] = form['H'] + 2
        if 'splash_standard' in self.group.lower():
            form['D'] = self.deuteriumNo
            form['H'] -= self.deuteriumNo
        return form            
    
    def chain_mass(self):
        '''
        returns the mass for the chain
        '''
        return sum(mul_dict(self.chain_formula(),MASSES).values())

    def masses(self):
        MS1 = sum(mul_dict(self.formulas()[0],MASSES).values())
        MS2 = sum(mul_dict(self.formulas()[1],MASSES).values())
        return MS1, MS2
    
    def print_backbone(self):
        return backbone

class LPC(Lyso):
    
    def __init__(self, name, mode, mass_pair):
        Lyso.__init__(self, name, mode, mass_pair)
        self.classifier =  {'C':5,'H':13,'N':1, 'P':1, 'O':4}
        self.backbone = add_dict(self.classifier, self.link)
        self.accurate_formulas = True
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'C':-1,'H':-3}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        if '{d' in self.name:
            self.deuteriumNo = self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))

    def formulas(self):
        if self.mode == '+':
            '''
            [M+H]
            careful!!!! this molecule gets protinated after begin split so it 
            has an additional H in the MS2
            '''
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1,self.adduct)
            MS2 = add_dict(self.classifier, self.adduct)
            #the MS2 then picks up a H according to RCMurphy, Tandem MassSpec of Lipids. 
            MS2 = add_dict(MS2,{'H':1})
            return MS1, MS2
        elif self.mode == '-':          
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = self.chain_formula()
            return MS1, MS2

class LPE(Lyso):
    
    def __init__(self, name, mode, mass_pair):
        Lyso.__init__(self, name, mode, mass_pair)
        self.classifier =  {'C':2,'H':7,'N':1, 'P':1, 'O':4}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of %s and rerun'%(self.name))
        self.accurate_formulas = True
    
    def formulas(self):
        if self.mode == '+':
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            NL = add_dict(self.classifier,self.adduct)
            MS2 = sub_dict(MS1 , NL)
            return MS1, MS2
        elif self.mode == '-':
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = self.chain_formula()
            return MS1, MS2
        else:
            print 'Need a mode to find formulas for %s'%(self.name)


class LPA(Lyso):
    
    def __init__(self, name, mode, mass_pair):
        Lyso.__init__(self, name, mode, mass_pair)
        self.classifier =  {'O':4, 'H':2, 'P':1}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True
            
    def formulas(self):
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = sub_dict(MS1 , self.classifier)
            return MS1, MS2        
        elif self.mode == '-':
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = self.chain_formula()
            return MS1, MS2        

class LPI(Lyso):
    
    def __init__(self, name, mode, mass_pair):
        Lyso.__init__(self, name, mode, mass_pair)
        self.classifier = {'C':6, 'H':12, 'P':1, 'O':9}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True
            
    def formulas(self):               
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            NL = add_dict(self.classifier, self.adduct)
            MS2 = sub_dict(MS1,NL)
            return MS1, MS2        
        elif self.mode == '-':
            self.backbone['H'] = self.backbone['H'] -1
            MS1 = add_dict(self.chain_formula(),self.backbone)
            MS2 = self.chain_formula()
            return MS1, MS2        
        
class PL(Polar_Lipid):
    '''
    class for phospho-lipids (PL)    
    this complete molecule is made of:
        the classifier (diffrent for each class)
        the link (the same for each class)
        the chains (unique to each lipid)
    the backbone is the link and classifier together                                         
    '''
    def __init__(self, name, mode, mass_pair):
        Polar_Lipid.__init__(self, name, mode, mass_pair)
        self.link = {'C':3, 'H':5}    
    
    def get_features(self):
        '''
        returns the number of carbons and double bonds on the chains
        '''
        if self.mode == '+':
            reg_ex = {'Carbons' : '\((\d+):', 'Double_Bonds' : ':(\d+)[\)e]'}
        elif self.mode == '-':
            reg_ex = {'Carbons' : self.__class__.__name__+'(\d+):', 'Double_Bonds' : ':(\d+)[()]'}
        self.features = {}
        for key in reg_ex:
            try:
                self.features[key] = int(search(reg_ex[key], self.name).group(1))
            except:
                self.features[key] = 'Undefined'
            setattr(self, key.lower(), self.features[key])
        return self.features

    def get_type(self):
        return 'PL'
        
    def chain_features(self, as_str = False):
        '''
        returns the number of carbons and double bonds in the R1 and R2 chains respectively
        '''
        if self.mode == '+':
            return None
        elif self.mode == '-':
            name = self.name         
            if 'or' in name:
                name = name.split('or')[0]
            R1str = search('\((.*)\,', name).group(1)
            R1C = int(R1str.rsplit(':', 1)[0])
            R1db = int(R1str.rsplit(':', 1)[1])
            R1_features = OrderedDict([('noC' , R1C),
                              ('nodb' , R1db)])
            R2str = search('\,(.*)\)', name).group(1)
            R2C = int(R2str.rsplit(':', 1)[0])
            R2db = int(R2str.rsplit(':', 1)[1])
            R2_features = OrderedDict([('noC' , R2C),
                              ('nodb' , R2db)])
            if not as_str:
                return R1_features, R2_features
            else:
                feat_str = str(R1C)+':'+str(R1db)+','+str(R2C)+':'+str(R2db)
                return feat_str
    
    def R1_features(self):
        if self.mode == '+':
            print 'Warning, can not get R1 features from a %s in pos mode' %(self.group)
            return None
        else:
            return self.chain_features()[0]
            
    def R2_features(self):
        if self.mode == '+':
            print 'Warning, can not get R1 features from a %s in pos mode' %(self.group)
            return None
        else:
            return self.chain_features()[1]
            
    def link(self):
        '''
        section of the lipid that links the backbone to the 2 chains,
        is the same for PA, PC, PE, PI, PG, PS
        '''
        form = {'C':3, 'H':5}
        return form
        
    def chain_formula(self):
        '''
        returns the chemical formula for the R1 and R2 chains respectively 
        '-' mode only
        '''
        
        if self.mode != '-':
            raise TypeError ('mode must be \'-\' to use this' )
        
        if self.chain_features is not None:
            R1C = self.chain_features()[0]['noC']
            R1db = self.chain_features()[0]['nodb']
            R2C = self.chain_features()[1]['noC']
            R2db = self.chain_features()[1]['nodb']
            R1 = {'C' : R1C, 'H' : 2*R1C - 2*R1db - 1, 'O' :2}
            R2 = {'C' : R2C, 'H' : 2*R2C - 2*R2db - 1, 'O' :2}
            if 'splash_standard' in self.group.lower() and '{d' in self.name:
                for chain in (R1,R2):
                    if chain['C'] == 18:
                        chain['H'] -= self.deuteriumNo
                        chain['D'] = self.deuteriumNo
            return R1, R2

    def chains_formula(self):
        '''
        returns the chemical formula for the chains combined
        mode must be selected to use this function, if not selected it will
        return an error
        '''
        
        if self.mode == '+':
            noC = self.features['Carbons']
            nodb = self.features['Double_Bonds']
            form = {'C' : noC, 'H' : 2*(noC-nodb-1), 'O' : 4}
            if 'e' in self.name:
                form['O'] = form['O'] - 1
                form['H'] = form['H'] + 2
            if 'splash_standard' in self.group.lower() and '{d' in self.name:
                form['H'] -= self.deuteriumNo
                form['D'] = self.deuteriumNo
            return form
        elif self.mode == '-':
            R1 = self.chain_formula()[0]
            R2 = self.chain_formula()[1]
            return add_dict(R1,R2)
        else:
            raise TypeError ('Mode must be selected to use this function')
        
    def R1_mass(self):
        '''
        returns the mass of the R1 chain from the chemical formula made.
        note this might be a little off of the measured mass from the mass pair.
        '-' mode only
        '''
        
        if self.mode == '+':
            raise TypeError ('mode must be \'-\' to use this' )
        form = self.chain_formula()[0]
        mass = sum(mul_dict(form,MASSES).values())
        return mass
        
    def R2_mass(self):
        '''
        returns the mass of the R2 chain from the chemical formula made.
        note this might be a little off of the measured mass from the mass pair.
        '-' mode only
        '''
        
        if self.mode == '+':
            raise TypeError ('mode must be \'-\' to use this' )
            
        form = self.chain_formula()[1]
        mass = sum(mul_dict(form,MASSES).values())
        return mass
        
    def MS2_chain(self):
        '''
        selects the MS2 chain by compairing the self.mass_pair to the masses of,
        R1 and R2 chains (calculated from self.R1_mass() and self.R2_mass() )
        '-' mode only
        '''
        if self.mode == '+':
            raise TypeError ('mode must be \'-\' to use this' )
        if self.R2_mass()-.3 < self.R1_mass() < self.R2_mass() +.3 and self.chain_features()[0] != self.chain_features()[1]:
            print 'warning masses of tails of '+self+ ' are too close to decide which one became the fragment, but R1 has been selected'            
        elif self.mass_pair.tolist()[1]-.5 <= self.R1_mass() <= self.mass_pair.tolist()[1]+.5:
            MS2 = self.chain_formula()[0]
        elif self.mass_pair.tolist()[1]-.5 <= self.R2_mass() <= self.mass_pair.tolist()[1]+.5:
            MS2 = self.chain_formula()[1]
        else:
            raise ValueError ('masses on the tails of '+str(self)+' dont match')
        return MS2
        
    def MS2_features(self):
        if self.mode == '+':
            raise TypeError ('mode must be \'-\' to use this')
        if self.R2_mass()-.3 < self.R1_mass() < self.R2_mass() +.3 and self.chain_features()[0] != self.chain_features()[1]:
            print 'warning masses of tails of '+self+ ' are too close to decide which one became the fragment, but R1 has been selected'            
        elif self.mass_pair.tolist()[1]-.5 <= self.R1_mass() <= self.mass_pair.tolist()[1]+.5:
            return self.R1_features()
        elif self.mass_pair.tolist()[1]-.5 <= self.R2_mass() <= self.mass_pair.tolist()[1]+.5:
            return self.R2_features()
        else:
            raise ValueError ('masses on the tails of '+str(self)+' dont match')
        return MS2
            
class PA(PL):
    
    def __init__(self, name, mode, mass_pair):
        PL.__init__(self, name, mode, mass_pair)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True
        
    def formulas(self):               
        self.classifier = {'O':4, 'H':2, 'P':1}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '-':
            self.backbone['H'] = self.backbone['H']
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1,self.adduct)
            MS2 = self.MS2_chain()
            return MS1, MS2
        elif self.mode == '+':
            '''
            not tested, as not in names list
            '''
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = sub_dict(MS1 , self.classifier)
            return MS1, MS2
    
class PC(PL):
    
    def __init__(self, name, mode, mass_pair):
        PL.__init__(self, name, mode, mass_pair)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'C':-1,'H':-3}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True

    def formulas(self):
        self.classifier =  {'C':5,'H':13,'N':1, 'P':1, 'O':4}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            '''
            [M+H]
            careful!!!! this molecule gets protinated after begin split so it 
            has an additional H in the MS2
            '''
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = self.classifier
            MS2 = add_dict(MS2, self.adduct)
            #the MS2 then picks up a H from NL according to RCMurphy, Tandem MassSpec of Lipids. 
            MS2 = add_dict(MS2,{'H':1})
            return MS1, MS2        
        if self.mode == '-':
            '''
            [M-15]
            '''       
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = self.MS2_chain()
            return MS1, MS2
        
class PE(PL):
    
    def __init__(self, name, mode, mass_pair):
        PL.__init__(self, name, mode, mass_pair)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True
    
    def formulas(self):               
        self.classifier = {'C':2,'H':7,'N':1, 'P':1, 'O':4}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            NL = add_dict(self.classifier,self.adduct)
            MS2 = sub_dict(MS1 , NL)
            return MS1, MS2
        elif self.mode == '-':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1,self.adduct)
            MS2 = self.MS2_chain()
            return MS1, MS2

class PG(PL):
    
    def __init__(self, name, mode, mass_pair):
        PL.__init__(self, name, mode, mass_pair)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True

    def formulas(self):               
        self.classifier = {'C':3,'H':8, 'P':1, 'O':6}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            NL = add_dict(self.classifier, self.adduct)
            MS2 = sub_dict(MS1, NL)
            return MS1, MS2
        elif self.mode == '-':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1,self.adduct)
            MS2 = self.MS2_chain()
            return MS1, MS2

class PI(PL):
    
    def __init__(self, name, mode, mass_pair):
        PL.__init__(self, name, mode, mass_pair)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True

    def formulas(self):               
        self.classifier = {'C':6, 'H':12, 'P':1, 'O':9}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            NL = add_dict(self.classifier,self.adduct)
            MS2 = sub_dict(MS1,NL)
            return MS1, MS2
        elif self.mode == '-':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1,self.adduct)
            MS2 = self.MS2_chain()
            return MS1, MS2
        
class PS(PL):
    
    def __init__(self, name, mode, mass_pair):
        PL.__init__(self, name, mode, mass_pair)
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'H':-1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True
    
    def formulas(self):
        self.classifier = {'C':3,'H':7, 'N':1, 'O':6,'P':1}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            NL = add_dict(self.classifier, self.adduct)
            MS2 = sub_dict(MS1, NL)
            return MS1, MS2
        elif self.mode == '-':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1,self.adduct)
            MS2 = self.MS2_chain()
            return MS1, MS2
        
class Cer(Polar_Lipid):
    pass

class HexCer(Polar_Lipid):
    pass

class Hex2Cer(Polar_Lipid):
    pass

class Hex3Cer(Polar_Lipid):
    pass

class GM1(Polar_Lipid):
    pass

class GM2(Polar_Lipid):
    pass

class GM3(Polar_Lipid):
    pass

class GD1(Polar_Lipid):
    pass

class GD2(Polar_Lipid):
    pass

class GD3(Polar_Lipid):
    pass

class SM(Polar_Lipid):
    
    def __init__(self, name, mode, mass_pair=None):
        Polar_Lipid.__init__(self, name, mode, mass_pair)
        self.classifier = {'C':5,'H':13,'N':1,'O':4,'P':1}
        if self.mode == '+':
            self.adduct = {'H':1}
        elif self.mode == '-':
            self.adduct = {'F':2,'H':1}
        else:
            print ('Please select a mode of and %s rerun'%(self.name))
        self.accurate_formulas = True
        
  
    def get_features(self):
        reg_ex = {'Carbons' : '\((\d+):',                 
                  'Double_Bonds' : ':(\d+)\)'}
        self.features = {key : int(search(reg_ex[key], self.name).group(1)) for key in reg_ex.keys()}

    def chains_formula(self):
        form = {'C':self.features['Carbons']}
        form['H'] = 2*form['C']-2*self.features['Double_Bonds']       
        form['O'] = 2
        form['N'] = 1
        return form

    def formulas(self):
        MS1 = add_dict(self.chains_formula(), self.classifier)
        MS1 = add_dict(MS1, self.adduct)
        if self.mode == '+':
            MS2 = add_dict(self.classifier,self.adduct)
            #MS2 gets H from NL when splitting
            MS2 = add_dict(MS2, {'H':1})
        if self.mode == '-':
            MS2 = sub_dict(MS1, self.adduct)
            MS2 = sub_dict(MS2, {'C':1,'H':3})
        return MS1,MS2

###CHOLESTEROL ESTER        
class CE(Neutral_Lipid):

    def __init__(self, name, mode, mass_pair):
        Neutral_Lipid.__init__(self, name, mode, mass_pair)
        self.adduct = {'N':1,'H':4}
        self.classifier = {'C':27, 'H':45}
        self.chain = self.chain_formula()
        if self.mode != '+':
            print ('Mode of %s should not be %s'%(self.name, self.mode))
        self.accurate_formulas = True
        
    def get_features(self):
        super(Neutral_Lipid, self).get_features()
    
    def chain_formula(self):
        form = {'C':self.features['Carbons']}
        form['H'] = form['C']*2-self.features['Double_Bonds']*2-1
        form['O']=2
        return form

    def formulas(self):
        MS1 = add_dict(self.classifier, self.chain)
        MS1 = add_dict(MS1, self.adduct)
        MS2 = self.classifier
        return MS1, MS2

class FAHFA(Endocyne):
        
    def get_features(self):
        features = {}
        features['Branch_1'], features['Branch_2'] = self.name.split('H')
        self.features = features
            
#==============================================================================
#standards are in the next segment
class Standard(Lipid):
    
    def __init__(self, name, mode, mass_pair):
        Lipid.__init__(self, name, mode , mass_pair)
 
    def get_features(self):
        return None

class SPLASH_Standard_LPC(LPC):

    def __init__(self, name, mode, mass_pair):
        
        LPC.__init__(self,name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
        if self.mode == '-':
            self.adduct = {'H':1,'F':2}
    
    def get_standard(self, observation):
        return self.name
            
class SPLASH_Standard_LPE(LPE):

    def __init__(self, name, mode, mass_pair):
        LPE.__init__(self,name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
    
    def get_standard(self, observation):
        return self.name


class SPLASH_Standard_PC(PC):
    
    def __init__(self, name, mode, mass_pair):
        PC.__init__(self, name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
        if self.mode == '-':
            self.adduct = {'H':1,'F':2}
            
    def get_standard(self, observation):
        return self.name

    def formulas(self):
        self.classifier =  {'C':5,'H':13,'N':1, 'P':1, 'O':4}
        self.backbone = add_dict(self.classifier, self.link)
        if self.mode == '+':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            MS2 = self.classifier
            MS2 = add_dict(MS2, self.adduct)
            #the MS2 then picks up a H according to RCMurphy, Tandem MassSpec of Lipids. 
            MS2 = add_dict(MS2,{'H':1})
            return MS1, MS2  
        if self.mode == '-':
            MS1 = add_dict(self.chains_formula(),self.backbone)
            MS1 = add_dict(MS1, self.adduct)
            ms2_mass = self.mass_pair.ms2
            R1_mass = self.R1_mass()
            R2_mass = self.R2_mass()
            if ms2_mass-.3 <= R1_mass <= ms2_mass+.3:
                MS2 = self.chain_formula()[0]
            elif ms2_mass-.3 <= R2_mass <= ms2_mass+.3:
                MS2 = self.chain_formula()[1]
            else:
                print 'Warning! MS2 given of %s in "%s" mode does not match the mass of either chain'%(self.name, self.mode)
            return MS1, MS2

class SPLASH_Standard_PE(PE):
    
    def __init__(self, name, mode, mass_pair):
        PE.__init__(self, name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
        
    def get_standard(self, observation):
        return self.name
            
class SPLASH_Standard_PG(PG):
    
    def __init__(self, name, mode, mass_pair):
        PG.__init__(self, name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
        if self.mode == '+':
            self.adduct = {'N':1,'H':4}
            
    def get_standard(self, observation):
        return self.name
        
class SPLASH_Standard_PI(PI):
    
    def __init__(self, name, mode, mass_pair):
        PI.__init__(self, name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
            
    def get_standard(self, observation):
        return self.name
        
class SPLASH_Standard_PA(PA):
    
    def __init__(self, name, mode, mass_pair):
        PA.__init__(self, name, mode, mass_pair)
        if '{d' in self.name:
            self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
            
    def get_standard(self, observation):
        return self.name
        
class SPLASH_Standard_MAG(MAG):
    
    def __init__(self, name, mode, mass_pair):
        MAG.__init__(self, name, mode, mass_pair)
        self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
        
    def get_standard(self, observation):
        return self.name

    def chain_formula(self):
        form = {'C':self.carbons}
        form['H'] = 2*form['C']-2*self.double_bonds-1-self.deuteriumNo
        form['O'] = 1
        form ['D'] = self.deuteriumNo
        return form
        
    def formulas(self):
        MS1 = add_dict(self.backbone,self.chain_formula())
        MS1 = add_dict(MS1,self.adduct)
        MS2 = self.chain_formula()
        return MS1, MS2

class SPLASH_Standard_DAG(DAG):
    
    def __init__(self,name, mode, mass_pair):
        DAG.__init__(self, name, mode, mass_pair)
        self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
    
    def get_standard(self, observation):
        return self.name
    
    def R2_formula(self):
        carbons = self.features['Carbons']-self.features['FA_Carbons']
        db = self.features['Double_Bonds']-self.features['FA_Double_Bonds']
        FA = {'C':carbons}
        FA['H'] = 2*FA['C']-2*db-1-self.deuteriumNo
        FA['O'] = 2
        FA['D'] = self.deuteriumNo
        return FA
        
    def formulas(self):     
        nc = self.carbons
        ndb = self.double_bonds
        nlc = self.features['FA_Carbons']
        nldb = self.features['FA_Double_Bonds']
        MS1 = {'C':nc+3,'H':2*nc-2*ndb+8,'O':5,'N':1}
        MS2 = {'C':nc-nlc+3,'H':2*(nc-nlc-1)-2*(ndb-nldb)+7,'O':3}
        if '{d' in self.name:
            deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
            MS1['H']=MS1['H']-deuteriumNo
            MS1['D']=deuteriumNo
            MS2['H']=MS2['H']-deuteriumNo
            MS2['D']=deuteriumNo
        return MS1, MS2
        
class SPLASH_Standard_TAG(TAG):
    
    def __init__(self, name, mode, mass_pair):
        TAG.__init__(self, name, mode, mass_pair)
        self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
    
    def get_standard(self, observation):
        return self.name

    def other_chains_formula(self):
        carbons = self.features['Carbons']-self.features['FA_Carbons']
        db = self.features['Double_Bonds']-self.features['FA_Double_Bonds']
        form = {'C':carbons}
        form['H'] = 2*carbons - 2*db -2 - self.deuteriumNo
        form['O'] = 4
        form['D'] = self.deuteriumNo
        return form

class SPLASH_Standard_SM(SM):
    
    def __init__(self, name, mode, mass_pair):
        SM.__init__(self, name, mode, mass_pair)
        self.deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))

    def get_standard(self, observation):
        return self.name
        
    def chains_formula(self):
        form = {'C':self.features['Carbons']}
        form['H'] = 2*form['C']-2*self.features['Double_Bonds']-self.deuteriumNo
        form['D'] = self.deuteriumNo
        form['O'] = 2
        form['N'] = 1
        return form

class SPLASH_Standard_CE(CE):
        
    def get_standard(self, observation):
        return self.name

    def chain_formula(self):
        if '{d' in self.name:
            deuteriumNo = int(search('\{d(.*?)\}',self.name).group(1))
            form = {'C':self.features['Carbons']}
            form['H'] = form['C']*2-self.features['Double_Bonds']*2-1-deuteriumNo
            form['O']=2
            form['D']=7
            return form
    
class Unknown(Lipid):
    
    def __init__(self, name, mode, mass_pair=None):
        ###This constructor doesn't really gel with what we need from a Lipid,
        ###but it might need to be this way so that the from_name and from_mass_pair
        ###methods work correctly.  Perhaps review this at some point.
#        try:
#            assert mass_pair is not None
#        except AssertionError:
#            raise ValueError('mass_pair needed to initialize Unknown type.')
        self.mass_pair = mass_pair
        self.mode = mode
        self.name = 'u'+str(self.mass_pair)
        self.group = 'Unknown'
        self.isotopomers = []
        
    def __hash__(self):
        return hash(self.mass_pair)
            
    def print_mp(self):
        print self.mass_pair
    
    
    def get_features(self):
        return {}
    
    @classmethod
    def get_lineage(cls):
        return 10*['Unknown']
        
    def add_isotopomer(self, other):
        self.isotopomers.append(other)
        
    def flag_isotopomer(self, other):
        if self.is_isotope(other) and other.group != 'Unknown':
            self.add_isotopomer(other)
            return True
        else:
            return False
        
class Misc(Lipid):
        
    def get_features(self):
        self.features = {}
        return self.features
        
class Isoprenoid(Misc):
    pass

class Vitamin(Misc):
    pass

class Cholesterol(Misc):
    pass     

        
if __name__ == '__main__':
    sys.path.append('..')
    from Lookups import name_lookup
    
    peneg=Lipid.from_group('PE','PE38:5(18:3,20:2)',mode = '-',mass_pair=MassPair(764.7,307.3))  
    pepos=Lipid.from_group('PE','PE(38:5)',mode = '+',mass_pair=MassPair(766.6,625.5))
    pcpos = Lipid.from_group('PC','PC(32:3)', mode = '+', mass_pair=MassPair(728.7,184.1))    
    pcneg = Lipid.from_group('PC','PC32:3(18:2,14:1)', mode = '-', mass_pair=MassPair(712.7,225.2))
    
    paneg = Lipid.from_group('PA','PA32:1(18:1,14:0)',mode = '-',mass_pair=MassPair(645.6,227.2))
    pgpos = Lipid.from_group('PG','PG(34:1)',mode='+',mass_pair=MassPair(749.6,577.5123))
    pgneg = Lipid.from_group('PG','PG28:1(16:1,12:0)',mode='-',mass_pair=MassPair(663.6,199.2))
    #pipos = Lipid.from_group('PI','PI(32:3)',mode = '+',mass_pair=MassPair(805.66,547.4557))
    pineg=Lipid.from_group('PI','PI38:6(20:5,18:1)',mode = '-',mass_pair=MassPair(881.8,301.3))
    pspos = Lipid.from_group('PS','PS(36:4)',mode = '+',mass_pair=MassPair(784.63,599.4867))
    psneg = Lipid.from_group('PS','PS33:2(17:1,16:1)',mode = '-',mass_pair=MassPair(744.7,267.3))
    
    lpcpos=Lipid.from_group('LPC','lysoPC(16:1)',mode = '+', mass_pair=MassPair(494.4,184.1))
    
    smpos = Lipid.from_group('SM','SM(34:1)',mode = '+', mass_pair=MassPair(703.7,184.1))
    
    cepos = Lipid.from_group('CE','CE(14:0)', mode = '+', mass_pair=MassPair(614.5,369.3))
    
    mag = Lipid.from_group('MAG','MAG(16:0)',mode = '+', mass_pair=MassPair(348.3,313.3))
    
    dag = Lipid.from_group('DAG','DAG(32:1) NL-16:0', mode = '+', mass_pair=MassPair(584.6,311.4))
    
    tag = Lipid.from_group('TAG','TAG(47:1) NL-15:0', mode = '+', mass_pair=MassPair(808.6,549.4))
    
    for i in [peneg,pepos,pcpos,pcneg,paneg,pgpos,pgneg,pineg,pspos,psneg]:
        print i        
        i.check()

    spcpos = Lipid.from_group('SPLASH_Standard_PC','PC(33:1){d7}',mode = '+', mass_pair=MassPair(753.6,184))
    spepos = Lipid.from_group('SPLASH_Standard_PE','PE(33:1){d7}',mode = '+', mass_pair=MassPair(711.6,570.5))
    spgpos = Lipid.from_group('SPLASH_Standard_PG','PG(33:1){d7}',mode = '+', mass_pair=MassPair(759.6,570.5))
    slpcpos = Lipid.from_group('SPLASH_Standard_LPC','LPC(18:1){d7}',mode = '+', mass_pair=MassPair(529.4,184))
    slpepos = Lipid.from_group('SPLASH_Standard_LPE','LPE(18:1){d7}',mode = '+', mass_pair=MassPair(487.3,346.3))
    scepos = Lipid.from_group('SPLASH_Standard_CE','CE(18:1){d7}',mode = '+',mass_pair=MassPair(675.5,369.3))
    smag = Lipid.from_group('SPLASH_Standard_MAG','MAG(18:1){d7}', mode = '+', mass_pair=MassPair(381.2,272.2))
    sdag = Lipid.from_group('SPLASH_Standard_DAG','DAG(33:1){d7} NL-15:0', mode = '+', mass_pair=MassPair(605.5,346.3))
    stag = Lipid.from_group('SPLASH_Standard_TAG','TAG(48:1){d7} NL-15:0', mode = '+', mass_pair=MassPair(829.7,570.5))
    ssmpos = Lipid.from_group('SPLASH_Standard_SM','SM(36:1){d7}', mode = '+', mass_pair=MassPair(738.6,184))

    spcneg = Lipid.from_group('SPLASH_Standard_PC','PC33:1(15:0,18:1){d7}',mode = '-', mass_pair=MassPair(791.6,288.2))
    speneg = Lipid.from_group('SPLASH_Standard_PE','PE33:1(15:0,18:1){d7}',mode = '-', mass_pair=MassPair(709.6,288.2))
    spgneg = Lipid.from_group('SPLASH_Standard_PG','PG33:1(15:0,18:1){d7}',mode = '-', mass_pair=MassPair(740.6,288.2))
    slpcneg = Lipid.from_group('SPLASH_Standard_LPC','LPC(18:1){d7}',mode = '-', mass_pair=MassPair(567.4,288.2))    
    slpeneg = Lipid.from_group('SPLASH_Standard_LPE','LPE(18:1){d7}',mode = '-', mass_pair=MassPair(485.3,288.2))
    spineg = Lipid.from_group('SPLASH_Standard_PI', 'PI33:1(15:0,18:1){d7}', mode = '-', mass_pair=(828.7,288.2))
    spaneg = Lipid.from_group('SPLASH_Standard_PA', 'PA33:1(15:0,18:1){d7}', mode = '-', mass_pair=(666.5,241.2))
    ssmneg = Lipid.from_group('SPLASH_Standard_SM','SM(36:1){d7}', mode = '-', mass_pair=MassPair(776.6,722.6))

    splash_standards = [spcpos,spepos,spgpos,slpcpos,slpepos,scepos,smag,sdag,stag,ssmpos,
                        spcneg,speneg,spgneg,slpcneg,slpeneg,spineg,spaneg,ssmneg]
    #this tests the entire names lists and makes sure the mass_pairs equal the 
    #calculated masses with .5 accuracy of the Lipids where self.accurate_formulas == True
    full_test = True
    if full_test == True:
        issue_group = []
        issue_lipids = []
        for k in name_lookup.keys():
            for i in name_lookup[k].index:
                name = name_lookup[k]['Name'][i]
                group = name_lookup[k]['Group'][i]
                pre = name_lookup[k]['Precursor'][i]
                frag = name_lookup[k]['Fragment'][i]
#                if 'or' in name:
#                    name = name.split('or')[0]
                if 'or' not in name:
                    lip = Lipid.from_group(group,name,mode = k, mass_pair=MassPair(pre,frag))
                    if lip.check_masses() != 'masses match within .5' and lip.accurate_formulas == True:
                        issue_lipids.append(lip)
                        if lip.group+'_'+lip.mode not in issue_group:
                            issue_group.append(lip.group+'_'+lip.mode)            

'''
    print Polar_Lipid.get_lineage()
    print Polar_Lipid.dir_string()
    print isinstance(Polar_Lipid('PC(16:0)', '+'), Lipid)
    dl = Lipid.from_name('DAG(34:1) NL-16:0', '+', MassPair(612.46, 339.424))
    tl = Lipid.from_group('TAG', 'TAG(46:0) NL-16:0', mode = '+', mass_pair=(796.65,523.69))
    from LipPy.Lookups import name_lookup
    pos_lipids = name_lookup['+'].copy()#[['Group', 'Name']]
    neg_lipids = name_lookup['-'].copy()#[['Group', 'Name']]
    test = Lipid.from_name('PC32:1(16:1,16:0)', mode = '-',mass_pair = MassPair(766.62,255.2332))
    test2 = Lipid.from_group('PC','PC32:1(16:1,16:0)', mode = '-',mass_pair = MassPair(766.62,255.2332))
    paneg =Lipid.from_group('PA','PA33:2(18:1,15:1)',mode = '-',mass_pair = MassPair(647.51,281.2482))
    papos =Lipid.from_group('PA','PA(32:0)',mode = '+',mass_pair = MassPair(649,551))
    papos =Lipid.from_group('PA','PA(40:3)',mode = '+',mass_pair = MassPair(755,657))
    pc1 =Lipid.from_group('PC','PC38:5(22:4,16:1)',mode = '-',mass_pair = MassPair(792.64,253.2195))
    pcneg =Lipid.from_group('PC','PC32:0(16:0,16:0)',mode = '-',mass_pair = MassPair(718,255.2326))
    pcneg2 =Lipid.from_group('PC','PC32:1(16:1,16:0)',mode = '-',mass_pair = MassPair(716,255.2326))
    pcpos = Lipid.from_group('PC','PC(32:0)',mode = '+',mass_pair = MassPair(734.7,184.1))
    
    pe1 =Lipid.from_group('PE','PE38:4(18:0,20:4)',mode = '-',mass_pair = MassPair(766.62,283))

    pe2 =Lipid.from_group('PE','PE30:0(16:0,16:0)',mode = '-',mass_pair = MassPair(690.00,255))
    pepos1 =Lipid.from_group('PE','PE(32:0)',mode = '+',mass_pair = MassPair(692,551))
    pepos2 =Lipid.from_group('PE','PE(42:6)',mode = '+',mass_pair = MassPair(820,679))
    pg1 =Lipid.from_group('PG','PG32:3(18:3,14:0)',mode = '-',mass_pair = MassPair(715.57,227.4093))
    pg2 =Lipid.from_group('PG','PG42:8(22:4,20:4)',mode = '-',mass_pair = MassPair(845.7,303.2849))
    pgpos1 =Lipid.from_group('PG','PG(32:0)',mode = '+',mass_pair = MassPair(721,255))
    pgpos2 =Lipid.from_group('PG','PG(40:4)',mode = '+',mass_pair = MassPair(825,303))
    pi1 =Lipid.from_group('PI','PI30:1(16:1,14:0)',mode = '-',mass_pair = MassPair(779.63,227.4117))
    pi2 =Lipid.from_group('PI','PI31:1(18:0,13:1)',mode = '-',mass_pair = MassPair(793.64,283.3205))
    pipos2 =Lipid.from_group('PI','PI(42:5)',mode = '+',mass_pair = MassPair(941,681))
    pipos1 =Lipid.from_group('PI','PI(32:0)',mode = '+',mass_pair = MassPair(805,545))
    ps1 =Lipid.from_group('PS','PS38:3(20:3,18:0)',mode = '-',mass_pair = MassPair(812.66,283.3062))
    ps2 =Lipid.from_group('PS','PS38:3(20:2,18:1)',mode = '-',mass_pair = MassPair(812,307.3))
    pspos1 =Lipid.from_group('PS','PS(32:0)',mode = '+',mass_pair = MassPair(736,551))
    pspos2 =Lipid.from_group('PS','PS(38:3)',mode = '+',mass_pair = MassPair(814,629))
    
    lysopc1= Lipid.from_group('LPC','LysoPC(22:6)',mode = '+',mass_pair = MassPair(494.34,184.14))
    lysopc2= Lipid.from_group('LPE','LysoPC(22:6)',mode = '+',mass_pair = MassPair(526.38,385.433))
    lysopi1= Lipid.from_group('LPI','LysoPC(18:0)',mode = '+',mass_pair = MassPair(601.45,341.448))
    lysope1= Lipid.from_group('LPE','LysoPC(16:1)',mode = '+',mass_pair = MassPair(452.3,311.2))
    
    mag1 = Lipid.from_group('MAG','MAG(18:0)',mode = '+', mass_pair = MassPair(376.23,341.4465))
    mag2 = Lipid.from_group('MAG','MAG(18:1)',mode = '+', mass_pair = MassPair(374.23,330.4465))
    mag3 = Lipid.from_group('MAG','MAG(16:0)',mode = '+', mass_pair = MassPair(348.2,313.4))
    mag4 = Lipid.from_group('MAG','MAG(14:0)',mode = '+', mass_pair = MassPair(320.17,285.5))
    testlist = [pe1,pe2,pepos1,pepos2,pg1,pg2,pgpos1,pgpos2,pi1,pi2,pipos1,pipos2,\
        ps1,ps2,pspos1,pspos2,mag1,mag2,mag3,mag4,lysopc1,lysopc2,lysopi1,lysope1]
    lst = []
    for i in testlist:
        if i.check_masses() != 'masses match within .3':
            lst.append(str(i) + ' ' +i.check_masses())  
    pos_test = pos_lipids.apply(lambda x: Lipid.from_group(x[3], x[2], mass_pair=MassPair(x[0], x[1]), mode='+').group, axis=1)
    neg_test = neg_lipids.apply(lambda x: Lipid.from_group(x[3], x[2], mass_pair=MassPair(x[0], x[1]), mode='-').group, axis=1)
    misID_pos = pos_lipids.ix[pos_lipids.Group != pos_test.apply(str)]
    misID_neg = neg_lipids.ix[neg_lipids.Group != neg_test.apply(str)]
    pos_test = pos_lipids.apply(lambda x: Lipid.from_group(x[3], x[2], mass_pair=MassPair(x[0], x[1]), mode='+'), axis=1)
    neg_test = neg_lipids.apply(lambda x: Lipid.from_group(x[3], x[2], mass_pair=MassPair(x[0], x[1]), mode='-'), axis=1)
    assert (misID_pos.Group == 'Labels').all()
    assert len(misID_neg.Group) == 0
'''