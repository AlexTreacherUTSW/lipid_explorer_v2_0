# -*- coding: utf-8 -*-
"""
Created on Mon Feb 02 12:20:07 2015

@author: Mike
"""
from __future__ import division
from collections import OrderedDict
import numpy as np
from sim_isotopes import ABUNDANCES, TRIOLEIN, B_TRIOLE
from scipy.stats import chisquare as pearson_chi_square
from scipy.stats import chisqprob

probs = ABUNDANCES
ISO_RANGE = 5
TOLERANCE = 0.1

class MassPair(object):
    
    @staticmethod
    def from_string(string, ascending=False):
        pair = map(float, string.split('_'))
        return MassPair.from_list(pair, ascending=ascending)
        
    @staticmethod
    def from_list(list_like, ascending=False):
        if ascending is True:
            ms2, ms1 = list_like
        elif ascending is False:
            ms1, ms2 = list_like
        elif ascending == 'auto':
            ms2, ms1 = sorted(list_like)
        return MassPair(ms1, ms2)
    
    def __init__(self, ms1, ms2):
        self.ms1 = ms1
        self.ms2 = ms2

    def __cmp__(self, other):
        ###This will have to be changed if we ever move to Python 3, where
        ###cmp is not valid
        if cmp(self.ms1, other.ms1) != 0:
            return cmp(self.ms1, other.ms1)
        else:
            return cmp(self.ms2, other.ms2)
        
    def __eq__(self, other, PRECISION=TOLERANCE):
        other = _to_mass_pair(other)
        if not isinstance(other, MassPair):
            return Falsemst.gen_isotop
        else:
            a1 = np.array(self.tolist())
            a2 = np.array(other.tolist())
            return np.allclose(a1, a2, atol=TOLERANCE)
            
    def __getitem__(self, i):
        return [self.ms1, self.ms2][i]
        
    def __add__(self, other):
        return MassPair(self.ms1+other.ms1, self.ms2+other.ms2)
        
    def __sub__(self, other):
        return MassPair(self.ms1-other.ms1, self.ms2-other.ms2)
    
    def __hash__(self):
        x1 = round(10*self.ms1)
        x2 = round(10*self.ms2)
        string = str(x1)+'_'+str(x2)
        return hash(string)
            
    def __repr__(self):
        string = str(round(self.ms1, 2))+'_'+str(round(self.ms2, 4))
        return string
        
    def __str__(self):
        string = str(round(self.ms1, 2))+'_'+str(round(self.ms2, 4))
        return string
        
    def tolist(self, ascending=False):
        """
        Returns [ms2, ms1] if ascending is True and [ms1, ms2] if ascending
        is False.
    
        Parameters
        ----------
        ascending : bool
            Whether or not to sort the masses in ascending order.
        """
        if ascending:
            return [self.ms2, self.ms1]
        else:
            return [self.ms1, self.ms2]
        
    def neutral_loss(self):
        return self.ms1 - self.ms2
        
    def has_isotope(self, other, RANGE=ISO_RANGE, **kwargs):
        """
        Returns True if other is in isotope of self and False otherwise.
    
        Parameters
        ----------
        other : MassPair_like
            other can be of type MassPair, list, tuple, or string.  If it is a
            list or tuple, it will be converted to a MassPair according to
            parameters defined in kwargs
        RANGE : int
            RANGE is the maximum difference between self.ms1 and other.ms1
            for which other can be said to be an isotope.  It is 4 by default.
            
        ascending: True, False or 'auto'
            if other is not a MassPair, ascending gives the order that ms1 and
            ms2 are in in the sequence.  If True, the sequence will be read
            as (ms2, ms1).  If 'auto', ms1 will be taken to be the maximum of
            the sequence, and ms2 will be taken to be the minimum.
            
        See also
        ----------
        MassPair.is_isotope()
        MassPair.gen_isotopes()
        """
        
        _to_mass_pair(other, **kwargs)
        return other in self.gen_isotopes(RANGE=RANGE)
        #return any(map(lambda x : x == other, self.gen_isotopes()))
        
    def is_isotope(self, other, RANGE=ISO_RANGE, **kwargs):
        """
        Returns True if self is an isotope of other and False otherwise.
    
        Parameters
        ----------
        other : MassPair_like
            other can be of type MassPair, list, tuple, or string.  If it is a
            list or tuple, it will be converted to a MassPair according to
            parameters defined in kwargs
        RANGE : int
            RANGE is the maximum difference between self.ms1 and other.ms1
            for which self can be said to be an isotope.  It is 4 by default.
            
        ascending: True, False or 'auto'
            if other is not a MassPair, ascending gives the order that ms1 and
            ms2 are in in the sequence.  If True, the sequence will be read
            as (ms2, ms1).  If 'auto', ms1 will be taken to be the maximum of
            the sequence, and ms2 will be taken to be the minimum.
        """
        other = _to_mass_pair(other, **kwargs)
        return self in other.gen_isotopes(RANGE=RANGE)
        
    def gen_isotopes(self, RANGE=ISO_RANGE):
        """
        Returns a list sorted list of MassPairs that are isotopes of self.
    
        Parameters
        ----------

        RANGE : int
            RANGE is the maximum difference between self.ms1 and the ms1 of 
            another molecule for which the other molecule will appear in the
            list.
            
        ascending: True, False or 'auto'
            if other is not a MassPair, ascending gives the order that ms1 and
            ms2 are in in the sequence.  If True, the sequence will be read
            as (ms2, ms1).  If 'auto', ms1 will be taken to be the maximum of
            the sequence, and ms2 will be taken to be the minimum.
        """
        isotopes = [] ##set lookups are faster
        for i in range(RANGE):
            j = 0
            if self.is_fragmented():
                while j <=i:
                    isotopes.append(MassPair(self.ms1+i, self.ms2+j))
                    j += 1
            else:
                isotopes.append(MassPair(self.ms1+i, self.ms2+i))
        return isotopes
        
    def is_fragmented(self):
        """
        Returns True if ms1 - ms2 >= 14 and False otherwise.
        """
        if (self.ms1 - self.ms2) >= 14:
            return True
        else:
            return False
            
    def which_isotopomer(self, other, RANGE=ISO_RANGE, **kwargs):
        """
        Returns the index at which other appears in the list returned by
        self.gen_isotopes() and returns -1 if other is not an isotope of
        self.
    
        Parameters
        ----------

        RANGE : int
            RANGE is the maximum difference between self.ms1 and other.ms1
            for which other will be said to be an isotope.
            
        ascending: True, False or 'auto'
            if other is not a MassPair, ascending gives the order that ms1 and
            ms2 are in in the sequence.  If True, the sequence will be read
            as (ms2, ms1).  If 'auto', ms1 will be taken to be the maximum of
            the sequence, and ms2 will be taken to be the minimum.
        """
        ###May want to try value other than -1, since l[-1] returns l[len(l-1)]
        not_present_val = -1
        other = _to_mass_pair(other, **kwargs)
        if other < self:
            ix = not_present_val
        else:
            isotopomers = self.gen_isotopes(RANGE=RANGE)
            try:
                ix = isotopomers.index(other)
            except ValueError:
                ix = not_present_val
        return ix
                
            
        
    def check_overlap(self, other, RANGE=ISO_RANGE):
        """
        Returns True if the spectra of self and other overlap and False
        if they do not.
    
        Parameters
        ----------

        RANGE : int
            RANGE is the maximum difference between self.ms1 and other.ms1
            for which other will be said to be an isotope.
            
        ascending: True, False or 'auto'
            if other is not a MassPair, ascending gives the order that ms1 and
            ms2 are in in the sequence.  If True, the sequence will be read
            as (ms2, ms1).  If 'auto', ms1 will be taken to be the maximum of
            the sequence, and ms2 will be taken to be the minimum.
        """        
        other = _to_mass_pair(other)
        self_iso = set(map(str, self.gen_isotopes(RANGE=RANGE)))
        other_iso = set(map(str, other.gen_isotopes(RANGE=RANGE)))
        return not self_iso.isdisjoint(other_iso)
        
    def inc_ms1(self, inc=1):
        return MassPair(self.ms1+inc, self.ms2)

    def inc_ms2(self, inc=1):
        return MassPair(self.ms1, self.ms2+inc)
        
    def as_ints(self):
        return MassPair(int(self.ms1), int(self.ms2))

def _to_mass_pair(obj, **kwargs):
    ascending = kwargs.get('ascending')
    if ascending is None:
        ascending = 'auto'
    if type(obj) == MassPair:
        return obj
    elif type(obj) == 'str':
        return MassPair.from_string(obj, ascending=ascending)
    elif isinstance(obj, (list, tuple)):
        return MassPair.from_list(obj, ascending=ascending)


def single_molecule_isotope_probabilities(chem_formula, as_ratio=False):
    elem_contributions = {}
    for elem, num_elem in OrderedDict(chem_formula).items():
        if num_elem > 0:
            elem_contributions[elem] = reduce(lambda x, y : np.convolve(x, y), [probs[elem]]*num_elem)
        else:
            elem_contributions[elem] = [1]
    isotope_dist = reduce(lambda x, y : np.convolve(x, y), elem_contributions.values())
    if as_ratio:
        return isotope_dist / isotope_dist[0]
    else:
        return isotope_dist
    
        
    
def CSQ_fit(chem_formulas, observation, fit_base=False):
    obs = OrderedDict(observation)
    fparams = int(fit_base)
    
    marginal_prob = OrderedDict(marginal_mass_pairs(chem_formulas, obs.keys()))
    if not fit_base:
        try:
            N_exp = sum(obs.values())
        except TypeError:
            N_exp = sum(obs.values)
    else:
        try:
            N_exp = marginal_prob.values()[0] * obs.values[0]
        except TypeError:
            N_exp = marginal_prob.values()[0] * obs.values()[0]
    expected = OrderedDict([(str(k),v*N_exp) for k,v in marginal_prob.items()])
    CSQ = 0
    for k in obs.keys():
        CSQ += (obs[k] - expected[k])**2 / expected[k]
    p_val = chisqprob(CSQ, len(obs.keys())-1-fparams)
    return np.array([CSQ, p_val])


def MSMS_isotope_probabilities(ms1, ms2, as_ratio=False):
    """
    Returns an array such that pair_matrix[i, j] gives the relative
    frequency of the pair [MS1+(i+j-2), MS2+(j-1)] either in absolute
    probability if as_ratio is false or in abundance relative to M0.
    """
    if as_ratio:
        pair_matrix = calc_ratios(ms1, ms2)
    else:
        neutral_loss = sub_dict(ms1, ms2)
        NL_dist = single_molecule_isotope_probabilities(neutral_loss)
        ms2_dist = single_molecule_isotope_probabilities(ms2)
        pair_matrix = np.outer(NL_dist, ms2_dist)
    return pair_matrix
    
def anchor_to_base(func, *args):
    if len(args) == 0:
        print 'No args.'
        return func
    else:
        pairs = args[1]
        #print 'Upon entry: ', pairs
        chem_formulas = args[0]
        isMassPair = type(pairs[0]) == MassPair
        if not isMassPair:
            pairs = map(MassPair, pairs)
        pairs = np.array(pairs)
        #print 'After being made into an array', pairs
        pairs.sort(axis=0)
        #print 'After sorting: ', pairs
        #pairs = map(lambda x: map(lambda y: float(y), x.split('_')), pairs)
        pairs = map(lambda x: x.tolist(ascending=False), pairs)
        pairs = np.array(pairs)
        new_pairs = np.round((pairs - pairs[0])).tolist()
        #print pairs, new_pairs
        to_anchor = func(chem_formulas, new_pairs)
        if not isMassPair:
            return [[pairs[i], to_anchor[i]] for i in range(len(to_anchor))]
        else:
            f = MassPair
            return [[f(pairs[i,0], pairs[i,1]), to_anchor[i][1]] for i in range(len(to_anchor))]

def marginal_mass_pairs(chem_formulas, pairs, as_ratio=False, anchor=False):
    if any(map(lambda x : type(x) == str, pairs)):
        to_mp = (lambda x: MassPair.from_string(x)
                           if type(x) == str else MassPair(x))
        return marginal_mass_pairs(chem_formulas, map(to_mp, pairs))
    is_zeroes = all(map(lambda x : x == 0, pairs[0]))
    if not is_zeroes:
        return anchor_to_base(marginal_mass_pairs, chem_formulas, pairs)
    ms1, ms2 = chem_formulas
    pair_matrix = MSMS_isotope_probabilities(ms1, ms2)
    probs = [(p, prob_from_pair_matrix(p, pair_matrix)) for p in pairs]
    if as_ratio:
        renorm = probs[0][1]
    else:
        renorm = sum([p[1] for p in probs])
    marginals = [[p[0], p[1]/renorm] for p in probs]
    return marginals
    
def prob_from_pair_matrix(mass_pair, pair_matrix):
    m1, m2 = mass_pair[0], mass_pair[1]
    NL = mass_pair[0] - mass_pair[1]
    if NL < 0:
        print m2, m1, NL
        raise ValueError('m2 cannot be greater than m1.\n Outputting probability 0.')
        return 0
    try:
        if not (np.shape(pair_matrix)[1] == 1):
            prob = pair_matrix[NL,m2]
        else:
            if m2 > 0:
                prob = 0
            else:
                prob = pair_matrix[NL+m2]
    except IndexError:
        prob = 0
    return prob
    
def predict_pair_num(chem_formulas, mass_pair, N):
    ms1, ms2 = chem_formulas
    pair_matrix = MSMS_isotope_probabilities(ms1, ms2)
    func_dict = {str : str_list_to_list, mass_pair : lambda x :
                                                     x.tolist(ascending=False)}
    m1, m2 = mass_pair[0], mass_pair[1]
    NL = m1-m2
    if NL < 0:
        print m2, m1, NL
        raise ValueError('m2 cannot be greater than m1.\n Outputting probability 0.')
        return 0
    try:
        if not (np.shape(pair_matrix)[1] == 1):
            prob = pair_matrix[NL,m2]
        else:
            if m2 > 0:
                prob = 0
            else:
                prob = pair_matrix[NL+m2]
    except IndexError:
        prob = 0
    num_predicted = prob*N
    SE = np.sqrt(prob*(1-prob)/N)
    conf_int = (num_predicted - 1.96*N*SE, num_predicted + 1.96*N*SE)
    return num_predicted, conf_int
    
def predictions(chem_formulas, **kwargs):
    N = None
    for key in ('N', 'num', 'NUM', 'Num'):
        if key in kwargs.keys():
            N = kwargs.get(key)
    table = kwargs.get('table')
    if table is not None:
        pairs = table.keys()
    else:
        pairs = kwargs.get('pairs')
    for key in ('pairs', 'mass_pairs'):
        if key in kwargs.keys():
            pairs = kwargs.get(key)
    func_dict = {str : eval, MassPair : lambda x : x}
    func = lambda x: func_dict[type(x)](x)
    if N is None:
        N = np.sum(table.values())
    return {p : predict_pair_num(chem_formulas, func(p), N) 
            for p in pairs}
                

    
                
#def predictions(chem_formulas, arr, N):
    

def calc_ratios(ms1, ms2):
    pair_matrix = MSMS_isotope_probabilities(ms1, ms2)
    base_abundance = pair_matrix[0,0]
    ratios = pair_matrix / base_abundance
    return ratios
    

    
def goodness_of_fit(obs, pred, print_nums=False):
    pred = np.array(pred)
    obs = np.array(obs)
    pred_norm = pred / pred.sum()
    filtered_pred = pred_norm[pred_norm > 0.1]
    filtered_pred = filtered_pred / filtered_pred.sum()
    filtered_obs = obs[pred_norm > 0.1]
    filtered_obs = filtered_obs / filtered_obs.sum()
    stat_vals = pearson_chi_square(filtered_obs, f_exp=filtered_pred)
    if print_nums:
        print 'Observed frequences:\t', filtered_obs
        print 'Predicted frequencies:\t', filtered_pred
        print 'Chi-squared statistic:\t', stat_vals[0]
        print 'p-value:\t\t', stat_vals[1]
        return None
    return stat_vals
    
def str_list_to_list(string):
    try:
        assert string.startswith('[')
        assert string.endswith(']')
        for i in list(string):
            assert i.isdigit() or i in ['.', ',', '[', ']']
    except AssertionError:
        return None
    listed = eval(string)
    return listed
    
def _test_mass_pair_eq(NUM_TESTS=1000):
    for k in range(NUM_TESTS):
        m1 = 800*np.random.rand()
        m2 = m1*np.random.rand()
        list_pair = map(lambda x : np.round(x, decimals=1), [m1, m2])
        pair = MassPair.from_list(list_pair, ascending=False)
        pair_str = MassPair.from_string(str(pair), ascending=True)
        if round(pair.ms1 - pair.ms2) >= 1:
            for i in range(ISO_RANGE):
                for j in range(i+1):
                    ###generate random float on interval  [i - TOLERANCE, i + TOLERANCE]
                    a1 = i - TOLERANCE
                    b1 = i + TOLERANCE
                    eps1 = (b1 - a1)*np.random.ranf() + a1
                    if pair.is_fragmented():
                        a2  = j - TOLERANCE
                        b2 = j + TOLERANCE
                        eps2 = (b2 - a2)*np.random.ranf() + a2
                    else:
                        eps2 = eps1
                    noisy_pair = [list_pair[0]+eps1, list_pair[1]+eps2]
                    noisy_pair.sort(reverse=True)
                    assert pair.has_isotope(MassPair.from_list(noisy_pair, ascending='auto'))
                    assert pair_str.has_isotope(MassPair.from_list(noisy_pair, ascending='auto'))
    #print 'Successful test of {0} random mass pairs.'.format(NUM_TESTS)
    
def _test_predictions():
    predictions(TRIOLEIN)
    
def add_dict(d1, d2, *args):
    extras = list(args)
    cop = d2.copy()
    while len(extras) > 0:
        cop = add_dict(cop, extras.pop())
        
    keys = list(set(d1.keys()+cop.keys()))
    new_d = {k: 0 for k in keys}
    for k in keys:
        if k in d1.keys():
            new_d[k] += d1[k]
        if k in cop.keys():
            new_d[k] += cop[k]
        if new_d[k] == 0:
            del new_d[k]
    return new_d
 
def mul_dict(d1,d2):
    new_d = {}    
    for i in d1.keys():
        new_d[i] = d1[i]*d2[i]
    return new_d
        
def _test_mass_pair_comparison():
    l = MassPair.from_string('902_603', ascending=False).gen_isotopes()
    h = MassPair.from_string('998_603', ascending=False).gen_isotopes()
    l_cmp = sorted(l)
    h_cmp = sorted(h)
    assert l == l_cmp
    assert h == h_cmp
    
def _test_mass_pair_hash():
    l = MassPair.from_string('902_603').gen_isotopes()
    h = MassPair.from_string('998_603').gen_isotopes()
    test_dict = {l[0] : l, h[0] : h}
    assert test_dict[l[0]] == l
    assert test_dict[h[0]] == h   
    
def sub_dict(d1, d2):
    return add_dict(d1, mult_dict(-1, d2))

def mult_dict(d, num):
    if isinstance(d, (float, int)) and isinstance(num, dict):
        return mult_dict(num, d)
    return {k: v*num for k,v in d.items()}
    
if __name__ == '__main__':
    errors = 'Tests failed: \n'
    tests_failed = 0
#    try:
    _test_mass_pair_eq()
#    except AssertionError:
#        errors += '\t equality method \n'
#        tests_failed += 1
        
    try:
        _test_mass_pair_comparison()
    except AssertionError:
        errors += '\t comparison method \n'
        tests_failed += 1
        
    try:
        _test_mass_pair_hash()
    except AssertionError:
        errors += '\t hash method \n'
        tests_failed += 1

    if tests_failed > 0:
        print errors
        
    else:
        print 'All tests passed.'
        
    for key, vals in probs.items():
        try:
            assert abs(sum(vals) - 1.0) < 10**-6
        except AssertionError:
            print key, 'probabilites sum to:'
            print '\t\tt', sum(vals)
            print 'when they should sum to 1.'
            

