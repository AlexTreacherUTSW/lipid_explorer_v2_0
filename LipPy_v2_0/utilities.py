# -*- coding: utf-8 -*-
"""
Created on Fri Jan 23 16:29:57 2015

@author: JMLab1
"""
import os
import sys
from shutil import move, copy
from datetime import datetime
import pandas as pd
from random import sample


months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
          'August', 'September', 'October', 'November', 'December']

def move_file(filename, directory):
    """
    
    """
    error_message = 'FILENAME not found.  Please move this file to DIRR and try again.'
    if filename is None:
        return None
    if not filename in os.listdir(directory):
        try:
            move(filename, directory)
        except IOError:
            err = error_message.replace('FILENAME', filename)
            err = err.replace('DIRRR', os.getcwd()+os.sep+directory)
            raise IOError(err)
    return directory+os.sep+filename
    
def copy_file(filename, directory):
    """
    
    """
    error_message = 'FILENAME not found.  Please move this file to DIRR and try again.'
    if filename is None:
        return None
    if not filename in os.listdir(directory):
        try:
            copy(filename, directory)
        except IOError:
            err = error_message.replace('FILENAME', filename)
            err = err.replace('DIRRR', os.getcwd()+os.sep+directory)
            raise IOError(err)
    return directory+os.sep+filename
    
def get_month_dir(make_dir=False, directory=None):
    current_month = months[datetime.now().month-1]
    current_year = str(datetime.now().year)
    date_directory = current_month+'_'+current_year
    if directory is None:
        directory = os.getcwd()
    month_dir = directory+os.sep+date_directory
    if not date_directory in os.listdir(directory) and make_dir:
        os.makedirs(month_dir)
    return directory+os.sep+date_directory
    
def log_inputs(destination):
    try:
        try:
            copy('inputs.py', destination)
        except IOError:
            os.remove(destination+os.sep+'inputs.py')
            copy('inputs.py', destination)
    except WindowsError:
        ##WindowsError means no inputs.py, means test.  Pass.
        pass
    
def shrink_dataset(dataset, n):
    """
    Input: .txt file
    Output: None
    
    Side effect: creates a new .txt file consisting of n rows chosen at random
    from the input file.  Useful for generating small data sets to speed up
    testing.
    """
    try:
        table = pd.read_table(dataset)
    except IOError:
        dataset = dataset+'.txt'
        return shrink_dataset(dataset, n)
    new_idx = sample(table.index[5:], min([n, len(table.index[5:])]))
    table = table.ix[new_idx]
    fname = dataset.split('.')[0]
    table.to_csv(fname+'_mini.txt', sep='\t', index=False)
    return None
    
def assert_inputs_exist(data_files, names_file, TOF_files=[]):
    """
    Inputs:
        ---data_files, iterable of strings
        ---names_file, a string
        ---TOF_files, iterable of strings
    Raises errors if any of the strings input do not exist in the current
    directory of the directory that they will be moved to.
    """
    home_name= os.getcwd()
    home_dir = os.listdir(os.getcwd())
    msall_dir = os.listdir(home_name+os.sep+'Raw_Data')
    name_dir = os.listdir(home_name+os.sep+'Name_Files')
    tof_dir = os.listdir(home_name+os.sep+'TOF_data')
    for f in data_files:
        if f is not None:
            if f not in home_dir+msall_dir:
                raise IOError('File '+f+' does not exist.')
    if names_file not in home_dir+name_dir:
        raise IOError('File '+names_file+' does not exist.')
    for f in TOF_files:
        if f is not None:
            if f not in home_dir+tof_dir:
                raise IOError('File '+f+' does not exist.')
    return None

def quick_read(path_to_file, sheet_name=0):
    reader_dict = {'.csv' : pd.read_csv,
                   '.txt' : pd.read_table,
                   '.xls' : lambda x: pd.read_excel(x, sheet_name=sheet_name),
                   '.xlsx' : lambda x: pd.read_excel(x, sheet_name=sheet_name)}
    extensions = reader_dict.keys()  
    file_type = filter(lambda x: path_to_file.endswith(x), extensions)[0]
    reader = reader_dict[file_type]
    df = reader(path_to_file)
    return df

def make_root_dir(ext):
    file_name = os.path.basename(ext)
    if not os.path.exists(ext):
        return file_name
    else:
        i = 1
        leni = len(str(i))
        while os.path.exists(ext):
            if i == 1:
                ext = ext +'_rerun' +str(i)
                file_name = file_name +'_rerun' +str(i)
                i += 1
            else:
                ext = ext[:-leni] +str(i)
                file_name = file_name[:-leni] +str(i)
                leni = len(str(i))
                i += 1
        print 'new_directory name already used, data will be saved under the file extension ' + ext
        return file_name
        
            
def read_time(time):
    if time >=3600:
        hours = int(time/3600)
        time = time - 3600*hours
        mins = int(time/60)
        secs = time % 60
    elif time >= 60 :
        hours = 0
        mins = int(time/60)
        secs = int(time % 60) 
    else:
        hours = 0
        mins = 0        
        secs = int (time)
    if hours < 10:
        hours = str(hours).zfill(2)    
    if mins < 10:
        mins = str(mins).zfill(2)
    else:
        pass
    if secs < 10:
        secs = str(secs).zfill(2)
    else:
        pass
    x = "%s:%s:%s" % (hours, mins, secs)
    return x
    
def names_file_checker(df):
    example_cols = ['Sample_Number','Group','Name','Group_Name', 'Sample_Amount','Standard_Volume']
    forbidden_chars = ['.',',','\'','\\','/','\"','#',' ']
    cols = df.columns.tolist()
    for i in cols:
        if i not in example_cols:
            raise ValueError('%s not a correct column name, please edit and rerun'%(i))
    if df.isnull().values.any():
        raise ValueError('Nans detected in names file, please remove them and rerun')
    if not df.Name.drop_duplicates().equals(df.Name):
        raise ValueError('Duplicates detected in the Names column of the namesfile, please edit and rerun')
    groups = []
    for i in df.index:
        groups.append((df.Group[i],df.Group_Name[i]))
    groups = list(set(groups))
    for l in groups:
        n = l[0]
        s = l[1]
        for g in groups:
            n1 = g[0]
            s1 = g[1]
            if (s1 == s and n1!=n) or (n1==n and s1!=s):
                raise ValueError('Duplicate group_name used for group, please change and rerun.')
    forbidden_found = ''
    for c in [df.Name.tolist(), df.Group_Name.tolist()]:
        for s in c:      
            for f in forbidden_chars:
                if f in s:
                    forbidden_found += ('\''+f+'\''+',')
    if forbidden_found != '':
        raise ValueError ('%s found in names_file, please remove and rerun'%(forbidden_found))
    for col in [df.Sample_Number, df.Group]:
        try:
            col.apply(lambda x: int(x))
        except ValueError:
            raise ValueError('Sample_Number and Group must be numbers')
    else:
        print ('Names file check and ok.')

def list_files(startpath):
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            print('{}{}'.format(subindent, f))

    