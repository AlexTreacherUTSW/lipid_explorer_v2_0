# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 11:39:47 2015

@author: Alex
"""

import pandas as pd
import numpy as np
import shelve
import os
import MS_classes as ms
from MS_classes import concat_names_standards
import Stats.LipidFunctions as lf
import Stats.stats as stats
from Lookups import name_lookup
import collections
import time
from multiprocessing.pool import ThreadPool

if __name__ == '__main__':
    os.chdir(os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0'))
    
testFile = os.path.join(os.getcwd(),'tests')
lookupDir = os.path.join(os.getcwd(),'LipPy_v2_0','Lookups')
stds = shelve.open(os.path.join(lookupDir,'Standards_DB'))
outputDir = 'D:\Alex Treacher\Lipid_Explorer_v2_0\Output'
newDir = '121615_Plasma_SPlash_Dil_series_OnlyPositive'

name_file = '121315_Plasma_Splash_PlasmaAndSplash_namesfile.csv'
ms_pos_raw_file = '121315_Plasma_Splash_PlasmaAndSplash_pos_ms.txt'
ms_neg_raw_file = '121315_Plasma_Splash_PlasmaAndSplash_neg_ms.txt'

raw_data_dir = os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0','Raw_Data')
names_file_file = os.path.join('D:',os.sep,'Alex Treacher','Lipid_Explorer_v2_0','Names_Files')
ms_pos_dir = os.path.join(raw_data_dir,ms_pos_raw_file)
ms_neg_dir = os.path.join(raw_data_dir,ms_neg_raw_file)
name_file_dir = os.path.join(names_file_file,name_file)

pospre = ms.MS_Process(ms_pos_dir,name_file_dir,'+')
negpre = ms.MS_Process(ms_pos_dir,name_file_dir,'-')

print ('Importing')
msall_pos = ms.MS_Data.from_raw_data_files(ms_pos_dir, name_file_dir, standard_mix = None, mode='+')
msall_neg = ms.MS_Data.from_raw_data_files(ms_neg_dir, name_file_dir, standard_mix = None, mode='-')
#msall_pos = ms.MS_Data.from_raw_data_files(ms_pos_dir, name_file_dir, standard_mix = None, std_amt = 30, mode='+')
#msall_neg = ms.MS_Data.from_raw_data_files(ms_neg_dir, name_file_dir, standard_mix = None, std_amt = 30, mode='-')

print ('Filtering')
pos_filtered = msall_pos.all_filters(msall_pos.sample_cols,avg_thresh=100,NL_thresh=-1,x_gt_y=(30,100), child_peaks=[(1,10)])
neg_filtered = msall_neg.all_filters(msall_neg.sample_cols,avg_thresh=100,NL_thresh=-1,x_gt_y=(30,100), child_peaks=[(1,10)])
'''
print ('Naming')
pos_named = pos_filtered.Name_Molecules()
neg_named = neg_filtered.Name_Molecules()
combined_named = pos_named.combine(neg_named)
'''