# -*- coding: utf-8 -*-
"""
Created on Wed Oct 29 13:45:53 2014

@author: Mike
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import LipPy.MS_classes as ms
from itertools import combinations
from statsmodels.regression.linear_model import OLS
from LipPy.utilities import move_file, assert_inputs_exist
import os, sys
sys.path.append(os.getcwd())
append_png = lambda x : x+('.' not in x)*'.png'

def ReadTOF(text_file):
    tof_data = pd.read_table(text_file, skiprows=[1,2])
    #tof_data.columns = ['mass']+[i+1 for i in range(len(tof_data.columns)-1)]
    #tof_data.index = tof_data.mass
    return tof_data

def ParseTOFData(tof_data):
    samples = pd.Series(map(lambda x : 
                        x.split('-')[0], tof_data.columns[1:])).drop_duplicates()
    samp_dict = {}
    for samp in samples:
        columns = filter(lambda x : 
                         x.startswith(samp) or
                         x.startswith('Sample Name'), tof_data.columns)
        columns = columns[-10:]
        columns = ['Sample Name']+columns
        samp_dict[samp] = tof_data[columns]
    return samp_dict

def MeanTOFbySample(tof_dict, method='conf', saveas=None):
    if not isinstance(tof_dict, dict):
        if isinstance(tof_dict, pd.DataFrame):
            td = ParseTOFData(tof_dict)
            return MeanTOFbySample(td, saveas=saveas)
        elif isinstance(tof_dict, str):
            td = ParseTOFData(ReadTOF(tof_dict))
            return MeanTOFbySample(td, saveas=saveas)
    else:
        sums = [tof_dict[i][tof_dict[i].columns[-10:]].sum() for i in tof_dict.keys()]
        meansum = [s.mean() for s in sums]
        stds = [s.std() for s in sums]
        upper = map(lambda x, y : x + 1.96*y, meansum, stds)
        lower = map(lambda x, y : x - 1.96*y, meansum, stds)
        
        fig, ax = plt.subplots()
        if method == 'box':
            ax.boxplot(sums)
        else:
            ax.plot(range(1, len(meansum)+1), meansum, 'ro', label='Average total intensity')
            ax.plot(range(1, len(meansum)+1), upper, 'b^', alpha=0.5, label='95% confidence bounds')
            ax.plot(range(1, len(meansum)+1), lower, 'bv', alpha=0.5)
            ax.legend(loc='best')
        ax.set_xlim(left=0.5, right=len(meansum) + 0.5)
        ax.set_ylim(bottom=0, top = 1.5*max(upper))
        ax.set_xlabel('Sample Number')
        ax.set_ylabel('Intensity')
        if saveas is not None:
            ax.set_title(saveas.replace('.png', ''))
            fig.savefig(append_png(saveas))
        return None
        
def TOFSlope(tof_dict, saveas=None):
    if not isinstance(tof_dict, dict):
        if isinstance(tof_dict, pd.DataFrame):
            td = ParseTOFData(tof_dict)
            return TOFSlope(td, saveas=saveas)
        elif isinstance(tof_dict, str):
            td = ParseTOFData(ReadTOF(tof_dict))
            return TOFSlope(td, saveas=saveas)
    else:
        slopes = []
        p_vals = []
        for df in tof_dict.values():
            total_signal = df[df.columns[-10:]].sum()
            try:
                assert len(total_signal) == 10
            except AssertionError:
                raise IOError('Possible Markerview error.  Please relaoad your TOF file from Markerview.  If you see this message again, contact Mike or Matt, in that order.')
            total_signal = 100*(total_signal - total_signal.mean())/total_signal.std()
            ols = OLS(total_signal, range(10)).fit()
            slopes.append(ols.params[0])
            p_vals.append(ols.pvalues[0])
            
        fig, ax = plt.subplots(ncols=2, figsize=(15,5))
        ax[0].plot(range(1, 1 + len(slopes)), slopes, 'o')
        ax[0].set_xlim(left=0, right=len(slopes)+1)
        ax[1].set_xlim(left=0, right=len(slopes)+1)
        ax[0].set_xticks(range(1, 1 + len(slopes)))
        ax[1].plot(range(1, 1 + len(slopes)), p_vals, 'o')
        ax[1].set_ylim(bottom=0, top=1)
        ax[1].set_xticks(range(1, len(p_vals)+1))
        ax[1].set_yticks(np.arange(0, 1.05, 0.1))
        scale = max([25, 2*max(slopes)])
        ax[0].set_ylim(bottom=-scale, top=scale)
        ax[0].set_yticks(range(-scale, scale+1, 5))
        ax[1].fill_between(range(len(p_vals)+2), 0, 0.05, color='red', alpha=0.15)
        ax[1].plot(range(len(p_vals)+2), [0.05 for i in range(len(p_vals)+2)], 'r--', alpha=0.3)
        
        ax[0].set_title('Slope of last ten TOF points', fontsize='16')
        ax[0].set_xlabel('Sample Number', fontsize='16')
        ax[0].set_ylabel('Slope', fontsize='16')
        ax[1].set_xlabel('Sample Number', fontsize='16')
        ax[1].set_ylabel('p-value', fontsize='16')
        ax[1].set_title('p-value associated with regression', fontsize='16')
        fig.tight_layout(w_pad=3)
        if saveas is not None:
            fig.savefig(append_png(saveas))#), bbox_extra_inches='tight')
        return None
        
def VarianceTOFbySample(tof_dict, method='conf', saveas=None):
    if not isinstance(tof_dict, dict):
        if isinstance(tof_dict, pd.DataFrame):
            td = ParseTOFData(tof_dict)
            return VarianceTOFbySample(td, saveas=saveas)
        elif isinstance(tof_dict, str):
            td = ParseTOFData(ReadTOF(tof_dict))
            return VarianceTOFbySample(td, saveas=saveas)
    else:
        sums = [tof_dict[i][tof_dict[i].columns[-10:]].sum() for i in tof_dict.keys()]
        meansum = [s.var() for s in sums]
        fig, ax = plt.subplots()
        if method == 'box':
            ax.boxplot(sums)
        else:
            ax.plot(range(1, len(meansum)+1), meansum, 'ro', label='Signal Variance')
            ax.legend(loc='best')
        ax.set_xlim(left=0.5, right=len(meansum) + 0.5)
        ax.set_ylim(bottom=0, top = 2*max(meansum))
        ax.set_xlabel('Sample Number')
        ax.set_ylabel('Variance')
        if saveas is not None:
            ax.set_title(saveas.replace('.png', ''))
            fig.savefig(append_png(saveas))

        return None

def move_if_not_none(f, direc):
    if f is None:
        return None
    else:
        return move_file(f, direc)    

def MeanIntensityPlot(tof_data, save=False, plot_name=None):
    try:
        total = tof_data.sum() / len(tof_data.index)
    except TypeError:
        total = tof_data[tof_data.columns[1:]].sum() / len(tof_data.index)
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    intensities = total[total.index[1:]]
    n_samples = len(intensities)
    samp_nums = range(n_samples)
    ax.plot(samp_nums, intensities, 'r', linewidth=2.0)
    mean = intensities.mean()
    std = intensities.std()
    ax.set_xlabel('Sample Number')
    ax.set_ylabel('Mean Peak Intensity')
    ax.set_ylim(bottom=min(intensities) - max([15, std]), top=max(intensities) + max([15, std]))
    ax.set_xlim(right=max(samp_nums))
    ax.fill_between(samp_nums, mean-1.96*std, mean+1.96*std, facecolor='blue', alpha=0.05)
    top_band = np.zeros(len(samp_nums)) + mean + 1.96*std
    bot_band = top_band - 2*1.96*std
    ax.plot(samp_nums, top_band, 'r--', alpha=0.3, label='95%% confidence band')
    ax.plot(samp_nums, bot_band, 'r--', alpha=0.3)
    ax.grid(True)
    ax.legend()    #plt.legend(handles=[blue_patch])
    
    if save and plot_name is not None:
        fig.savefig(plot_name)
    return fig

def TotalIntensityPlot(tof_data, title=None, save=False, plot_name=None):
    try:
        total = tof_data.sum() / len(tof_data.index)
    except TypeError:
        total = tof_data[tof_data.columns[1:]].sum()
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    intensities = total[total.index[1:]]
    n_samples = len(intensities)
    samp_nums = range(n_samples)
    ax.plot(samp_nums, intensities, 'r', linewidth=2.0)
    mean = intensities.mean()
    std = intensities.std()
    ax.set_xlabel('Sample Number')
    ax.set_ylabel('Total Intensity')
    ax.set_ylim(bottom=0, top=1.5*max(intensities))
    ax.set_xlim(right=max(samp_nums))
    if title is not None:    
        ax.set_title(title)
    ax.fill_between(samp_nums, mean-1.96*std, mean+1.96*std, facecolor='blue', alpha=0.05)
    top_band = np.zeros(len(samp_nums)) + mean + 1.96*std
    bot_band = top_band - 2*1.96*std
    ax.plot(samp_nums, top_band, 'r--', alpha=0.3, label='95%% confidence band')
    ax.plot(samp_nums, bot_band, 'r--', alpha=0.3)
    ax.grid(True)
    ax.legend(loc='best')    #plt.legend(handles=[blue_patch])
    
    if save and plot_name is not None:
        fig.savefig(plot_name)
    return fig

def SpecPlot(tof_data, title, save=False, plot_name=None):
    fig = plt.figure(figsize=(10,5))
    ax = fig.add_subplot(1,1,1)
    ax.plot(tof_data['Sample Name'], tof_data[tof_data.columns[-10:]].mean(axis=1), 'b')
    ax.set_ylabel('Intensity')
    ax.set_xlabel('Mass')
    ticks = np.arange(0, 1250, 50)
    ax.set_xticks(ticks)
    ax.set_xticklabels([str(i) for i in ticks], rotation=60)
    ax.set_title(title)
    if save and plot_name is not None:
        fig.savefig(plot_name)       
    return fig
    
def TOFMeanSpecPlot(tof_data, saveas=None, title=None):
    if isinstance(tof_data, str):
        return TOFMeanSpecPlot(ParseTOFData(ReadTOF(tof_data)), saveas=saveas, title=title)
    mean_spec = pd.Series(np.mean(([i[i.columns[-10:]].mean(axis=1) for i in tof_data.values()]), axis=0))
    mass_per_charge = tof_data.values()[0]['Sample Name']
    df = pd.DataFrame([mass_per_charge, mean_spec]).T
    df.columns = ['m/z', 'Intensity']
    sorted_df = df.sort('Intensity', ascending=False).head(20)
    sorted_df['Rank'] = range(1, 21)
    sorted_df = sorted_df[['Rank', 'm/z', 'Intensity']]
    sorted_df['Intensity'] = sorted_df['Intensity'].apply(lambda x : np.round(x))
    html = sorted_df.to_html(index=False)
    mean_spec = mean_spec[mass_per_charge >= 600]
    mass_per_charge = mass_per_charge[mass_per_charge >= 600]
    fig, ax = plt.subplots()
    ax.plot(mass_per_charge, mean_spec)
    ax.set_xlabel('M/Z')
    ax.set_ylabel('Intensity')
    ax.set_xlim(left=600, right=1000)
    plt.tight_layout()
    if title is not None:
        ax.set_title(title)
    if saveas is not None:
        fig.savefig(append_png(saveas))
    return html    

def AllMeanPlots(tof_data, saveas=None):
    parsed = ParseTOFData(tof_data)
    fig, ax = plt.subplots(nrows=len(parsed.keys()), figsize=(5,(200.0/58.0)*len(parsed.keys())))
    i = 0
    #samp = parsed.keys()[0]
    keys = parsed.keys()
    keys.sort()
    for samp in keys:
    #for i in range(58):
        total = parsed[samp].sum() #/ len(tof_data.index)
        #fig = plt.figure()
        #ax = fig.add_subplot(1,1,1)
        intensities = total[total.index[1:]]
        n_samples = len(intensities)
        samp_nums = range(n_samples)
        ax[i].plot(samp_nums, intensities, 'r', linewidth=2.0)
        mean = intensities.mean()
        std = intensities.std()
        ax[i].set_xlabel('Time point')
        ax[i].set_ylabel('Sum of peak intensities')
        ax[i].set_ylim(bottom=0, top=1.5*max(intensities))#min(intensities) - max([15, std]))#, top=max(intensities) + max([15, std]))
        ax[i].set_xlim(right=max(samp_nums))
        ax[i].fill_between(samp_nums, mean-1.96*std, mean+1.96*std, facecolor='blue', alpha=0.05)
        top_band = np.zeros(len(samp_nums)) + mean + 1.96*std
        bot_band = top_band - 2*1.96*std
        ax[i].plot(samp_nums, top_band, 'r--', alpha=0.3, label='95% confidence band')
        ax[i].plot(samp_nums, bot_band, 'r--', alpha=0.3)
        ax[i].grid(True)
        ax[i].legend(loc=4)
        ax[i].set_title(samp)
        i += 1
    
    plt.tight_layout(h_pad=2)
    if saveas is not None:
        fig.savefig(append_png(saveas), dpi=300)
    return None

def AllSpecPlots(tof_data, saveas=None):
    parsed = ParseTOFData(tof_data)
    fig, ax = plt.subplots(nrows=len(parsed.keys()), figsize=(5, (200.0/58.0)*len(parsed.keys())))
    #samp = parsed.keys()[0]
    i = 0
    keys = parsed.keys()
    keys.sort()
    for samp in keys:
    #for samp in parsed.keys():
        samp_data = parsed[samp]
        x = tof_data[tof_data['Sample Name'] >= 600]['Sample Name']
        y =  samp_data[samp_data.columns[-10:]].mean(axis=1)[x.index]
        #ax[i].plot(tof_data['Sample Name'], samp_data[samp_data.columns[-10:]].mean(axis=1), 'r', alpha=0.7)
        ax[i].plot(x, y, 'r', alpha=0.6)
        ax[i].set_ylabel('Intensity')
        ax[i].set_xlabel('Mass')
        ax[i].set_xlim(left=600, right=1000)
        #ticks = np.arange(0, 1250, 50)
        #ax[i].set_xticks(ticks)
        #ax[i].set_xticklabels([str(j) for j in ticks], rotation=60)
        ax[i].set_title(samp+' spectrum')   
        i += 1
    plt.tight_layout(h_pad=2)    
    if saveas is not None:
        fig.savefig(append_png(saveas), dpi=300)       
    return None
        
def NaiveJSDMatrix(data_frame):
    df = data_frame.copy()[data_frame.columns[-10:]]
    JSD_mat = np.zeros((10,10))
    #if not (df.sum() < 1.0001).all() and (df.sum() > 0.999).all():
    #    df = df.apply(lambda x : x / df.sum(), array)
    
    iteration = combinations(range(10),2)
    while True:
        try:
            i, j = iteration.next()
            JSD_mat[i][j] = NaiveJSD(df[df.columns[i]], df[df.columns[j]])
            JSD_mat[j][i] = JSD_mat[i][j]
        except StopIteration:
            break        
    return JSD_mat

def NaiveJSD(a, b, distance=False):

    weights = np.array([a.sum(), b.sum()]) / float(a.sum() + b.sum())
    p = np.array([(x + 1)  / (x + 1).sum() for x in [a, b]])
    mixture = np.dot(weights, p)
    try:
        assert  (mixture.sum() < 1 + 10**-5 and mixture.sum() > 1 - 10**-5)
    except:
        print 'Warning: mixture sum = %s' % mixture.sum()
        print weights, p
    JS_div = ShannonEntropy(mixture) - ( weights[0]*ShannonEntropy(p[0]) + weights[1]*ShannonEntropy(p[1]) )
    if not distance:
        return JS_div
    else:
        return np.sqrt(JS_div)
    #mixture = (a + b)
    #return 0.5*(NaiveKL(a, mixture) + NaiveKL(b, mixture))
    
def ShannonEntropy(p):
    return -np.dot(p.T, np.log2(p))
     

def NaiveKL(v1, v2):
    v1 = v1 + 1
    v2 = v2 + 1
    v1 = v1 / v1.sum()
    v2 = v2 / v2.sum()
    KL_terms = v1 * np.log2(v1 / v2)
    #print v1.sum(), v2.sum()
    KL = KL_terms.sum()
    return KL
    
def PlotMSAllSum(ms_all_data, saveas=None):
    if isinstance(ms_all_data, str):
        return PlotMSAllSum(ReadTOF(ms_all_data), saveas=saveas)
    else:
        sums = ms_all_data[ms_all_data.columns[1:]].sum()
        fig, ax = plt.subplots()
        samp_nums = range(1, len(sums)+1)
        ax.plot(samp_nums, sums, 'b', lw=3)
        ax.set_xlabel('Sample number')
        ax.fill_between(samp_nums, 0, sums, facecolor='blue', alpha=0.1)
        ax.set_ylabel('Intensity')
        ax.grid()
        ax.set_ylim(bottom=0, top=1.5*max(sums))
        ax.set_xlim(left=1, right=len(samp_nums))
        if saveas is not None:
            fig.savefig(append_png(saveas))
            
    return None
    
def NumFilterNamed(mdata, sample):
    num_frame = mdata[mdata[sample] > 30]
    names = num_frame['Name']
    num_filter = len(names)
    num_named = sum(names != 'Unknown')
    return num_filter, num_named
    
def FilterNamePlots(ms_data, saveas=None):
    filter_name = [NumFilterNamed(ms_data.data_frame, samp) for samp in ms_data.sample_cols]
    filtered = [filter_name[i][0] for i in range(len(filter_name))]
    name = [filter_name[i][1] for i in range(len(filter_name))]
    fig, ax = plt.subplots(ncols=2, figsize=(15, 5))
    ax[0].plot(range(len(name)), filtered, 'o')
    ax[1].plot(range(len(name)), name, 'o')
    ax[0].set_xlabel('Sample number', fontsize=16)
    ax[1].set_xlabel('Sample number', fontsize=16)
    ax[0].set_ylabel('# filtered peaks', fontsize=16)
    ax[1].set_ylabel('# named peaks', fontsize=16)
    ax[0].set_ylim(bottom=0)
    ax[1].set_ylim(bottom=0)
    fig.tight_layout(w_pad=5)
    if saveas is not None:
        fig.savefig(append_png(saveas))
    return None
    
def ParentIntensity(ms_data, saveas=None):
    df = ms_data.data_frame[ms_data.data_frame['parent'] > 600]
    df = df[df['parent'] < 1000]
    grouped = df.groupby('parent').mean()[ms_data.sample_cols].mean(axis=1)
    fig, ax = plt.subplots()
    ax.plot(grouped.index, grouped)
    ax.set_xlabel('m/z')
    ax.set_ylabel('Signal Intensity')
    ax.fill_between(grouped.index, grouped)
    ax.set_title('MS1 Intensities')
    if saveas is not None:
        fig.savefig(append_png(saveas))
    sortd = pd.DataFrame(grouped).sort(0, ascending=False)
    mz_sorted = pd.DataFrame(sortd.index[:10])
    mz_sorted['Rank'] = range(1, 11)
    mz_sorted.columns = ['m/z', 'Rank']
    mz_sorted = mz_sorted[['Rank', 'm/z']]
    return mz_sorted.to_html(index=False)

def flip(boolean):
    if boolean == True:
        return False
    if boolean == False:
        return True
    else:
        return True
    
def DaughterIntensity(ms_data, saveas=None):
    df = ms_data.data_frame[ms_data.data_frame['daughter'] > 200]
    df = df[df['daughter'] < 350]
    grouped = df.groupby('daughter').mean()[ms_data.sample_cols].mean(axis=1)
    fig, ax = plt.subplots()
    ax.plot(grouped.index, grouped)
    ax.set_xlabel('m/z')
    ax.set_ylabel('Signal Intensity')
    ax.set_title('MS2 Intensities')
    ax.fill_between(grouped.index, grouped)
    if saveas is not None:
        fig.savefig(append_png(saveas))    
    sortd = pd.DataFrame(grouped).sort(0, ascending=False)
    mz_sorted = pd.DataFrame(sortd.index[:10])
    mz_sorted['Rank'] = range(1, 11)
    mz_sorted.columns = ['m/z', 'Rank']
    mz_sorted = mz_sorted[['Rank', 'm/z']]
    return mz_sorted.to_html(index=False)
    
def replacement(string, replace_dict):
    new_str = string
    for key in replace_dict.keys():
        new_str= new_str.replace(key, replace_dict[key])
    return new_str

def main(pos_msall, neg_msall, pos_tof, neg_tof, html_file_name='QC'):
    
    if pos_tof is not None:
        pos_tof_dict = ParseTOFData(ReadTOF(pos_tof))
        MeanTOFbySample(pos_tof_dict, saveas='pos_total_tof.png')
        pos_spectrum_rank_table = TOFMeanSpecPlot(pos_tof_dict, saveas='pos_tof_spectrum.png')        
        VarianceTOFbySample(pos_tof_dict, saveas='pos_tof_var.png')
        TOFSlope(pos_tof_dict, saveas='pos_tof_slope.png')
        try:
            AllMeanPlots(ReadTOF(pos_tof), saveas='pos_tof_int_by_sample')
        except:
            print('Positive mean plots image too large to save.')
        try:
            AllSpecPlots(ReadTOF(pos_tof), saveas='pos_tof_spec_by_sample')
        except:
            print('Positive spectrum plots image too large to save.')
    else:
        pos_spectrum_rank_table = ''
    if neg_tof is not None:
        neg_tof_dict = ParseTOFData(ReadTOF(neg_tof))
        MeanTOFbySample(neg_tof_dict, saveas='neg_total_tof.png')
        neg_spectrum_rank_table = TOFMeanSpecPlot(neg_tof_dict, saveas='neg_tof_spectrum')
        VarianceTOFbySample(neg_tof_dict, saveas='neg_tof_var.png')
        TOFSlope(neg_tof_dict, saveas="neg_tof_slope.png")
        try:
            AllMeanPlots(ReadTOF(neg_tof), saveas='neg_tof_int_by_sample')
        except:
            print('Negative mean plots  image too large to save.')
        try:
            AllSpecPlots(ReadTOF(neg_tof), saveas='neg_tof_spec_by_sample')
        except:
            print('Negative spectrum plots too large to save.')
    else:
        neg_spectrum_rank_table = ''
    if pos_msall is not None:
        mdata_pos = ms.MS_Process(pos_msall, None, '+').to_MS_data().Name_Molecules()
        PlotMSAllSum(pos_msall, saveas='pos_total_MSall')
        FilterNamePlots(mdata_pos, saveas='pos_name_filter')
        pos_ms1_ranks = ParentIntensity(mdata_pos, saveas='pos_parent_intensity')
        pos_ms2_ranks = DaughterIntensity(mdata_pos, saveas='pos_daughter_intensity')
    else:
        pos_ms1_ranks = ''
        pos_ms2_ranks = ''
    if neg_msall is not None:
        mdata_neg = ms.MS_Process(neg_msall, None, '-').to_MS_data().Name_Molecules()
        PlotMSAllSum(neg_msall, saveas='neg_total_MSall.png')     
        FilterNamePlots(mdata_neg, saveas='neg_name_filter')
        if pos_msall is not None:
            FilterNamePlots(mdata_neg.combine(mdata_pos), saveas='combined_name_filter')
        neg_ms1_ranks = ParentIntensity(mdata_neg, saveas='neg_parent_intensity')
        neg_ms2_ranks = DaughterIntensity(mdata_neg, saveas='neg_daughter_intensity')
    else:
        neg_ms1_ranks = ''
        neg_ms2_ranks = ''

    temp_file = open(ms.cur_dir+os.sep+'Templates'+os.sep+'QC_sheet.html', 'r')
    template = temp_file.readlines()
    written = open(html_file_name+(html_file_name[-5:] != '.html')*'.html', 'w')
    write = True
    replacement_dict = {'pos_peaks_table' : pos_spectrum_rank_table,
                        'neg_peaks_table' : neg_spectrum_rank_table,
                        'pos_parent_intensity_ranks' : pos_ms1_ranks,
                        'neg_parent_intensity_ranks' : neg_ms1_ranks,
                        'pos_daughter_intensity_ranks' : pos_ms2_ranks,
                        'neg_daughter_intensity_ranks' : neg_ms2_ranks}    
    
    for line in template:
        if '--end' in line:
            write = True
        if write:
            if 'neg msall only' in line and neg_msall is None:
                write = False
            if 'pos msall only' in line and pos_msall is None:
                write = False
            if 'pos tof only' in line and pos_tof is None:
                write = False
            if 'neg tof only' in line and neg_tof is None:
                write = False
            if 'pn only' in line and (pos_tof is None or neg_tof is None):
                write = False
            if 'either tof' in line and (pos_tof is None and neg_tof is None):
                write = False
            written.write(replacement(line, replacement_dict))
            
    written.close()
    temp_file.close()
    return None
    
if __name__ == '__main__':
    main('mark_msall_pos.txt', 'mark_msall_neg.txt', 'mark_tof_neg.txt', 'mark_tof_neg.txt')
else:
    import inputs, os
    target_dir = os.sep.join([inputs.new_directory, 'Quality_Control'])
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    from shutil import move
    pos_mode_msall = inputs.pos_mode_data
    neg_mode_msall = inputs.neg_mode_data
    pos_mode_tof = inputs.pos_mode_tof
    neg_mode_tof = inputs.neg_mode_tof
    names_file = inputs.name_table_string
    assert_inputs_exist([pos_mode_msall, neg_mode_msall], names_file, 
                        TOF_files=[pos_mode_tof, neg_mode_tof])
    pos_mode_msall = move_if_not_none(inputs.pos_mode_data, 'Raw_Data')
    neg_mode_msall = move_if_not_none(inputs.neg_mode_data, 'Raw_Data')
    pos_mode_tof = move_if_not_none(inputs.pos_mode_tof, 'TOF_Data')
    neg_mode_tof = move_if_not_none(inputs.neg_mode_tof, 'TOF_Data')
    if len(filter(lambda x : '.html' in x, os.listdir(target_dir))) == 0:
        main(pos_mode_msall, neg_mode_msall, pos_mode_tof, neg_mode_tof,
             html_file_name=inputs.header+'_QC')
        files_to_move = filter(lambda x: 
                               '.png' in x or inputs.header+'_QC' in x,
                               os.listdir(os.getcwd()))
        for f in files_to_move:
            try:
                move(f, target_dir)
            except:
                os.remove(target_dir+os.sep+'+f')
                move(f, target_dir)
   

