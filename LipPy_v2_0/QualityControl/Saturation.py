# -*- coding: utf-8 -*-
"""
Created on Thu Apr 09 13:12:08 2015

@author: Mike
"""
import matplotlib.pyplot as plt
from Molecules.Lipids import Unknown
import os
import shutil

def sample_saturation(spectrum, f_header=None):
    spec_name = spectrum.name
    num_acceptable = 0
    i = 0
    image_files = []
    spectrum.sort(ascending=False)
    while num_acceptable < 5 and i < 20 :
        pair = Unknown(None, None, mass_pair=spectrum.index[i])
        m0_intensity = spectrum.ix[i]
        predicted_values = m0_intensity*pair.mass_pair_dist()
        observed_values = spectrum[predicted_values.index.tolist()[:3]] ##:3 selects M00, M10, M11
        predicted_values = predicted_values[observed_values.index]
        cond1 = observed_values.values[1] / predicted_values.values[1] < 1.5
        cond2 = observed_values.values[2] / predicted_values.values[2] < 1.5
        if cond1 and cond2:
            num_acceptable += 1
        if f_header is not None:
            head = f_header+'_'+str(i)
        else:
            head = None
        image_name = isotope_plot(predicted_values, observed_values, f_header=head)
        image_files.append(image_name)
        i += 1
    return [spec_name, image_files]

def isotope_plot(pred, obs, f_header=None):
    ###This will make the plots
    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.bar(range(0, 2*len(pred), 2), pred, color='r', label='Expected')
    ax.bar(range(1, 2*len(obs)+1, 2), obs, color='b', label='Observed')
    ax.set_title(str(pred.index[0]))
    ax.set_xticks(range(1, 2*len(pred.index)+1, 2))
    ax.set_xticklabels(map(str, pred.index), rotation=30)
    ax.set_ylabel('Intensity')
    ax.set_xlabel('MS1_MS2')
    ax.legend()
    fig.tight_layout()
    if f_header is not None:
        plt.savefig(f_header+'.png')
    plt.close(fig)
    return f_header+'.png'

def saturation_report(samples_and_images, filename, directory):
    populate_directory_for_saturation_report(directory)
    f = open(filename, 'w')
    f.write('<html>\n')
    f.write('<title>Saturation Check</title>')
    for sample, images in samples_and_images:
        f.write('<h2>'+sample+'</h2>\n')
        for image in images:
            image_path_html = '"Images'+os.sep+image+'"'
            image_html = '<img src='+image_path_html+'>\n'
            f.write(image_html+'\n')
            if os.path.exists(directory+os.sep+'Images'+os.sep+image):
                os.remove(directory+os.sep+'Images'+os.sep+image)
            shutil.move(image, directory+os.sep+'Images')
    f.write('</html>')
    f.close()
    if os.path.exists(directory+os.sep+filename):
        os.remove(directory+os.sep+filename)
    shutil.move(filename, directory)
    return None
    
def populate_directory_for_saturation_report(directory):
    if not os.path.exists(directory):
        os.makedirs(directory+os.sep+'Images')
    
    
            
	    ###add image line
	    ###going to have to worry about directories for images
	    ###going to have to *make* directories
	    ###analysis is going have to make isotope qc folder

