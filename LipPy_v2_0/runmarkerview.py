# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 14:01:44 2015

@author: S23689
"""
#To make markerview.exe work in the command prompt, the file path was added to the environment variable path. Go to control panel->system and security->security->change settigns->advanced->environment variables
#Save this in LipPY
import sys
import shutil
import os
from Tkinter import *
import tkFileDialog
import subprocess
import pandas as pd
import numpy as np
import collections

#This function modifies strings in text files
def change_string(filename, old_string, new_string):
        s=open(filename).read()
        if old_string in s:
                s=s.replace(old_string, new_string)
                f=open(filename, 'w')
                f.write(s)
                f.close()

directory = r'N:\Alex_T\For_testing_RMV\120915_Plasma_DilSet_PBSvsW'
def throughMV(directory, run = True, additional_runs = None):
    #folder name manipulation
    dirName=directory.replace(r'/',os.sep)
    folderName=os.path.basename(directory)
    commandFilesDir = os.path.join(dirName, 'MarkerView_Files')
    cmdFile = os.path.join(dirName,folderName+'_CMD.bat')
    names_file = os.path.join(dirName,folderName+'_NamesFile.csv')
    raw_data_file = os.path.join(dirName, 'Raw_data')
    tof_data_file = os.path.join(dirName, 'Tof_data')
    for direc in [raw_data_file,tof_data_file]:
        if not os.path.exists(direc):
            os.mkdir(direc)
    if os.listdir(raw_data_file) != []:
        print ('Files located in raw_data. Will not run sames though marker view. If you wish for the files to be made, please stop the program and delete or move the contents of the raw_data file in your folder and re-run')
        return None
    #change strings on command files
    all_files = os.listdir(commandFilesDir)
    text_files = [i for i in all_files if '.txt' in i]
    if all_files != text_files:
        raise IOError('Unexpected file in MarkerView_Files folder please remove or edit and try again')
    for i in text_files:
        change_string(os.path.join(commandFilesDir,i),'temporary', dirName)
    #change strings on CMD files 
    newCMDstr = os.path.join(dirName,'MarkerView_Files')+os.sep
    change_string(cmdFile, 'OutPutDir', newCMDstr)
    change_string(cmdFile, 'RawDataDir', raw_data_file)
    change_string(cmdFile, 'TofDataDir', tof_data_file)
    if run:
        p=subprocess.Popen(cmdFile, shell=True, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            print ('Possible issue with the MarkerView files, please make sure they are correct and complete')
        for i in text_files:
            change_string(os.path.join(commandFilesDir,i), dirName,'temporary')
        change_string(cmdFile,newCMDstr, 'OutPutDir')
        change_string(cmdFile, raw_data_file, 'RawDataDir')
        change_string(cmdFile, tof_data_file, 'TofDataDor')
        
def pack(file_loc):
    input_file = os.path.join(file_loc, 'Input')
    if not os.path.exists(input_file):
        os.makedirs(input_file)
    #output_file = os.path.join(file_loc, 'Output')
    files = os.listdir(file_loc)
    if 'Output' in files:
        files.remove('Output')
    if 'inputs.py' in files:
        files.remove('inputs.py')
    for i in files:
        file_path = os.path.join(file_loc,i)
        shutil.move(file_path, input_file)
        
def unpack(file_loc):
    input_file = os.path.join(file_loc, 'Input')   
    if not os.path.exists(input_file):
        pass
    else:
        files = os.listdir(input_file)
        for i in files:
            file_path = os.path.join(input_file,i)
            shutil.move(file_path, file_loc)
        os.rmdir(input_file)
        print 'Unpacking successful'

MSMSsettingsloc = ''
TOFsettingsloc = ''


def change_MS_setting(setting, value):
    if setting not in ['threshold','ignorefragmentswindow',
                       'masstolerance','maxcompounds',
                       'usepeakarea','ignorefragmentsaboveprecuror']:
        raise ValueError('setting not found')
    text = open(MSMSsettingsloc).read()
    replace = '\n  <%s value="%s" />'%(setting, value)
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))    
    text=re.sub(pattern, replace,text)
    with open(MSMSsettingsloc, 'w') as wr:
        wr.write(text)
        
def change_TOF_setting(setting, value):
    if setting not in ['Operation','masstolerance',
                       'masstoleranceinppm','processpeaklistsmasstolerance',
                       'processpeaklistsmasstoleranceinppm',
                       'massbinsize','massbinsizeinppm',
                       'baselinesubtract','baselinesubtractbins',
                       'minintensity','maxcompounds',
                       'useexclusion','period',
                       'experiment']:
        raise ValueError('setting not found')
    text = open(TOFsettingsloc).read()
    replace = '\n  <%s value="%s" />'%(setting, value)
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))    
    text=re.sub(pattern, replace,text)
    with open(TOFsettingsloc, 'w') as wr:
        wr.write(text)
        
    