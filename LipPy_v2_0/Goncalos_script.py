# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 12:13:25 2016

@author: Alex
"""
import pandas as pd
import numpy as np
import shelve
import os
import LipPy_v2_0.MS_classes as ms
from LipPy_v2_0.MS_classes import concat_names_standards
import LipPy_v2_0.Stats.LipidFunctions as lf
import LipPy_v2_0.Stats.stats as stats
from LipPy_v2_0.Lookups import name_lookup
import collections
import time
from multiprocessing.pool import ThreadPool

#standard locater
import time

start = time.time()


new_folder = 'contaminant'
name_file = '022816_NASH13_std1_namesfile.csv'
ms_pos_raw_file = '022816_NASH13_std1-MSALL.txt'
ms_neg_raw_file = '022816_NASH13_std1+MSALL.txt'
standard_mix = None#'SPLASH'

root_dir = os.getcwd()
output_dir = os.path.join(root_dir,'Output')
new_dir = os.path.join(output_dir,new_folder)
if not os.path.exists(new_dir):
    os.makedirs(new_dir)
raw_data_dir = os.path.join(root_dir,'Raw_Data')
names_file_file = os.path.join(root_dir,'Names_Files')
ms_pos_dir = os.path.join(raw_data_dir,ms_pos_raw_file)
ms_neg_dir = os.path.join(raw_data_dir,ms_neg_raw_file)
name_file_dir = os.path.join(names_file_file,name_file)    

print ('Importing')
msall_pos = ms.MS_Data.from_raw_data_files(ms_pos_dir, name_file_dir, standard_mix = standard_mix, mode='+')
msall_neg = ms.MS_Data.from_raw_data_files(ms_neg_dir, name_file_dir, standard_mix = standard_mix, mode='-')

print ('Filtering')
pos_filtered = msall_pos.all_filters(msall_pos.sample_cols,avg_thresh=100,NL_thresh=-1,x_gt_y=(30,100), child_peaks=[(1,10)])
neg_filtered = msall_neg.all_filters(msall_neg.sample_cols,avg_thresh=100,NL_thresh=-1,x_gt_y=(30,100), child_peaks=[(1,10)])

print('Nameing')
pos_named = pos_filtered.Name_Molecules()
neg_named = neg_filtered.Name_Molecules()
named = pos_named.combine(neg_named)
















def iterable_to_excel(iterable, saveas, sort_by_name = True):
    '''
    used to pass dictionarys of and sigle objects of MS_classes to excel
    must not have 'mass_pairs' column in to work
    used lipids_to_str to change the class of lipids to a stirng to it can write
    '''
    if iterable is None: #do nothing if passed None-type
        return None
    if not saveas.endswith('.xlsx'):
        saveas = saveas.split('.')[0]+'.xlsx'
    if len(iterable) > 0:
        with pd.ExcelWriter(saveas) as writer:
            if hasattr(iterable, 'keys'):
                to_iter = iterable.items()
            else:
                to_iter = iterable
            for key, val in to_iter:
#                print key
                if hasattr(val, 'data_frame'):
                    write_index = not val.data_frame.index.name is None ##want to write index if it's of type liipd
                    
                elif hasattr(val, 'index'):
                    write_index = not val.index.name is None
                else:
                    raise AttributeError(val.__class__.__name__+' has no attribute index or data_frame.')
                df = lipids_to_str(val)
                if 'Mass_Pair' in df.columns.tolist():
                    df.drop('Mass_Pair', axis = 1, inplace = True)
                if 'Name' in df.columns.tolist() and sort_by_name == True:
                    df.sort('Name', inplace = True)
                df.to_excel(writer, sheet_name=key, index=write_index, float_format='%.4f')
            writer.save()
    return None

def lipids_to_str(ms):
    '''
    used by iterable_to_excel to make lipids classes into strings so it can 
    write to excel
    '''    
    if hasattr(ms, 'data_frame'):
        return lipids_to_str(ms.data_frame)
    else:
        df = ms.copy()
        if 'Name' in df.columns:
            df['Name'] = df.Name.apply(lambda x : str(x))
        if 'Group' in df.columns:
            df['Group'] = df.Group.apply(lambda x : str(x))
        return df