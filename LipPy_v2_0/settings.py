# -*- coding: utf-8 -*-
"""
Created on Tue Feb 17 19:03:13 2015
Contains parameters to be set for LipPy
@author: Mike
"""

SIGNIFICANCE = 0.05
OMICS = 'Counts'
LOG_SUMMARY = True
SUMMARY_DIRECTORY = 'Summaries'