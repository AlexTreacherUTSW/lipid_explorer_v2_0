# -*- coding: utf-8 -*-
"""
Created on Mon Sep 22 13:27:35 2014

@author: JMLab1
"""
import pandas as pd
import numpy as np
#look into
import Stats.LipidFunctions as lf
import Stats.stats as stat
from Molecules.Lipids import *
from collections import defaultdict, OrderedDict
# look into
from scipy.stats import ttest_ind, ranksums
#for probability?
from itertools import combinations
#look into
from Stats.Rescaling import *
from settings import *
#?
from utilities import quick_read
from Lookups import name_lookup
#for locate
from bisect import bisect_left
import copy
import pdb

from sklearn.decomposition import PCA as sklPCA
import matplotlib.pyplot as plt
import os
import shelve
from random import sample
pd.options.mode.chained_assignment = None
cur_dir = os.path.dirname(os.path.realpath(__file__))+os.sep
template_dir = cur_dir+'Templates'+os.sep
test_dir = cur_dir+'TestData'+os.sep
lookup_dir = cur_dir+'Lookups'+os.sep
standards_db = shelve.open(lookup_dir+'Standards_DB')
symbols = ['v', '>', '^', '<', 'o', 's', 'p']

class MS_Process:
    
    def __init__(self, data, names, mode, standard_mix = None, omit=None):#,std_amt = 20):
        """
        
        """
        self.names_file = names
        self.omit = omit
        self.mode = mode
        self.data_file = data
        self.mass_cols = ['Mass_Pair', 'Precursor', 'Fragment', 'neutral_loss', 'Mode']
        self.standard_mix = standard_mix
        if names:
            self.name_table = self.read_names()
            self.sample_names = self.name_table.Name.tolist()
            self.name_table = self.read_names()
        else:
            pass
        self.clean_data = self.pre_process()
            
        
    def pre_process(self):
        """
        Processes MS/MS of everything raw text file into a pandas data frame
        with the correct columns.  Name table must be read before this is called.
        """
        df = pd.read_table(self.data_file, skiprows = [1,2], header=0, index_col = None)
        if df.columns.tolist() == ["This sample is corrupted. Select another sample or file. (Could not find file 'N:\\Alex_T\\For_testing_RMV\\120915_Plasma_DilSet_PBSvsW\\-MSALL\\Liver_Mix_Standard_Start-MSALL.wiff'.)"]:
            raise IOError('MarkerView file corrupted, please change and rerun')
        try:
            Pair = df['Sample Name'].apply(lambda x: MassPair.from_string(x, ascending=True))
        except AttributeError:
            raise IOError('Input MSAll file'+self.data_file+' flagged as likely TOF.')
        del df['Sample Name']
        Precursor = Pair.apply(lambda x : x.ms1)
        Fragment = Pair.apply(lambda x: x.ms2)
        Neutral_Loss = Precursor - Fragment
        self.sample_names = self.name_table.sort_values('Sample_Number').Name.tolist()
        if df.columns.tolist() == self.sample_names:
            print('Names file matched up with names on sample Data')
        else:
            print ('Names file does not match up with names on sample Data, please check for alignment issues')
            df.columns = range(1,df.shape[1]+1)
            try:
                df = df[df.columns.intersection(self.name_table.Sample_Number)]
                df.columns = self.sample_names
            except:
                self.sample_names = map(lambda x : 's'+str(x), df.columns)
                table_gen = pd.DataFrame({'Sample_Number' : df.columns.tolist(),
                                          'Group' : ['NA']*len(df.columns),
                                          'Name' : self.sample_names,
                                          'Group_Name' : ['NA']*len(df.columns)})
                self.name_table = table_gen
                df.columns = self.sample_names        
                           
        df['Mass_Pair'] = Pair
        df['Precursor'] = Precursor
        df['Fragment'] = Fragment
        df['neutral_loss'] = Neutral_Loss
        df['Mode'] = self.mode
        cols = self.mass_cols+self.sample_names
        '''
        df = pd.read_table(self.data_file, skiprows=
                            [0,1,2], header=None, index_col=None)
        df = df.drop_duplicates(0)
        try:
            Fragment = df[0].apply(lambda x : float(x.split('_')[0]))
        except AttributeError:
            raise IOError('Input MSAll file'+self.data_file+' flagged as likely TOF.')
        Pair = df[0].apply(lambda x: MassPair.from_string(x, ascending=True))
        del df[0]
        Precursor = Pair.apply(lambda x : x.ms1)
        Fragment = Pair.apply(lambda x: x.ms2)
        Neutral_Loss = Precursor - Fragment
        try:
            df = df[df.columns.intersection(self.name_table.Sample_Number)]
            df.columns = self.sample_names
        except:
            self.sample_names = map(lambda x : 's'+str(x), df.columns)
            table_gen = pd.DataFrame({'Sample_Number' : df.columns.tolist(),
                                      'Group' : ['NA']*len(df.columns),
                                      'Name' : self.sample_names,
                                      'Group_Name' : ['NA']*len(df.columns)})
            self.name_table = table_gen
            df.columns = self.sample_names        
                    
        df['Mass_Pair'] = Pair
        df['Precursor'] = Precursor
        df['Fragment'] = Fragment
        df['neutral_loss'] = Neutral_Loss
        df['Mode'] = self.mode
        cols = self.mass_cols+self.sample_names
        '''
        return df[cols]
         
    def read_names(self):
        """
        Parses name table, checking that it is properly formmated along the way.
        Returns the parsed table.
        """
        name_table = pd.read_csv(self.names_file, skiprows=self.omit)
        sanitize = lambda x : x.strip().title().replace(' ', '_').replace('.', '_')
        ##capitalize first letter of each word in name table columns and replace whitespace with underscores
        name_table.columns = map(sanitize, name_table.columns) 
        example_cols = ['Sample_Number','Group','Name','Group_Name', 'Sample_Amount','Standard_Volume']
        #map(sanitize, pd.read_csv(lookup_dir+'names_checker.csv').columns)
        try:
            assert 'Sample_Number' in name_table.columns
            assert 'Group' in name_table.columns
            assert 'Name' in name_table.columns
            assert 'Group_Name' in name_table.columns     
            for heading in name_table.columns:
                assert heading in example_cols
        except:
            raise IOError('Column headings in %s not properly formatted.  Column heads should be: \'Sample_Number\' \'Group\', \'Group_Name\' followed by optional columns: \'Sample_Amount\', \'Standard_Volume\'')
            #name_table.columns = ['Sample_Number', 'Group', 'Name', 'Group_Name', 'Type']
        if 'Standard_Volume' not in name_table.columns:
            name_table['Standard_Volume'] = 20
        name_table['Group_Name'] = name_table['Group_Name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_')) ##Remove trailing whitespace in group names    
        name_table['Name'] = name_table['Name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_'))
        try:
            assert all([isinstance(name_table.Sample_Number[i], (int, np.int64, np.float64)) 
                        for i in name_table.index])
            assert all([isinstance(name_table.Group[i], (int, np.int64, np.float64))
                        for i in name_table.index])
        except AssertionError:
            raise IOError('Not all entries in the Sample Number or Group columns in your names file are integers.  Correct your names file and try again.')
        if name_table['Name'].duplicated().any():
            raise IOError('File '+self.names_file+' contains duplicates in the sample names column.  Give each sample a distinct name and run again.')
        return name_table
        
    def saturation_check(self, filename, directory):
        from QualityControl.Saturation import sample_saturation, saturation_report
        mode_dict = {'+' : 'pos', '-' : 'neg'}
        df = self.clean_data.copy()
        if filename.endswith('.html'):
            filename = filename.strip('.html')+'_'+mode_dict[self.mode]+'.html'
        else:
            filename = filename+'_'+mode_dict[self.mode]+'.html'
        #df.Mass_Pair = df.Mass_Pair.apply(MassPair.from_string)
        sample_cols = df.columns.difference(self.mass_cols)
        df.index = df.Mass_Pair
        images = []
        for c in sample_cols:
            spectrum = df[c].copy()
            new_images = sample_saturation(spectrum, f_header=c)
            images.append(new_images)
        saturation_report(images, filename, directory)
    
    def to_MS_data(self):
        """
        
        """
        if self.standard_mix == None:
            stand_dict = {'Observed' : {'+' : None, '-' : None}, 'Input' : {'+' : None, '-' : None}}
        elif self.standard_mix not in standards_db.keys() or self.standard_mix == None:
            raise ValueError('standard mix name not in standard mix\'s')
        else:
            stand_dict = {'Observed' : {'+' : None, '-' : None}, 'Input' : {'+' : None, '-' : None}}
            standards = standards_db[self.standard_mix]
            pos = standards[standards.Polarity == '+']
            neg = standards[standards.Polarity == '-']
            pos['Name'] = pos['Name']#.apply(lambda x : Standard(x, '+'))
            neg['Name'] = neg['Name']#.apply(lambda x : Standard(x, '-'))
            standards = pd.concat([pos, neg])
            standards.index = standards.Name
            stand_dict['Input']['+'] = standards[standards.Polarity == '+']
            stand_dict['Input']['-'] = standards[standards.Polarity == '-']
            stand_dict['Input']['+'].index =  stand_dict['Input']['+'].Normalize_Group
            stand_dict['Input']['-'].index = stand_dict['Input']['-'].Normalize_Group
            #stand_dict['Input']['+'].index =  stand_dict['Input']['+'].Group
            #stand_dict['Input']['-'].index = stand_dict['Input']['-'].Group
            #stand_dict['Amount'] = self.std_amt
        return MS_Data(self.clean_data, self.name_table, mode=self.mode, ms_standards=stand_dict)

    
class MS_Data:
    """
    Represents a dataset from a tandem mass spectrometry experiment.  The object
    is specified by two pandas DataFrames
        1) The data frame with the ms1, ms2, and sample intensities.
        2) The names_table data frame.
    
    Implements common procedures such as filtering and naming of molecules, 
    hypothesis tests, normalizations, restriction to types and groups of lipids, 
    data subsetting by dropping of groups, combination with other MS_data objects.
    
    
    Parameters
    ----------
    data_frame : pandas.DataFrame
        Processed and parsed data from the mass spectrometer.  Must have
        columns Mass_Pair, Precursor, Fragment, neutral_loss.
    
    names : pandas.DataFrame
        Contains sample numbers,sample names,  group names, group numbers, and
        optional columns that may contain information on standards and sample 
        weight.
    
    mode : string or NoneType -- '+', '-', or None -- default None
        The mode that the dataset has been obtained in.  This affects name and
        standard lookups.  Combined positive and negative mode data has mode None.
        
    ms_standards : dict or None -- default None
        If the data set was run without standards, this should be None.  If the
        data set was run with standards, the input format is as follows:
        
            self.standards = {'Observed' : {'+' : pos_observed_standards, '-' : neg_observed_standards}, 
                                  'Input' : {'+' : pos_input_standards, '-' : neg_input_standards}}

        All terminal values must be pandas DataFrames.  Note that the terminal 
        values for 'Observed' may be None if the molecules have not yet been named;
        these will be filled in when the Name_Molecules method is called.
        Initializing an object by passing files through the MS_Process object
        pipeline will keep the programmer from having to deal with the ms_standards
        input.
        
    
    Examples
    --------
    >>> pos_data_set = MS_data(pos_data, names, mode='+')
    >>> neg_data_set = MS_data(neg_data, names, mode='-')
    >>> data_set = pos_data_set.combine(neg_data_set)
    >>> named_data = data_set.Name_Molecules()
    >>> normed_named_data = named_data.normalize()
    
    See also
    --------
    LipPy.MS_data.quick_read_from_files
    LipPy.MS_data.from_raw_data_files
    LipPy.MS_Process
    """
    @staticmethod
    def quick_read_from_files(data_file, name_file,  mode=None, ms_standards=None):
        """
        Constructs an MS_data object from a given data_file and name_file which
        contain already-processed data (perhaps from a previous analysis).
        Currently, it is difficult to work with datasets that contain standards
        this way, but it is very useful for reviewing past analyses.
        
        Utilizes the quick_read function from LipPy.utilities, which uses the
        file's extension (.xlsx, .csv, .txt) to select the correct reader function.
        
        'parent' and 'daughter' are replaced in columns of DataFrames with 'Precursor'
        and 'Fragment, respectively, to ensure some degree of backwards compatibility.
        
        It is not recommended that this be used in production code because it has
        not been thoroughly tested (feel free to test it yourself), but it is 
        quite a time-saver for one-off analyses and could be useful for
        meta-analyses if some tests can be imagined and written and errors can
        be handled more gracefully.
        
        Returns
        ----------
        An MS_data object
    
        Parameters
        ----------
        data_file : string
            The name of the file containing the experimental data.  This can be
            a csv, txt, xls, or xlsx file.  Note that performance is best on csv
            and txt files.  If your data is in an Excel workbook, it is
            recommended that you save the sheet that you want to use as a csv
            or txt file before passing it to this method.
            
        name_file : string
            The name of the file containing the experimental data.  This can be
            a csv, txt, xls, or xlsx file.  Note that performance is best on csv
            and txt files.  If your data is in an Excel workbook, it is
            recommended that you save the sheet that you want to use as a csv
            or txt file before passing it to this method.
            
        mode : string or NoneType -- '+', '-', or None -- default None
            The mode that the dataset has been obtained in.  This affects name and
            standard lookups.  Combined positive and negative mode data has mode None.
    
        ms_standards : dict or None -- default None
            If the data set was run without standards, this should be None.  If the
            data set was run with standards, the input format is as follows:
            
                self.standards = {'Observed' : {'+' : pos_observed_standards, '-' : neg_observed_standards}, 
                                      'Input' : {'+' : pos_input_standards, '-' : neg_input_standards}}
    
            All terminal values must be pandas DataFrames.  Note that the terminal 
            values for 'Observed' may be None if the molecules have not yet been named;
            these will be filled in when the Name_Molecules method is called.
            Initializing an object by passing files through the MS_Process object
            pipeline will keep the programmer from having to deal with the ms_standards
            input.
            
        Examples
        --------
            >>> data_set = MS_data.quick_read_from_files('data_name.xlsx', 'names.xlsx')
            
        See also
        --------
            LipPy.utilities.quick_read
        """
        MSMS_all = quick_read(name_file)
        MSMS_all.rename(columns={'parent' : 'Precursor', 'daughter' : 'Fragment'}, inplace=True)
        names = quick_read(name_file)
        return MS_Data(MSMS_all, names, mode=mode, ms_standards=ms_standards)

    @staticmethod
    def from_raw_data_files(raw_data, name_file, mode, standard_mix = None, omit=None):
        """
        Constructs an MS_data object from a raw data files.
        
        Returns
        ----------
        An MS_data object
    
        Parameters
        ----------
        raw_data : string
            The name of the file containing the raw experimental data.
            
        name_file : string
            The name of the file with the name table.  This can be none.
            
        mode : string or NoneType -- '+', '-', or None -- default None
            The mode that the dataset has been obtained in.  This affects name and
            standard lookups.
            
        Examples
        --------
            >>> data_set = MS_data.from_raw_data_files('data_name.txt', 'names.csv', '+')
        """
        process = MS_Process(raw_data, name_file, mode, standard_mix = standard_mix, omit=omit)
        data = process.to_MS_data()
        return data
        
    def __init__(self, data_frame, names, mode=None, ms_standards=None):
        self.data_frame = data_frame
        self.name_table = names
        self.mass_cols = ['Mass_Pair', 'Precursor', 'Fragment', 'neutral_loss', 'Mode']
        self.sample_dict = dict(zip(self.name_table.Sample_Number, self.name_table.Name))
        self.sample_cols = self.sample_dict.values()
        self.group_num_dict = dict(zip(self.name_table.Group, self.name_table.Group_Name))
        self.groups_dict = self.get_groups_dict()
        self.mode = mode
        if ms_standards is None:
            self.standards = {'Observed' : {'+' : None, '-' : None}, 
                              'Input' : {'+' : None, '-' : None}}
        else:
            self.standards = ms_standards
        self.name_cols = ['Name', 'Group', 'Type', 'MS1_Formula', 'MS2_Formula']
        if mode != None or 'Mode' not in self.data_frame.columns and mode != 'Mixed':
            self.data_frame['Mode'] = self.mode
        try:
            self.data_frame = self.data_frame[self.name_cols+self.mass_cols+self.sample_cols]
        except KeyError:
            self.data_frame = self.data_frame[self.mass_cols+self.sample_cols]
            
    def shape(self):
        """
        Parameters
        ----------
        None
        
        Returns
        ----------
        A numpy array containing the dimensions of the experimental data set.
        """
        return np.shape(self.data_frame)
        
    def __len__(self):
        return len(self.data_frame.index)
        
    def __repr__(self):
        return self.data_frame.__repr__()
            
    def combine(self, other):
        """
        Combines self with another MS_Data object with the same name_table into
        a single MS_Data object.
        Parameters
        ----------
        other : MS_Data
            The MS_Data object that will be combined with self.  self and other
            should have different modes, else a warning will be raised -- though
            the code will still run.
        
        Returns
        ----------
        An MS_Data object with the experimental data combined into one DataFrame
        with the modes labeled and the standard dictionaries combined.
        """
        new_mode = None
        new_standards = self.standards.copy()
        if other is None:
            return self
        else:
            try:
                assert self.mode != other.mode
            except AssertionError:
                print "Warning: modes of MS_Data objects to be combined are both "+self.mode+"."
                new_mode = self.mode
                
            new_df = pd.concat([self.data_frame, other.data_frame])
            if new_df.index.duplicated().any():
                try:
                    assert new_df.index.is_integer()
                    new_df = new_df.reset_index(drop=True)
                except AssertionError:
                    raise ValueError('Duplicates in non-integer index.  Find Mike immediately!')
            for i in ['Observed', 'Input']:
                new_standards[i][self.mode] = self.standards[i][self.mode]
                new_standards[i][other.mode] = other.standards[i][other.mode]
            if new_df.index.duplicated().any():
                new_df = new_df.reset_index(drop=True)
            return MS_Data(new_df, self.name_table, mode=new_mode, ms_standards=new_standards)

    def groups_present(self, as_str=True):
        """
        Returns
        ----------
        A list of lipid groups present in the data set.  If data are not named,
        the Name_Molecules method will be called to determine which groups are
        present.
        
        Parameters
        ----------
        as_str : bool, default True
            If True, a list of strings is returned, if False, the class objects
            themselves populate the list.
        
        See also
        ----------
        MS_data.types_present()
        """
        if not 'Group' in self.data_frame.columns:
            return self.Name_Molecules().groups_present()
        else:
            groups = self.data_frame.Group.drop_duplicates().tolist()
            for g in groups:
                if 'standard' in g.lower():
                    groups.remove(g)
            try:
                groups.remove('Unknown')
            except ValueError:
                pass
            if as_str:
                if not all([isinstance(g, str) for g in groups]):
                    groups = map(lambda x : x.__name__ 
                                            if type(x)==type else str(x),
                                                                  groups)
        return groups
            
    def types_present(self, as_str=True):
        """
        Returns
        ----------
        A list of lipid types present in the data set.  If data are not named,
        the Name_Molecules method will be called to determine which groups are
        present.
        
        Parameters
        ----------
        as_str : bool, default True
            If True, a list of strings is returned, if False, the class objects
            themselves populate the list.
        
        See also
        ----------
        MS_data.groups_present()
        MS_data.restrict_to_group
        MS_data.restrict_to_type
        """
        if not 'Group' in self.data_frame.columns:
            return self.Name_Molecules().types_present()
        else:
            types = self.data_frame.Type.drop_duplicates().tolist()
            try:
                types.remove('Unknown')
            except ValueError:
                pass
            if as_str:
                if not all([isinstance(t, str) for t in types]):
                    types = map(lambda x : x.__name__ 
                                          if type(x)==type else str(x), types)
            return types

    def get_groups_dict(self):
        """
        Constructs group dictionary with the group names as keys.
        Use attribute self.groups_dict instead.
        """
        groups_dict = defaultdict(list)
        for group in self.name_table.Group.drop_duplicates():
            groups_dict[group] = self.name_table[self.name_table.Group == group].Name.tolist()    
        return groups_dict

    def groups_in_types_dict(self , exclude_standards = True):
        '''
        returns a dict of each type as the key, and groups in each type 
        as the values 
        
        if exclude_standards == Ture remove the standards from the list
        '''        
        dct = {}
        to_skip = ['Isotope', 'Misc','Unknown' , 'PA']
        for t in self.types_present():
            if t not in to_skip:            
                t_df = self.data_frame[self.data_frame['Type'] == t]
                groups_list = list(set((t_df['Group'])))
                for r in to_skip:
                    if r in groups_list:
                        groups_list.remove(r)
                dct[t] = groups_list
        if exclude_standards == True:
            dct = {k:[elem for elem in v if 'standard' not in elem.lower()] for k,v in dct.iteritems()}
        return dct

    def center_samples(self):
        samples = self.data_frame[self.sample_cols].copy()
        centered = samples - samples.mean(axis=1)
        other_cols = [c for c in self.data_frame.columns if c not in self.sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([centered, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def standardize_samples(self):
        samples = self.data_frame[self.sample_cols].copy()
        standardized = (samples - samples.mean(axis=1)) / samples.std(axis=1)
        other_cols = [c for c in self.data_frame.columns if c not in self.sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([standardized, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def pareto_scale_samples(self):
        samples = self.data_frame[self.sample_cols].copy()
        par_scaled = (samples - samples.mean(axis=1)) / np.sqrt(samples.std(axis=1))
        other_cols = [c for c in self.data_frame.columns if c not in self.sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([par_scaled, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def log_transform_samples(self, base='exp'):
        samples = self.data_frame[self.sample_cols].copy()
        if base == 'exp':
            b = np.e
        log_transformed = np.log(samples) / np.log(b) ##denominator is for log change-of-base formula
        other_cols = [c for c in self.data_frame.columns if c not in self.sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([log_transformed, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def range_scale_data(self):
        samples = self.data_frame[self.sample_cols].copy()
        centered = samples - samples.min(axis=1)
        rng = samples.max(axis=1) - samples.min(axis=1)
        range_scaled = centered / rng
        other_cols = [c for c in self.data_frame.columns if c not in self.sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([range_scaled, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)

    ##Filters
    def NL_Filter(self, threshold=-1):
        """
        Neutral loss filter.  Returns MS_data where the new data_frame contains
        only entries where the neutral loss is greather than threshold.
        By default, threshold = -1
        """
        df = self.data_frame
        filtered_data = df[df.neutral_loss > threshold]
        filtered_data = filtered_data#.reset_index(drop=True)
        return MS_Data(filtered_data, self.name_table, self.mode, self.standards)
    
    def Low_Average_Filter(self, threshold, samples_to_filter):
        """
        
        """
        return MS_Data(lf.Remove_Low_Average(self.data_frame, samples_to_filter, average_threshold=threshold), self.name_table, self.mode, self.standards)
    
    def Quantile_Filter(self, x_gt_y, samples_to_filter):
        """
        
        """
        return MS_Data(lf.Remove_Low_Freq(self.data_frame, samples_to_filter, under_thresh=x_gt_y), self.name_table, self.mode, self.standards)
    
    def Remove_Child_Peaks(self, peaks_tuple):
        """
        
        """
        return MS_Data(lf.Child_Peak_Filter(self.data_frame, peaks_tuple), self.name_table, self.mode, self.standards)
    
    def all_filters(self, samples, avg_thresh=30, NL_thresh=-1, x_gt_y=(30, 30), child_peaks=[(1, 10)]):
        filtered_data = self.Remove_Child_Peaks(child_peaks)
        filtered_data = filtered_data.Low_Average_Filter(avg_thresh, samples)
        filtered_data = filtered_data.NL_Filter(NL_thresh)
        filtered_data = filtered_data.Quantile_Filter(x_gt_y, samples)
        return filtered_data
    
    def set_baseline(self, baseline):
        new_df = self.data_frame.copy()
        info_cols = new_df.columns.tolist()
        for i in self.sample_cols:
            if i in info_cols:
                info_cols.remove(i)        
        info_df = new_df[info_cols]
        sample_df = new_df[self.sample_cols]
        num = sample_df._get_numeric_data()       
        num[num < baseline] = baseline
        baseline_df = pd.concat([info_df,sample_df],axis = 1)
        return MS_Data(baseline_df, self.name_table, self.mode, self.standards) 
    
    def Name_Molecules(self, by_name=True, to_name = None):
        """
        names molecules based on Name_Data in lipidfunctions
        to_name should be a sting that is a key of the name_lookup DB
        if not it will default to name_lookup[self.mode]
        
        """
        if self.mode not in ['-', '+']:
            pos = self.restrict_to('Mode', '+', '==')
            neg = self.restrict_to('Mode', '-', '==')
            pos.mode = '+'
            neg.mode = '-'
            pos_named = pos.Name_Molecules(by_name=by_name)
            neg_named = neg.Name_Molecules(by_name=by_name)
            if by_name:
                return pos_named.combine(neg_named).sort('Name')
            else:
                return pos_named.combine(neg_named)
        else:
            if to_name == None:
                naming_table = name_lookup[self.mode]
            else:
                try:
                    naming_table = name_lookup[to_name]
                except KeyError:
                    naming_table = name_lookup[self.mode]
            ##lookup_df has the standards concatenated with the names list
            lookup_df = concat_names_standards(naming_table,
                                               self.standards['Input'][self.mode],
                                               self.mode)
            named = lf.Name_Data(self.data_frame, lookup_table_handle=lookup_df)
            #ISSUE!!!!!!!!!!!!!!!!!!!!!!
            new_standards = named[named.Group.str.contains('Standard')]
            #named = named[named.Group != 'Standard']
            names_and_groups = named[['Group', 'Name']].copy()
            masses = named[['Precursor', 'Fragment']].copy()
            mass_pairs = masses.apply(lambda x: MassPair(x[0], x[1]), axis=1)
            lipid_info = pd.concat((names_and_groups, mass_pairs), axis=1)
            lipid_info.columns = ['Group', 'Name', 'Masses']
            #next line turns name in to lipid class
            names = lipid_info.apply(lambda x: Lipid.from_group(x.Group,
                                                                x.Name,
                                                                self.mode,
                                                                mass_pair=x.Masses), 
                                                                axis=1)
            named['Name'] = names
            named['MS1_Formula'] = names.apply(lambda x: x.formula_string()[0])
            named['MS2_Formula'] = names.apply(lambda x: x.formula_string()[1])
            named['Type'] = names.apply(lambda x: x.get_type()) 
            named = named[named.Group != 'Labels']
            named = named[named.Group != 'labels']
            new_stand_dict = self.standards.copy()
            new_stand_dict['Observed'][self.mode] = new_standards
            new_stand_dict['Observed'][self.mode].index = new_standards.Name
            new_stand_dict['Observed'][self.mode] = new_stand_dict['Observed'][self.mode][self.mass_cols+self.sample_cols]            
            return MS_Data(named, self.name_table, self.mode, new_stand_dict)
    
    def samples_missing_standard(self):
        '''
        returns the samples that are calculated to me missing the standard
        (uses outliers)
        be carefull that if it is a pos and neg run that both have the same
        missing stnadards!
        ''' 
        if self.standards == None:
            return []
        else:
            pos_missing = []
            neg_missing = []
            if self.mode == None or self.mode == '+':
                outliers = stat.is_outlier_df(self.standards['Observed']['+'], self.sample_cols)
                pos_missing.extend(outliers[1])
                if self.mode == '+':
                    return pos_missing
            if self.mode == None or self.mode == '-':
                outliers = stat.is_outlier_df(self.standards['Observed']['-'], self.sample_cols)
                neg_missing.extend(outliers[1])
                if self.mode == '-':
                    return neg_missing
            if self.mode == None:
                if set(pos_missing) != set(neg_missing):
                    print ('WARNING! in pos mode %r are missing standard, \n and in neg mode %r are missing standard' %(pos_missing, neg_missing))
                    print ('Only samples that are missing std in both modes will be ignored')
                both_missing = set(pos_missing)&set(neg_missing)
                return both_missing
                
    def get_all_spectra(self, reindex=None):
        df = self.data_frame.copy()
        if reindex is not None:
            try:
                assert reindex in self.data_frame.columns
            except AssertionError:
                raise IndexError(str(reindex)+' not in data frame columns.')
            df.index = df[reindex]
        return df[self.sample_cols]
        
    def deconvolve_single_spectrum(self, sample):
        if self.mode is None:
            raise ValueError('Cannot deconvolve spectrum with NoneType mode.')
        spec = self.get_spectrum(sample, reindex='Mass_Pair')
        observed_lipids = self.data_frame.Name
        for lpd in observed_lipids:
            num_M0M0 = spec[lpd.mass_pair]
            if isinstance(num_M0M0, (float, int, np.int64)) or num_M0M0 == 0:
                predicted = lpd.mass_pair_dist(N=num_M0M0)
                if len(predicted) > 1:
                    try:
                        assert (predicted > -0.1).all()
                    except AssertionError:
                        raise ValueError('method deconvolve_single_spectrum not working as intended.')
                    corrected = spec.ix[predicted.index][1:] - predicted[1:]
                    for l in corrected.index:
                        if l in spec.index:
                            spec.loc[l] = corrected[l]
                        else:
                            spec[l] = -predicted[l]
            else:
                print num_M0M0
                raise ValueError('')
        return spec
        
    def deconvolve_all_spectra(self, cols='all'):
        if self.mode is None:
            pos = self.restrict_to('mode', '+').deconvolve_all_spectra()
            neg = self.restrict_to('mode', '-').deconvolve_all_spectra()
            return pos.combine(neg)
        if cols == 'all':
            cols = self.sample_cols
        all_spectra = pd.DataFrame()
        for c in cols:
            assert c in self.sample_cols or type(c) == int
            all_spectra[c] = self.deconvolve_single_spectrum(c)
        spectra = all_spectra.replace(np.nan, 0)
        molecule_info = self.data_frame[self.data_frame.columns.difference(self.sample_cols)]
        molecule_info.index = molecule_info.Mass_Pair
        df = pd.concat([molecule_info, spectra], axis=1)
        df.reset_index()
        ###
        df = df[self.name_cols+self.mass_cols+spectra.columns.tolist()].reset_index(drop=True)
        names = self.name_table[self.name_table.Name.apply(lambda x: x in spectra.columns.tolist())]
        return MS_Data(df, names, mode=self.mode, ms_standards=self.standards)

    def get_spectrum(self, sample, reindex=None):
        """
        Gets the spectrum
        """
        df = self.data_frame.copy()
        if reindex is not None:
            try:
                assert reindex in self.data_frame.columns
            except AssertionError:
                raise IndexError(str(reindex)+' not in data frame columns.')
            df.index = df[reindex]
        if sample in self.sample_cols:
            return df[sample]
        elif isinstance(sample, int):
            sample = self.sample_cols[sample]
            return self.get_spectrum(sample, reindex=reindex)
        else:
            raise IndexError("Bad data type for get_spectrum")
            
    def sort(self, column, ascending=False):
        return MS_Data(self.data_frame.sort_values(column, ascending=ascending), self.name_table, self.mode, self.standards)

    def normalize(self, separate_modes=False):
        if separate_modes and self.mode == None:
            pos = self.restrict_to('Mode', '+', '==').normalize(separate_modes=False)
            neg = self.restrict_to('Mode', '-', '==').normalize(separate_modes=False)
            return pos.combine(neg)
        normalized = lf.normalizer(self.data_frame, self.sample_cols)
        return MS_Data(normalized, self.name_table, self.mode, self.standards)
    
    def mol_normalize(self):
        normalized = lf.molar_norm(self.data_frame, self.sample_cols)
        return MS_Data(normalized, self.name_table, self.mode, self.standards)
        
    def _get_subset(self, N):
        """
        Returns an MS_data object that contains only the first N entries in 
        self.data_frame.  Useful for testing.
        """
        shrunken_df = self.data_frame.ix[self.data_frame.index[:N]]
        return MS_Data(shrunken_df, self.name_table, self.mode, self.standards)
        
    def remove_samples_without_standard(self):
        ####needs work
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            pos_removed = pos.remove_samples_without_standard()
            neg_removed = neg.remove_samples_without_standard()
            pos_len = len(pos_named.sample_cols)
            neg_len = len(neg_named.sample_cols)
            if pos_len >= neg_len:
                return pos_removed.combine(neg_removed)
            elif pos_len <= neg_len:
                return neg_removed.combine(pos_removed)
            else:
                raise ValueError('Issue with remove_samples_without_standard')
        samples_to_ignore = self.samples_missing_standard()
        new_df = self.data_frame.copy()
        new_name_table = self.name_table.copy()
        #new_standards = copy.deepcopy(self.standards)
        for s in samples_to_ignore:
            del new_df[s]
            new_name_table = new_name_table[new_name_table.Name != s]
            #del new_standards['Observed'][self.mode][s]
            #del new_standards['Input'][self.mode][s]
        return MS_Data(new_df, new_name_table, self.mode, self.standards)
    
    def standard_normalize(self):
        if 'Name' not in self.data_frame.columns:
            named = self.Name_Molecules()
            return named.standard_normalize()
            print ('Data needs to be named before this can be called')
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            pos_missing = pos.samples_missing_standard()
            neg_missing = neg.samples_missing_standard()
            if set(pos_missing)!=set(neg_missing):
                self.samples_missing_standard = set(pos_missing)&set(neg_missing)
                print ('WARNING! Missing standards miss match',
                       '%r where found to be missing standard in pos' %(pos_missing),
                       'and %r where found to be missing standard in neg' %(neg_missing))
            #pos_to_norm = pos.remove_samples_without_standard()
            #neg_to_norm = neg.remove_samples_without_standard()
            pos_norm = pos.standard_normalize()
            neg_norm = neg.standard_normalize()
            pos_len = len(pos_norm.sample_cols)
            neg_len = len(neg_norm.sample_cols)
            if pos_len >= neg_len:
                return pos_norm.combine(neg_norm)
            elif pos_len <= neg_len:
                return neg_norm.combine(pos_norm)
            else:
                raise ValueError('Issue with remove_samples_without_standard')
        else:            
            to_norm = self.remove_samples_without_standard()
            df = to_norm.data_frame.copy()
            samples = df[to_norm.sample_cols]
            stds_observed = to_norm.standards['Observed'][to_norm.mode].copy()
            #stds_observed.index =stds_observed.index.map(lambda x: x.name)
            #series of the samples
            stand_series = to_norm.data_frame.Name.apply(lambda x : x.get_standard(to_norm))
            #series of the concentrations from to_norm.standards['Input']
            stand_conc = stand_series.apply(lambda x: to_norm.standards['Input'][to_norm.mode].set_index('Name')['Conc'][x])
            for samp in samples.columns:
                std_vol = to_norm.name_table.set_index('Name')['Standard_Volume'][samp]
                sample_amt = to_norm.name_table.set_index('Name')['Sample_Amount'][samp]
                stdC_times_StdV = stand_conc.multiply(std_vol)
                constant = stdC_times_StdV.divide(sample_amt)
                std_intensities = stand_series.apply(lambda x : stds_observed.ix[x][samp])
                samples[samp] = samples[samp].divide(std_intensities)
                samples[samp] = samples[samp].multiply(constant)
            new_df = pd.concat([df[df.columns - to_norm.sample_cols], samples], axis=1)
            return MS_Data(new_df, to_norm.name_table, to_norm.mode, to_norm.standards)
        
    def check_normalization_groups(self):
        """
        Returns a df that has an example of each group and what lipid from the
        standard mix it was normalized to
        """
        group_standard_df = pd.DataFrame(columns = ['Group','Standard'])        
        for g in self.groups_present():
            gdf = self.restrict_to_group(g)
            item = gdf.data_frame.Name.iloc[0]
            standard = item.get_standard(self)
            group_standard_df = group_standard_df.append(pd.DataFrame({'Group':[item],'Standard':[standard]}))
        return group_standard_df
    
    def restrict_to_group(self, Group):
        """
        Returns
        ----------
        A new MS_data object with the same name and standard information, but
        with a data frame that contains only records for the specified lipid group.
        
        Parameters
        ----------
        Group : str
            The lipid group that the dataset will be restricted to.
            
        Examples
            >>>dataset.restrict_to_group('TAG')
            >>>dataset.restrict_to_group('PC')
        ----------
        
        
        See also
        ----------
        MS_data.groups_present
        MS_data.restrict_to_type
        MS_data.restrict_to
        """
        tdf = self.data_frame[self.data_frame.Group == Group]
        if Group == 'PL':
            tdf['not PA'] = tdf['Name'].apply(lambda x : not x.name.startswith('PA'))
            tdf = tdf[tdf['not PA']]
            del tdf['not PA']
        return MS_Data(tdf, self.name_table, self.mode, self.standards)
    
    def restrict_to_type(self, Type):
        tdf = self.data_frame[self.data_frame.Type == Type]
        if 'Group' in tdf.columns.tolist():
            tdf = tdf[tdf.Group != 'Isotope']
        if Type == 'PL':
            tdf['not PA'] = tdf['Name'].apply(lambda x : not x.name.startswith('PA'))
            tdf = tdf[tdf['not PA']]
            del tdf['not PA']
        return MS_Data(tdf, self.name_table, self.mode, self.standards)
        
    def remove_unknowns(self):
        return self.restrict_to('Group', 'Unknown', '!=')
        
    def restrict_to(self, lhs, rhs, rel='=='):
        """
        Not recommended for use unless you can figure out what the code does.
        """
        R = {'==' : lambda a, b : a == b, '!=' : lambda a, b : a != b}
        return MS_Data(self.data_frame[R[rel](self.data_frame[lhs], rhs)], self.name_table, mode=self.mode, ms_standards = self.standards)
        
    def restrict_to_mode(self, Mode):
        if Mode == '+':
            mdf = self.data_frame[self.data_frame['Mode'] == '+']
        elif Mode == '-':
            mdf = self.data_frame[self.data_frame['Mode'] == '-']
        else:
            raise ValueError('Mode should = "-" or "+".')
        return MS_Data(mdf, self.name_table, Mode, self.standards)
        
    def Fold_Changes(self, group_nums, append_group_names=False, sort=False):
        g1, g2 = self.groups_dict[group_nums[0]], self.groups_dict[group_nums[1]]
        gname1, gname2 = self.group_num_dict[group_nums[0]], self.group_num_dict[group_nums[1]]
        app = append_group_names*('_'+gname2+'_/_'+gname1)
        df = self.data_frame[g1+g2]
        stats_frame = self.data_frame[self.name_cols+self.mass_cols]
        mean_g1 = df[g1].mean(axis=1)
        mean_g2 = df[g2].mean(axis=1)
        FC = mean_g2 / mean_g1
        return FC
    
    def Hypothesis_Tests(self, group_nums, paired=False, append_group_names=False, sort=False):
        '''        
        STDEV = Sqrt(SUM(x-ave(x))/(n-1)) x is mean average and n is sample size

        '''         
        stats_frame = pd.DataFrame()
        g1, g2 = self.groups_dict[group_nums[0]], self.groups_dict[group_nums[1]]
        gname1, gname2 = self.group_num_dict[group_nums[0]], self.group_num_dict[group_nums[1]]
        app = append_group_names*('_'+gname2+'_/_'+gname1)
        df = self.data_frame[g1+g2]
        stats_frame = self.data_frame[self.name_cols+self.mass_cols]
        stats_frame['Mean_'+gname1] = df[g1].mean(axis=1)
        stats_frame['Mean_'+gname2] = df[g2].mean(axis=1)
        stats_frame['Stdev_'+gname1] = df[g1].std(axis=1)
        stats_frame['Stdev_'+gname2] = df[g2].std(axis=1)
        stats_frame['Fold_Change'+app] = stats_frame['Mean_'+gname2].div(stats_frame['Mean_'+gname1])
        if len(g1) >= 3 and len(g2) >= 3:
            if not paired:        
                stats_frame['RS_p_value'+app] = df.apply(lambda x : ranksums(x[g1], x[g2])[1], axis=1)
                stats_frame['t_test_p_value'+app] = ttest_ind(df[g1], df[g2], axis=1)[1]
            if paired:
                stats_frame['t_test_p_value'+app] = ttest_rel(df[g1], df[g2], axis=1)[1]
        stats_frame['Median_'+gname1] = df[g1].median(axis=1)
        stats_frame['Median_'+gname2] = df[g2].median(axis=1)
        stats_frame['Range_'+gname1] = df[g1].max(axis=1) - df[g1].min(axis=1)
        stats_frame['Range_'+gname2] = df[g2].max(axis=1) - df[g2].min(axis=1)
        cols = self.name_cols+self.mass_cols+['Fold_Change'+app, 'RS_p_value'+app,
                                              't_test_p_value'+app,
                                              'Mean_'+gname1, 'Stdev_'+gname1,
                                              'Median_'+gname1, 'Range_'+gname1,
                                              'Mean_'+gname2, 'Stdev_'+gname2,
                                              'Median_'+gname2, 'Range_'+gname2]
        if len(g1) < 3 or len(g2) < 3:
            cols.remove('t_test_p_value'+app)
            cols.remove('RS_p_value'+app)
        if paired:
            cols = cols.remove('RS_p_value'+app)
        if sort:
            stats_frame = stats_frame.sort_values(by = 'Fold_Change')
        self.hypothesis_tests = stats_frame[cols]
        self.stats_cols = [c for c in cols if c not in self.name_cols+self.mass_cols]
        return self.hypothesis_tests
        
    def Compare_All_Groups(self, normalize = False):
        '''        
        Used to make the all groups_sheet, 
        '''
        if normalize == False:
            df = self.data_frame.copy()
        elif normalize == True:
            df1 = self.normalize()
            df = df1.data_frame
        stats_frame = pd.DataFrame()
        stats_frame = df[self.name_cols+self.mass_cols]
        for g in self.groups_dict:
            group_name = self.group_num_dict[g]
            stats_frame['Mean_'+group_name] = df[self.groups_dict[g]].mean(axis = 1)
            stats_frame['Stdev_'+group_name] = df[self.groups_dict[g]].std(axis = 1)
        return stats_frame
            
    def Describe_Unknowns(self):
        ###FIX THIS
        df = self.data_frame[self.data_frame.Name == 'Unknown']
        new_name = pd.Series(dict([(i, 'Unknown_'+df.mode[i]+'_'+str(df.Precursor[i])[:5]+'_'+str(df.Fragment[i])[:5]) for i in df.index]))
        df['Name'] = new_name
        final_df = pd.concat([self.data_frame[self.data_frame.Name != 'Unknown'], df])
        return MS_Data(final_df, self.name_table, self.mode, self.standards)
        
    def summarize_group_feature(self, group, groups_to_test, paired=False, MS1_group = True, MS2_features = True, chain_features = False):
        restricted = self.restrict_to_group(group)
        features = restricted.data_frame.Name.apply(lambda x : 'LOLWTF'.join(x.features.keys())) #join features in string so they can be compared (can't as a list)
        to_analyze = features[features.first_valid_index()]
        try:
            assert len(features.drop_duplicates()) == 1 ##throw an error if the features are not all the same
            first = features[features.first_valid_index()]
            assert (features == first).all()
            common_to_all = first.split('LOLWTF')
        except AssertionError:
            ##get features common to all
            common_to_all = set(to_analyze)
            for i in features.index:
                next_set = set(features[i].split('LOLWTF'))
                common_to_all = common_to_all.intersection(next_set)
                try:
                    assert len(common_to_all) > 0
                except AssertionError:
                    print 'Error: group', group, 'has no features common to all species.'                    
                    return None
        ###perform the groupby and tests
        excel_dict = OrderedDict()
        df = restricted.data_frame.copy()
        df['Count'] = 1
        features = list(common_to_all)
        for f in features:
            df[f]= df.Name.apply(lambda x : x.features[f])
        to_group = df[self.name_cols+features+self.mass_cols+self.sample_cols+['Count']] #organize columns for data file
        stats = restricted.Hypothesis_Tests(groups_to_test)
        stats_cols = restricted.stats_cols
        for f in features:
            stats[f]= stats.Name.apply(lambda x : x.features[f])
        stats = stats[self.name_cols+self.mass_cols+features+stats_cols]
        to_data = df.copy()
        for f in features:
            del to_data[f]
        del to_data['Count']
        excel_dict['Data'] = to_data
        excel_dict['Stats'] = stats.copy()
        for f in features:
            fdata = to_group.groupby(f).sum()
            g1, g2 = self.groups_dict[groups_to_test[0]], self.groups_dict[groups_to_test[1]]
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            gname1, gname2 = self.group_num_dict[groups_to_test[0]], self.group_num_dict[groups_to_test[1]]
            df = fdata[g1+g2]
            fdata = to_group.groupby(f).sum()
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            stats_frame = make_stats_frame(df, gname1, gname2, g1, g2, paired = paired)
            excel_dict[f+'_Statistics'] = stats_frame
        #enter sarahs sheets here
        if MS1_group:
            MS1_bin = restricted.bin_and_sum('Precursor', normalize=False)
            MS1_bin_stats = make_stats_frame(MS1_bin, gname1, gname2, g1 ,g2, paired = paired)
            excel_dict['MS1_bin'] = MS1_bin
            excel_dict['MS1_bin_Statistics'] = MS1_bin_stats
        if MS2_features:
            if group not in ['DAG','TAG']:
                pass
            else:
                #pdb.set_trace() #uncomment line for debugging
                ms2_features = restricted.data_frame.Name.apply(lambda x: x.MS2_features(as_str = True))
                to_group = restricted.data_frame.copy()
                to_group['Count'] = 1
                to_group['MS2_features'] = ms2_features
                MS2_grouped = to_group.groupby('MS2_features').sum()
                MS2_grouped.reset_index(range(len(MS2_grouped.index)), inplace = True)
                cols = ['MS2_features','Count']+self.sample_cols
                MS2_grouped = MS2_grouped[cols]
                MS2_grouped_stats = make_stats_frame(MS2_grouped, gname1, gname2, g1 ,g2, paired = paired)
                excel_dict['MS2_bin'] = MS2_grouped
                excel_dict['MS2_bin_stats'] = MS2_grouped_stats
        if chain_features:
            pass
        return excel_dict
        
    def summarize_type_feature(self, typ, groups_to_test, paired=False):
        restricted = self.restrict_to_type(typ)
        features = restricted.data_frame.Name.apply(lambda x : 'LOLWTF'.join(x.features.keys())) #join features in string so they can be compared (can't as a list)
        to_analyze = features[features.first_valid_index()]
        try:
            first = features[features.first_valid_index()]
            assert (features == first).all()
            common_to_all = first.split('LOLWTF')
        except:
            ##get features common to all
            common_to_all = set(to_analyze.split('LOLWTF'))
            for i in features.index:
                next_set = set(features[i].split('LOLWTF'))
                common_to_all = common_to_all.intersection(next_set)
                try:
                    assert len(common_to_all) > 0
                    #features = list(common_to_all)
                except AssertionError:
                    print 'Error: type', typ, 'has no features common to all species.'
                    print common_to_all
                    return None
        ###perform the groupby and tests
        excel_dict = OrderedDict()
        df = restricted.data_frame.copy()
        df['Count'] = 1
        #features = df.Name[df.Name.first_valid_index()].features.keys()
        features = list(common_to_all)
        for f in features:
            df[f]= df.Name.apply(lambda x : x.features[f])
        to_group = df[self.name_cols+features+self.mass_cols+self.sample_cols+['Count']] #organize columns for data file
        stats = restricted.Hypothesis_Tests(groups_to_test)
        stats_cols = restricted.stats_cols
        for f in features:
            stats[f]= stats.Name.apply(lambda x : x.features[f])
        stats = stats[self.name_cols+self.mass_cols+features+stats_cols]
        to_data = df.copy()
        for f in features:
            del to_data[f]
        del to_data['Count']
        excel_dict['Data'] = to_data
        excel_dict['Stats'] = stats.copy()
        for f in features:
            fdata = to_group.groupby(f).sum()
            g1, g2 = self.groups_dict[groups_to_test[0]], self.groups_dict[groups_to_test[1]]
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            gname1, gname2 = self.group_num_dict[groups_to_test[0]], self.group_num_dict[groups_to_test[1]]
            df = fdata[g1+g2]
            fdata = to_group.groupby(f).sum()
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            stats_frame = make_stats_frame(df, gname1, gname2, g1, g2, paired = paired)
            excel_dict[f+'_Statistics'] = stats_frame 
        MS1_bin = restricted.bin_and_sum('Precursor',normalize=False)
        MS1_bin_stats = make_stats_frame(MS1_bin, gname1, gname2, g1, g2, paired = paired )
        excel_dict['MS1_bin'] = MS1_bin
        excel_dict['MS1_bin_Statistics'] = MS1_bin_stats
        return excel_dict
    
    def to_excel(self, writer, **kwargs):#, float_format='%.4f'):
        if kwargs.get('index') is None:
            kwargs['index'] = False
        df = self.data_frame.copy()
        if 'Mass_Pair' in df.columns:
            df.Mass_Pair = df.Mass_Pair.apply(str)
        if 'Name' in df.columns:
            df.Name = df.Name.apply(str)
        df.to_excel(writer, **kwargs)
    
    def drop_group_of_samples(self, num=None, name=None):
        if name is not None:
            new_names = self.name_table[self.name_table.Group_Name != name]
            new_data = self.data_frame[self.name_cols+self.mass_cols+new_names.Name.tolist()]
            return MS_Data(new_data, new_names, mode=self.mode, ms_standards=self.standards)
        elif num is not None:
            new_names = self.name_table[self.name_table.Group != num]
            new_data = self.data_frame[self.name_cols+self.mass_cols+new_names.Name.tolist()]
            return MS_Data(new_data, new_names, mode=self.mode, ms_standards=self.standards)
            
    def drop_sample_groups(self, nums=None, names=None):
        if nums is not None:
            nums = list(nums)
        if names is not None:
            names = list(names)
        new_mdata = self.copy()
        while nums:
            to_drop = nums.pop()
            new_mdata = new_mdata.drop_group_of_samples(num=to_drop)
        while names:
            to_drop = names.pop()
            new_mdata = new_mdata.drop_group_of_samples(name=to_drop)
        return new_mdata

    def to_skl_classifier(self):
        """
        Y, X = self.to_skl_classifier()
        """
        data = np.array(self.data_frame[self.sample_cols].T)
        labels = np.array(self.name_table.Group)
        return labels, data
    
    def copy(self):
        new_data = self.data_frame.copy()
        new_names = self.name_table.copy()
        return MS_Data(new_data, new_names, mode=self.mode, ms_standards=self.standards)
    
    def Volcano(self, saveas=None):
        
        if not hasattr(self, 'hypothesis_tests'):
            print('Fold change and p-value not yet calculated.  Cannot make volcano plot.')
            return None
        df = self.hypothesis_tests[self.hypothesis_tests['Type'] != 'Unknown']
        fig, ax = plt.subplots()
        plot_groups = df['Group'].drop_duplicates().tolist()
        colormap = plt.cm.Set1
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, np.exp(1)/np.pi, len(plot_groups))])
        for group in plot_groups:
            restricted_df = df[df['Group'] == group]
            pvals = -np.log10(restricted_df['t_test_p_value'])
            FCs = np.log2(restricted_df['Fold_Change'])
            if group != 'Unknown':
                ax.plot(FCs, pvals, sample(symbols, 1)[0], ms=7.5, label=group)
            else:
                pass#ax.plot(pvals, FCs, '*', ms=5, label=group)
        lgd = ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        restricted_df = df[df['Group'] == group]
        ax.set_xlabel('log2(Fold Change)')
        ax.set_ylabel('-log10(p-value)')
        fig.tight_layout()
        if saveas is not None:
            fig.savefig(saveas, bbox_extra_artists=lgd)
        return None
        
    def Manhattan(self, saveas=None):
        
        if not hasattr(self, 'hypothesis_tests'):
            raise AttributeError('Statistics not done.')
        grouped = self.hypothesis_tests.sort_values(by = 'Group').reset_index(drop=True)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for g in grouped.Group.drop_duplicates():
            pvals = grouped[grouped.Group == g]['t_test_p_value']
            ax.plot(pvals.index, -np.log10(pvals), label=g)
        ax.set_ylim(bottom=0)
        if saveas is not None:
            fig.savefig(saveas)
        return fig
        
    def fit_skl_model(self, skl_model, **kwargs):
        Y, X = self.to_skl_classifier()
        skl_model.fit(X, Y)
        return skl_model
        
    def score_skl_model(self, skl, **kwargs):
        Y, X = self.to_skl_classifier()
        skl_model.score(X, Y)
        return skl_model
            
    def raw_counts(self):
        return self.data_frame[self.sample_cols]
    
    def isotope_distributions(self):
        try:
            dist = self.data_frame.Name.apply(lambda x: x.mass_pair_dist(from_zero=True))
            return dist
        except AttributeError:
            raise AttributeError('Data needs to be named before isotope_distibutions is called')
         
    def test(self):
        return self.data_frame
        
    def isotope_correction(self, treat = '0', missing_value = 200, baseline = 200, max_m = 4, concidered_iso_fract = .4, name_isotopes = True):
        """
        Takes the samples and correctes them isotopically.
        this will be done on self.data_frame
        data_frame needs to have been named
        missing_value:
            is the isotope distibution times the signal is larger than this 
            number it will be added to the missing signal list
        baseline:
            the smallest value that it will correct for.
            decreasing this increases the runtime but increases the accuracy
            increasing this decreases the runtime but decreases the accuracy 
                
        this function makes a huge dataframe with all of the info from the isotopes
        including:
            each isotope distribution for each mass pair
            each of the fragtion for each distibution for each mass pair
            isotope corrected
            isotope adjusted (whcih is the same as isotope corrected but only pos numbers)
        
        Use isotope_filter to reduce the self.data_frame to only the sample info
        
        mcs aka Matts Crazy Sheet:
            returns a dataframe that shows what fraction of a mass pairs Mxy 
            come from its isotopes
            =(Raw-correction_value)/Raw
        
        note1: this does not remove any mass pairs, this only corrects the data.
            no fractions or graphs or analysis, just correction.
            
        note2: This will not alter the standards
        """
        #write some checks to make sure it is named data and only one mode!        
        if self.mode == None:
            raise ValueError('Data must be seperated by mode before calling')
            #needs to be tested
            #pos = self.restrict_to('Mode', '+', '==')
            #neg = self.restrict_to('Mode', '-', '==')
            #pos.mode = '+'
            #neg.mode = '-'
            #pos_cor = pos.isotope_correction(treat, missing_value, baseline, max_m, concidered_iso_fract, name_isotopes)
            #neg_cor = neg.isotope_correction(treat, missing_value, baseline, max_m, concidered_iso_fract, name_isotopes)
            #print type(pos_cor)
            #print type(neg_cor)
            #mcs = pd.concat([pos_cor[2],neg_cor[2]])
            #missing = pd.concat([pos_cor[1],neg_cor[1]])
            #new_df = pd.concat([pos_cor[0].data_frame,neg_cor[0].data_frame])
            #new_ms_class = pos_cor[0].combine(neg_cor[0])
            #new_ms_class.data_frame = new_df
            #return new_ms_class, missing, mcs
            
        print '    Calculating isotope distributions for %s mode'%(self.mode)        
        try:
            ratios = self.data_frame.Name.apply(lambda x: x.mass_pair_dist(from_zero=True))
        except AttributeError:
            raise AttributeError('Data needs to be named before isotope_distibutions is called')        
        
        iso_df = self.data_frame.copy()        
        iso_df.sort_values(by = 'Mass_Pair', inplace=True)
        
        ratios = iso_df.Name.apply(lambda x: x.mass_pair_dist(from_zero=True))
        molecule_info = iso_df[self.name_cols+self.mass_cols]
        raw_data = iso_df[self.sample_cols]
        #adj stands for adjusted, this will be the dataframe that ends up with
        #the isotopically corrcted data        
        iso_df = pd.concat([molecule_info, ratios, raw_data], axis=1).replace(np.nan, 0)
        iso_df.sort_values(by = 'Mass_Pair', inplace=True)
        
        '''
        first makes the dataframe with all of the columns needed for correction
        the in the for i in iso_df.index loop
            for s in samples
                first it sums the Int Mxy for each sample and sumtracts that from 
                the raw to give the adjusted value
                then stores that value along with the fraction value for each sample
            if iso_df
                uses fractions to see if it should be classified as an isotope
                if so changes the name, group and type to reflect that.
            for M, Ms in zip(m_num,m_str):
                does Mxy * cor, adds it to a dictionary if that value is larger than baseline
                then if that dictionary is not empty,
                finds each of the isotopes precursor and fragments an puts in the Mxy*cor value
            
        '''
        m_num = []
        m_str = []
        for m in range(1, max_m+1):
            for n in range(0, m+1):
                m_num.append((m,n))
                m_str.append('Int M'+str(m)+str(n))

        print '    Constructing the dataframes'
        #mcs stands for matts_crazy_sheet
        mcs = iso_df[['Name','Precursor','Fragment','Mode']]
        missing_signals = OrderedDict([])
        samples = self.sample_cols
        for s in samples:
            iso_df[s +' Adjusted Intensity'] = iso_df[s]
            mcs[s+ 'Raw'] = iso_df[s]
            missing_signals[s] = pd.DataFrame(columns=[s+' Precursor', s+' Fragment',s+' From',s+' Counts'])
            ########################## adding mcs need to add Mxy for each s
            for x in m_str:
                iso_df[s+' '+x] = 0.0
            for x in m_num:
                mcs[s+' M'+str(x[0])+str(x[1])] = 0.0
            iso_df[s +' Isotope Correction'] = 0.0
            iso_df[s +' Cor/Raw'] = 0.0
        print'    Correcting, this can take a few minutes'
        #loops though each index for isotope correction               
        for i in iso_df.index:
            pre = iso_df['Precursor'][i]
            frag = iso_df['Fragment'][i]
            mass_pair_mode = iso_df['Mode'][i]
            
            #used for the naming function       
            fractions = []            
            sum_Mxy = {}            
            for m in m_str:
                sum_Mxy[m] = 0
            #calculated the adj and correctd for each sample   
            for s in samples:
                sumM = 0.0
                for Ms in m_str:
                    sumM += iso_df[s +' '+ Ms][i]
                    sum_Mxy[Ms] += (iso_df[s +' '+ Ms][i])
                corrected_subtraction = iso_df[s][i] - sumM
                iso_df[s +' Isotope Correction'][i] = corrected_subtraction
                if iso_df[s +' Isotope Correction'][i] <= 0:
                    iso_df[s +' Adjusted Intensity'][i] = 0
                else:
                    iso_df[s +' Adjusted Intensity'][i] = corrected_subtraction
                iso_df[s +' Cor/Raw'][i] = iso_df[s +' Isotope Correction'][i]/iso_df[s][i]
                fractions.append(iso_df[s +' Cor/Raw'][i])
            
            if name_isotopes == True:
                #used for naming 
                highestMxy = keywithmaxval(sum_Mxy)
                fract_median = round(np.median(fractions),2)
                if fract_median < concidered_iso_fract:
                    iso_df['Group'][i] = 'Isotope'
                    x = int(highestMxy[-2:-1])
                    y = int(highestMxy[-1:])
                    iso_name =  'Isotope from ' + str(pre-x) +'_'+ str(frag-y) + ' '+ highestMxy[-3:] + ' ('+str(fract_median) +')'
                    isotope = Lipid.from_group('Isotope',iso_name, mode = mass_pair_mode, mass_pair = MassPair(pre,frag))
                    iso_df['Name'][i] = isotope
                    mcs['Name'][i] = isotope

            #calculates the dist* Isotope Correction and puts it in the correct
            #place for each Mxy and each sample
            for M, Ms in zip(m_num,m_str):
                samples_for_correction={}      
                for s in samples:
                    if treat in {'neg','Neg','negative','Nagative','-'}:
                        adjMxy = iso_df[s+ ' Isotope Correction'][i]*iso_df[MassPair.from_list(M)][i]
                    elif treat in {'zero','Zero','0'}:
                        adjMxy = iso_df[s+ ' Adjusted Intensity'][i]*iso_df[MassPair.from_list(M)][i]
                    else:
                        raise ValueError('treat needs to be one of the following "neg", "Neg", "Negative", "negative" or "-", "Zero", "zero", "0"')
                    if abs(adjMxy) >= baseline:
                        samples_for_correction[s] = adjMxy
                if samples_for_correction.keys():
                    isotope_ind = find(iso_df, pre+M[0], frag+M[1])
                    for v in samples_for_correction:
                        if isotope_ind is not None:
                            iso_df[v + ' '+Ms][isotope_ind] = samples_for_correction[v]
                            mcs_num = (iso_df[v][isotope_ind]-samples_for_correction[v])/iso_df[v][isotope_ind]
                            mcs[v + ' M'+str(M[0])+str(M[1])][i] = mcs_num
                        else: #add to missing signals!
                            missing_pre = pre+M[0]
                            missing_frag = frag+M[1]
                            missing_intensity = samples_for_correction[v]
                            missing_from = str(pre)+'_'+str(frag)+' M'+str(M[0])+str(M[1])
                            to_add = pd.DataFrame([[missing_pre,missing_frag,missing_from,missing_intensity]],columns=[v+' Precursor', v+' Fragment',v+' From',v+' Counts'])
                            missing_signals[v] = missing_signals[v].append(to_add, ignore_index=True)
        print ('    Correction complete')
        missing_signals_dfs = []
        for df in missing_signals.values():
            missing_signals_dfs.append(df)
        missing_signals_concat = pd.concat(missing_signals_dfs,axis = 1) 
        new_MS = MS_Data(iso_df, self.name_table, self.mode, self.standards)
        new_MS.data_frame = iso_df
        #get new corrected standards and change new_MS
        #iso_filtered_MS = new_MS.isotope_filter()
        #old_standards = self.standards['Observed'][self.mode].copy()
        #new_standards = iso_filtered_MS.data_frame[iso_filtered_MS.data_frame.Group.str.contains('Standard')]
        #new_standards.set_index('Name', drop = True, inplace = True)
        #new_standards = new_standards[iso_filtered_MS.mass_cols+iso_filtered_MS.sample_cols]
        #new_standards.index = new_standards.index = new_standards.index.map(lambda x: x)
        #new_MS.standards['Observed'][self.mode] = new_standards
        #print new_standards
        #for i in new_standards.index:
        #    for s in self.sample_cols:
        #        old = old_standards[s][i]
        #        new = new_standards[s][i]
        #        if 0.95*old >= new <= 1.05*old:
        #            std_name = i
        #            print ('Warning standard %s has change by more than 5%% when isotope correcting!' %(std_name))
        #new_MS.standards['Observed'][self.mode] = new_standards
        return new_MS, missing_signals_concat, mcs
        
    def isotope_raw(self):
        '''
        returns a MS_class object that has the raw data, and correlating sample info
        benefit of this is that isotopes are now named
        '''
        if self.mode == None:
            raise ValueError('Sample must be resticted to one mode first')
        new_df = self.data_frame[self.name_cols+self.mass_cols+self.sample_cols]
        return MS_Data(new_df, self.name_table, self.mode, self.standards)

    def isotope_filter(self, report = 'zero'):       
        #write in tests to make sure it is isotope corrected            
        '''
        iso_df the corrected columns from the complete isotope
        if iso_df = 'zero' negative numbers will be reported as 0
        if iso_df != 'zero' negative numbers will be reported
        '''
        if self.mode == None:
            raise ValueError('Sample must be resticted to one mode first')
        corrected_cols = []
        if report in {'zero', 'Zero', '0'}:
            for i in self.sample_cols:
                corrected_cols.append(i+' Adjusted Intensity')            
        elif report in {'neg', 'Neg', 'negative', 'Negative','-'}:
            for i in self.sample_cols:
                corrected_cols.append(i+' Isotope Correction')
        else:
            raise ValueError('treat needs to be one of the following "neg", "Neg", "Negative", "negative" or "-", "Zero", "zero", "0"')
        new_df = self.data_frame[self.name_cols+self.mass_cols+corrected_cols]
        new_df.columns = [self.name_cols+self.mass_cols+self.sample_cols]
        new_stds = copy.deepcopy(self.standards)
        new_standards_df = new_df[new_df.Group.str.contains('Standard')]
        new_standards_df.set_index('Name', drop = True, inplace = True)
        new_standards_df = new_standards_df[self.mass_cols+self.sample_cols]
        new_standards_df.index = new_standards_df.index.map(lambda x: x.name)
        new_stds['Observed'][self.mode] = new_standards_df
        return MS_Data(new_df, self.name_table, self.mode, new_stds)                
        #change the corrected
    
    def remove_isotopes(self):
        new_df = self.data_frame[self.data_frame['Group'] != 'Isotope']
        return MS_Data(new_df, self.name_table, self.mode, self.standards)
        
    def bin_and_sum(self, component, accuracy = .5, normalize = True):
        '''
        bins together and sums the intensity of the specified component
        eg component = 'Fragment'
        this will bin the fragments with the + or - accuracy and then does stats
        change the poap increment of the poap file changes!
        '''
        msc_modes = {'+':self.restrict_to_mode('+').data_frame,
                     '-':self.restrict_to_mode('-').data_frame}
        msc_normalized = {}
        msc_data = {}
        poap = []
        start_up = 150
        start_down = 150
        while start_up <= 1202 or start_down >=0:
            if start_up <= 1202:
                poap.append(start_up)
                start_up += 1.007825
            if start_down >= 0:
                poap.append(start_down)
                start_down -= 1.007825
        poap = [ round(elem, 3) for elem in poap ]
        neg_poap = [-x for x in poap]
        poap.extend(neg_poap)
        poap.append(0)
        poap.sort()
        for k in msc_modes.keys():
            msc = msc_modes[k]
            msc.sort_values(by = component,inplace = True)
            msc.reset_index(drop= True,inplace = True)
            samples = self.sample_cols
            used_indexes = []
            comp_col = [component, '# examples', 'Mode']+samples
            component_df = pd.DataFrame(columns = comp_col)
            for i in msc.index:
                if i not in used_indexes:
                    comp_val = msc[component][i]
                    center_point = take_closest(poap, comp_val)
                    spread = abs(comp_val-center_point) 
                    if spread > accuracy:
                        pass#print 'WARNING!!!!',comp_val,center_point    
                    found = locate(msc, component, center_point, accuracy = accuracy)
                    if found is not None:
                        used_indexes.extend(found)
                        if len(found) > 1:
                            comp_row = OrderedDict([(component,[center_point])])
                            comp_row['# examples'] = [len(found)]
                            comp_row['Mode'] = k
                            for s in samples:
                                total = 0
                                for f in found:
                                    total += msc[s][f]
                                comp_row[s] = [total]
                            component_df = component_df.append(pd.DataFrame(comp_row),ignore_index = True)
                    else:
                        used_indexes.extend([i])    
                else:
                    used_indexes.extend([i])
            #component_df = component_df[comp_col]
            normalized = lf.normalizer(component_df, self.sample_cols)
            msc_data[k] = component_df            
            msc_normalized[k] = normalized
        msc_data_df = pd.concat([msc_data['+'],msc_data['-']])
        msc_normalized_df = pd.concat([msc_normalized['+'],msc_normalized['-']]) 
        msc_normalized_df = msc_normalized_df[msc_data_df.columns]    
        if normalize == True:
            return msc_normalized_df
        else:
            return msc_data_df

class PCA:
    ###TODO: make passing of scale_func actually work
    
    
    def __init__(self, mdata, group_by_group=False, scale_func=AutoScale, num_components=10, explain_variance=None):
        
        if 'Name' not in mdata.data_frame.columns:
            self.mdata = mdata.Name_Molecules().Describe_Unknowns()
        else:
            self.mdata = mdata
        
        self.scale_func = scale_func
        self.scaled = self.scaled()
        self.analysis = sklPCA(n_components=num_components)
        self.transformed_samples = self.analysis.fit_transform(self.scaled.T)
        self.transformed_samples = pd.DataFrame(self.transformed_samples.T)
        self.transformed_samples.columns = mdata.sample_cols
        self.transformed_samples.index = ['Comp '+str(i+1) for i in range(num_components)]
        self.explained_variance = 100.0*self.analysis.explained_variance_ratio_
        self.molecular_loadings = pd.DataFrame(self.analysis.components_).T
        self.molecular_loadings.index = self.scaled.index
        self.molecular_loadings.columns = ['Comp '+str(i+1) for i in range(num_components)]
        
    def scaled(self):
        df = self.mdata.data_frame.copy()
        df.index = df.Name
        df = df[self.mdata.sample_cols]
        df = self.scale_func(df)
        return df
    
    def plots(self):
        pass        
    
    def plot_samples(self, components=(1,2), saveas=None):
        mdata = self.mdata
        df = self.transformed_samples
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal')
        c1, c2 = components
        for group_num in mdata.group_num_dict.keys():
            ax.plot(df[mdata.groups_dict[group_num]].ix[c1-1], df[mdata.groups_dict[group_num]].ix[c2-1], '^', label=mdata.group_num_dict[group_num])
        ax.set_xlabel('PC{0} Score (explained variance: {1})'.format(c1, np.round(self.explained_variance[c1-1], decimals=1)))
        ax.set_ylabel('PC{0} Score (explained variance: {1})'.format(c2, np.round(self.explained_variance[c2-1], decimals=1)))
        
        ax.grid()
        
        lgd = ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        try:
            ax.set_title(scale_tag[self.scale_func].capitalize()+' PCA')
        except KeyError:
            ax.set_title(self.scale_func.func_name+' PCA')
        if saveas is not None:
            fig.savefig(saveas, bbox_extra_artists=(lgd,), bbox_inches='tight')
        return None
    
    def plot_molecular_loads(self, color_by='group', saveas=None):
        
        df = self.molecular_loadings.reset_index()
        df['Name'] = df['Name'].apply(lambda x : Lipid(x, '+'))
        color_by = color_by.lower().replace(' ', '_')

        
        attr_dict = {'group' : lambda x : x.group,
             'carbons' : lambda x : x.num_carbons(),
             'double_bonds' : lambda x : x.num_double_bonds()}
        df[color_by] = df['Name'].apply(attr_dict[color_by])
        
        from random import sample
        symbols = ['v', '>', '^', '<', 'o', 's', 'p']
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal')
        plot_groups = df[color_by].drop_duplicates()
        colormap = plt.cm.Set1
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, np.exp(1)/np.pi, len(plot_groups))])
        for group in plot_groups:
            if group != 'Unknown':
                restricted_df = df[df[color_by] == group]
                ax.plot(restricted_df['Comp 1'], restricted_df['Comp 2'], sample(symbols, 1)[0], ms=7.5, label=group)
            else:
                restricted_df = df[df[color_by] == group]
                ax.plot(restricted_df['Comp 1'], restricted_df['Comp 2'], '*', ms=5, label=group)
        lgd = ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        ax.grid()
        ax.set_xlabel('PC1 Loading')
        ax.set_ylabel('PC2 Loading')
        try:
            ax.set_title(scale_tag[self.scale_func].capitalize()+' PC Loadings')
        except KeyError:
            ax.set_title(self.scale_func.func_name+' PC Loadings')
        fig.tight_layout()
        if saveas is not None:
            fig.savefig(saveas, bbox_extra_artists=(lgd,), bbox_inches='tight')
        return None
    
    def samples_loads(self, color_by='group'):
        fig, ax = plt.subplots(2, 1, subplot_kw={'aspect':'equal'}, figsize=(45, 15))
        mdata = self.mdata
        df = self.transformed_samples
        #ax1 = fig.add_subplot(111, aspect='equal')
        for group_num in mdata.group_num_dict.keys():
            ax[0].plot(df[mdata.groups_dict[group_num]].ix[0], df[mdata.groups_dict[group_num]].ix[1], 'o', ms=12.5, label=mdata.group_num_dict[group_num])
        ax[0].set_xlabel('PC1 Score (explained variance: %s%%)' % np.round(self.explained_variance[0], decimals=1))
        ax[0].set_ylabel('PC2 Score (explained variance: %s%%)' % np.round(self.explained_variance[1], decimals=1))
        ax[0].set_aspect('equal')
        ax[0].grid()
        try:
            ax[0].set_title(scale_tag[self.scale_func].capitalize()+' PCA')
        except KeyError:
            ax[0].set_title(self.scale_func.func_name+' PCA')
        
        
        color_by = color_by.lower().replace(' ', '_')
        attr_dict = {'group' : lambda x : x.group,
             'carbons' : lambda x : x.num_carbons(),
             'double_bonds' : lambda x : x.num_double_bonds()}
        
        df = self.molecular_loadings.reset_index()
        df['Name'] = df['Name'].apply(lambda x : Lipid(x, '+'))
        df[color_by] = df['Name'].apply(attr_dict[color_by])
        
        symbols = ['p', '>', 'o', '<', 'p','^', 's']
        #ax = fig.add_subplot(222, aspect='equal')
        plot_groups = df[color_by].drop_duplicates()
        colormap = plt.cm.Set1
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, np.exp(1)/np.pi, len(plot_groups))])
        i = 0 
        for group in plot_groups:
            if group != 'Unknown':
                restricted_df = df[df[color_by] == group]
                ax[1].plot(restricted_df['Comp 1'], restricted_df['Comp 2'], symbols[i % len(symbols)], ms=10, label=group)
                i += 1
            else:
                restricted_df = df[df[color_by] == group]
                ax[1].plot(restricted_df['Comp 1'], restricted_df['Comp 2'], 'k.', alpha=.25, ms=3, label=group)
        lgd = ax[0].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.), ax[1].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        ax[1].grid()
        ax[1].set_aspect('equal')
        ax[1].set_xlabel('Component 1 Loading')
        ax[1].set_ylabel('Component 2 Loading')
        try:
            ax[1].set_title(scale_tag[self.scale_func].capitalize()+' PC Loadings')
        except KeyError:
            ax[1].set_title(self.scale_func.func_name+' PC Loadings')
        fig.tight_layout()
        #fig.savefig('testPCA.png', bbox_extra_artists=(lgd,), bbox_inches='tight')
        return fig, lgd
        
    def dimension_reduced_data(self):
        return pd.DataFrame(self.analysis.inverse_transform(self.transformed_samples.T)).T
        

class KineticAnalysis(MS_Data):
    
    def __init__(self, data, time_keys=None, mode=None):
        self.mass_cols = self.mass_cols = ['Precursor', 'Fragment', 'neutral_loss', 'Mode']
        self.name_cols = ['Name', 'Group', 'Type']
        if isinstance(data, pd.DataFrame):
            self.data_frame = data
        elif isinstance(data, MS_Data):
            self.data_frame = data.data_frame
            self.sample_cols = data.sample_cols
        elif isinstance(data, str):
            mdata = MS_Process(data, None, mode).to_MS_data()#
            self.sample_cols = mdata.sample_cols
            self.data_frame = mdata.Low_Average_Filter(30, self.sample_cols).NL_Filter(-1).Remove_Child_Peaks([(1,10)]).Name_Molecules().data_frame

                
        else:
            try:
                data = pd.DataFrame(data)
            except:
                raise TypeError('Argument data is of type %s and cannot be converted to DataFrame.' % type(data))
            for item in self.mass_cols:
                try:
                    assert(item in data.columns)
                except:
                    raise KeyError('%s not in columns of data.' % item)
            self.sample_cols = self.data_frame.columns.diff(self.mass_cols+self.name_cols)
        
        if time_keys is None:
            try:
                self.time = pd.Series(self.data_frame[self.sample_cols].ix['time'])
            except KeyError:
                raise KeyError('data does not contain index value \'time\'')
        else:
            if isinstance(time_keys, dict):
                try:
                    assert time_keys.keys() == self.sample_cols
                    self.time = pd.Series(time_keys)
                except:
                    raise KeyError('time_keys does not have same keys as sample columns')
            else:
                try:
                    assert len(time_keys) == len(self.sample_cols)
                except:
                    print(time_keys)
                    print(self.sample_cols)
                    raise ValueError('time_keys must have same length as sample columns.  Sample columns has length '+str(len(self.sample_cols))+'.  time_keys has length '+str(len(time_keys)))
                if isinstance(time_keys, pd.Series):
                    if time_keys.index == self.sample_cols:
                        self.time = time_keys
                    else:
                        raise KeyError('time_keys does not have same keys as sample columns')
                else:
                    self.time = pd.Series(dict(zip(self.sample_cols, time_keys)))
                    print time_keys
                    print self.time
        if 'Name' not in self.data_frame.columns:
            self.data_frame = self.Name_Molecules().data_frame
            
        self.data_frame.index = self.data_frame.Name
        self.time = self.time[['s'+str(i+1) for i in range(len(self.sample_cols))]]
        
    def to_time_series(self, lipid, scale=True):
        if scale:
            t0_keys = self.time[self.time == 0].index
            tf_keys = self.time[self.time == max(self.time)].index
        if lipid not in self.data_frame.index:
            raise KeyError(str(lipid)+' not in data set.')
        series = self.data_frame[self.sample_cols].ix[str(lipid)]
        if scale:
            s0, sf = np.mean(series[t0_keys]), np.mean(series[tf_keys])
            series = series.apply(lambda x : float(x - min(s0, sf))/float(max(s0, sf) - min(s0, sf)))
        return self.time, series
        
    def plot(self, lipid, scale=True, fit_func=None, saveas=None, time_unit='minutes', lipid_unit='Saturation'):
        fig, ax = plt.subplots()
        t, series = self.to_time_series(lipid, scale=scale)
        
        if fit_func is not None:
            params, cov = self.fit_to_func(fit_func, lipid, scale=scale)
            sim_t = np.arange(min(self.time), max(self.time), 0.01)
            sim_func = fit_func(sim_t, *params)
            resid = []
            for i in t.index:
                resid += [fit_func(t[i], *params) - series[i]]
                
            upper = np.percentile(resid, 75.0)
            lower = np.percentile(resid, 25.0)
            try:
                assert len(sim_t) == len(sim_func)
            except AssertionError:
                raise ValueError('len sim_t: '+str(len(sim_t))+', len sim_fun: '+str(len(sim_func)))
            ax.plot(sim_t, sim_func, 'k', lw=3, label='best fit')
            #ax.plot(t, series, 'o', label='observed')
            ax.plot(sim_t, sim_func + upper, 'r--', label='Interquartile range')
            ax.plot(sim_t, sim_func + lower, 'r--')
            ax.fill_between(sim_t, sim_func + lower, sim_func + upper, facecolor='red', alpha=0.3)
            ax.set_xlim(left=0, right=max(t))
            ax.legend(loc='best')
            
        else:
            ax.plot(t, series, 'o')
        ax.set_xlabel('time (%s)' % time_unit)
        ax.set_ylabel(lipid_unit)
        ax.set_title(lipid)
        if saveas is not None:
            fig.savefig(saveas)
        return None
        
    def fit_to_func(self, func, lipid, scale=True):
        t, series = self.to_time_series(lipid, scale=scale)
        popt, pcov = curve_fit(func, t, series)
        return popt, pcov

        
    def CheckTimes(self):
        for i in self.time.index:
            print i, self.time.index[i]
            
class OneSample(object):
    
    pass

def find(df, a, b, accuracy = .3, for_isotope = True):
    '''
    used in iso_correction to find the desired parent and daughter isotope by precursor and fragment size
    if it is found return [ture, index] if not found return [false, none]
    '''  
    a1 = a-accuracy
    a2 = a+accuracy
    b1 = b-accuracy
    b2 = b+accuracy    
    check = df.loc[((df['Precursor'] > a1) & (df['Precursor'] < a2)) & ((df['Fragment'] > b1) & (df['Fragment'] < b2))]
    if check.empty and for_isotope == True:
        return None
    elif for_isotope == False:
        '''
        this is used for concat_names_standards
        this should never be used for isotope correction as each mass pair should be unique
        '''
        return check.index
    elif len(check.index) > 1 and for_isotope == True:
        ms2diff = check.Fragment.apply(lambda x:abs(x-b))
        ms2diff.sort_values(inplace = True)
        return ms2diff.index[0]
    else:
        return check.index[0]

def locate(df, component, a, accuracy = .3):
    '''
    this will locate all of the indexes where 'a' is found in column 'component'
    on DataFrame df
    '''
    df = df   
    a1 = a-accuracy
    a2 = a+accuracy   
    check = df.loc[((df[component] >= a1) & (df[component] <= a2))]
    if check.empty:
        return None
    else:
        return list(check.index)

def keywithmaxval(d):
     """ a) create a list of the dict's keys and values; 
         b) return the key with the max value"""  
     v=list(d.values())
     k=list(d.keys())
     return k[v.index(max(v))]   

def take_closest(myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before

def concat_names_standards(names_table, standards_table, mode):
    """
    Adds standards to a names list for naming with the group name 'Standard'
    """    
    if standards_table is None:
        return names_table
    standards_table = standards_table[standards_table.Polarity == mode]
    to_append = standards_table[['MS1', 'MS2', 'Name', 'Lipid_Group']]
    to_append.columns = ['Precursor', 'Fragment', 'Name', 'Group'] 
    assert to_append.columns.all() in names_table.columns
    #need to remove and mass pairs that match the standards from the names table
    for i in to_append.index:
        pre = to_append['Precursor'][i]
        frag = to_append['Fragment'][i]
        found = find(names_table, pre,frag, for_isotope=False)
        if len(found) != 0:
            names_table.drop(found, inplace = True)
        else:
            pass
    #remove any name from the origonal names list that shows up in the standard mix
    for name in to_append.Name:
        names_table = names_table[names_table.Name != name]
    return pd.concat([names_table, to_append]).reset_index(drop=True)
    
def combine_MS_data(pos_data, neg_data):
    '''
    This does not work if the class.data_frame has more columns than that in
    the names list    
    '''
    if pos_data is None and neg_data is None:
        raise TypeError('Both inputs are None type.')
    else:
        try:
            return pos_data.combine(neg_data)
        except:
            return neg_data.combine(pos_data)
            
def MS_Data_Like(data_frame, ms_data):
    return MS_Data(data_frame, ms_data.name_table, ms_data.mode, ms_data.standards)
    
def _test_isotope_qc():
    MS_Process(test_dir+'111814_Pano_MSMSall_pos.txt', test_dir+'Names_Pano_111814.csv', '+').saturation_check('TEST_SATURATION', 'isotope_qc_check')
    #MS_Process(test_dir+'111814_Pano_MSMSall_neg.txt', test_dir+'Names_Pano_111814.csv', '-').to_MS_data()

def make_stats_frame(df, g1_name, g2_name, g1_cols, g2_cols, paired = False):
    g1,g2 = g1_cols, g2_cols
    init_columns = list([x for x in df.columns.tolist() if not x in g1+g2])
    stats = df[init_columns]
    stats['Mean_'+g1_name] = df[g1].mean(axis=1)
    stats['Mean_'+g2_name] = df[g2].mean(axis=1)
    stats['Stdev_'+g1_name] = df[g1].std(axis=1)
    stats['Stdev_'+g2_name] = df[g2].std(axis=1)
    stats['Fold_Change'] = stats['Mean_'+g1_name].div(stats['Mean_'+g2_name])
    if len(g1) >= 3 and len(g2) >= 3:
        if not paired:        
            stats['RS_p_value'] = df.apply(lambda x : ranksums(x[g1], x[g2])[1], axis=1)
            stats['t_test_p_value'] = ttest_ind(df[g1], df[g2], axis=1)[1]
        if paired:
            stats['t_test_p_value'] = ttest_rel(df[g1], df[g2], axis=1)[1]
    stats['Median_'+g1_name] = df[g1].median(axis=1)
    stats['Median_'+g2_name] = df[g2].median(axis=1)
    stats['Range_'+g1_name] = df[g1].max(axis=1) - df[g1].min(axis=1)
    stats['Range_'+g2_name] = df[g2].max(axis=1) - df[g2].min(axis=1)
    cols = init_columns + ['Fold_Change', 'RS_p_value',
                          't_test_p_value',
                          'Mean_'+g1_name, 'Stdev_'+g1_name,
                          'Median_'+g1_name, 'Range_'+g1_name,
                          'Mean_'+g2_name, 'Stdev_'+g2_name,
                          'Median_'+g2_name, 'Range_'+g2_name]
    if len(g1) < 3 or len(g2) < 3:
        cols.remove('t_test_p_value')
        cols.remove('RS_p_value')
    if paired:
        cols = cols.remove('RS_p_value')
    stats = stats[cols]
    return stats

##TESTS
if __name__ == '__main__':
    pass



#    _test_isotope_qc()
#    pos = MS_Process(test_dir+'sim_pos.txt', test_dir+'test_weight_standard.csv', '+').to_MS_data()
#    neg = MS_Process(test_dir+'sim_neg.txt', test_dir+'test_weight_standard.csv', '-').to_MS_data()
#    pos_named = pos.Name_Molecules()
#    neg_named = neg.Name_Molecules()
#    combined = pos_named.combine(neg_named)
#    test_s = combined.data_frame.Name
#    test_standards = test_s.apply(lambda x : x.get_standard(combined))
#    std_norm_neg = neg_named.standard_normalize()
#    std_norm_pos = pos_named.standard_normalize()
#    combined_std_norm = combined.standard_normalize()
#    tested = combined.Hypothesis_Tests((1,2))
#    gp = combined.data_frame.Group.drop_duplicates()
#    ss_dict = {g : combined.summarize_group_feature(g, (1,2)) for g in gp if g.lower() not in GROUPS_TO_SKIP}
#    types = combined.data_frame.Type.drop_duplicates()
#    t_dict =  {g : combined.summarize_type_feature(g, (1,2)) for g in types if g.lower() not in TYPES_TO_SKIP}
#    ###Check that standard normalized data entries are all
#    ###equal to 1/sample_weight to within floating point error.
#    for i in range(len(combined.sample_cols)):
#        print 'Checking standard normalization for column', i,  '...'
#        condition = abs(combined_std_norm.data_frame[combined.sample_cols[i]] - 1.0/combined.name_table.ix[i].Weight) < 10**-7
#        try:
#            assert condition.all()
#            print 'Standard normalization test passed.'
#        except AssertionError:
#            raise ValueError('Standard normalization is not working at sample number: '+str(i))
#    df_len = len(combined.hypothesis_tests.index)
#    combined.hypothesis_tests['Fold_Change'] = 10*np.random.rand(df_len)
#    combined.hypothesis_tests['t_test_p_value'] = np.random.rand(df_len)
#    combined.Volcano(saveas='who.png')
#    combined.Manhattan(saveas='what.png')
    
    
    
    
